import os
import sys
from alembic import context
from logging.config import fileConfig

#logging
config = context.config
fileConfig(config.config_file_name)

#add path
sys.path.append(os.path.abspath("."))
import thanatos.utils

db = thanatos.utils.DB("sqlite:///thanatos.db")
with db.engine.connect() as connection:
    context.configure(
        connection=connection,
        target_metadata=thanatos.models.Base.metadata,
        compare_type=True,
        render_as_batch=True
    )

    with context.begin_transaction():
        context.run_migrations()
