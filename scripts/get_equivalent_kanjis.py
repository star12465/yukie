#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
from requests import post

with open("equivalent_kanjis.txt", "w", encoding="utf-8") as fp:
    url = "https://www.jpmarumaru.com/tw/teachKanjiComparison.asp"
    for i in range(1, 72):
        print("Processing {}/71".format(i))

        r = post(url, {"keyword": "", "comType": "", "Page": "{}".format(i)})
        res = re.findall('<a lang="ja" href="teachKanjiDetail\.asp\?sn=\d+">(.*?)</a>.*?style.*?>(.*?)</td>.*?style.*?>(.*?)</td>', r.text, re.DOTALL)

        if i < 71: assert(len(res) == 10)
        else: assert(len(res) == 5)

        #best variable names ever
        for xxx in res:
            words = set()
            for xx in xxx:
                for x in xx.split(","):
                    words.add(x)

            if len(words) > 1:
                fp.write("=".join(list(words)) + "\n")
