from thanatos.utils import StringNormalizer
from thanatos.managers import *
from .mocks import thanatos
from .db import TestDB, BigDB, EmptyDB

__all__ = ["defaults", "ut_utils", "mocks",
            "TestDB", "BigDB", "EmptyDB"]

#init environment
StringNormalizer.initialize(thanatos.config.data_folder)
AwarenessManager.initialize()
PersonalitiesManager.initialize(thanatos)
LanguagesManager.initialize(thanatos.config.language, thanatos.config.locale_folder)
