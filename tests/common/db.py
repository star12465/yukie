import asyncio
import shutil
import os
from tempfile import mktemp
from thanatos.utils import DB

TMP_DIR = "tmp/"
BIG_DB = "big_db.db"
EMPTY_DB = "empty.db"
SAMPLE_DB_PATH = "tests/sample.db"

try:
    os.mkdir(TMP_DIR, 0o750)
except OSError:
    #directory already exists
    pass

class TestDB(DB):
    def __init__(self, db_filename=None):
        self.db_path = mktemp(dir=TMP_DIR)
        if db_filename != None:
            shutil.copyfile(db_filename, self.db_path)
        super().__init__("sqlite:///{}".format(self.db_path))

    def close(self):
        super().close()
        os.unlink(self.db_path)
        

class BigDB(TestDB):
    def __init__(self):
        super().__init__(SAMPLE_DB_PATH)

class EmptyDB(TestDB):
    def __init__(self):
        super().__init__()
