import asyncio
import concurrent.futures
from thanatos.thanatos import ThanatosConfig
from unittest.mock import MagicMock
from tests.common import defaults

__all__ = ["thanatos"]

#we'll be exposing config through thanatos.config
config = ThanatosConfig()
config.load_from_file("tests/config.yaml")
config.validate()

thanatos = MagicMock()
thanatos.config = config
thanatos.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

#async methods
async def nothing(*args, **kwargs): pass
thanatos.edit_profile = nothing
thanatos.change_nickname = nothing

#simulate send_message
async def send_message(*args, **kwargs):
    discord_msg = MagicMock()
    discord_msg.id = send_message.discord_msg_id
    send_message.discord_msg_id += 1
send_message.discord_msg_id = 1
thanatos.send_message = send_message

#db and loop cannot be mocked
del thanatos.loop
del thanatos.db
