import sqlalchemy
from unittest.mock import MagicMock
from sqlalchemy.sql.expression import func
from thanatos.models import *
from thanatos.contexts import *
from tests.common import defaults, mocks, TestDB

__all__ = ["get_a_user", "new_req_ctx", "new_guild", "new_user", "new_user_favs"]

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

def db_to_session(f):
    def wrapper(*args, **kwargs):
        db = args[0]
        if isinstance(db, TestDB):
            #we passed in a db
            with db.session(expire_on_commit=False, autoflush=False) as db_session:
                args = list(args)
                args[0] = db_session
                return f(*args, **kwargs)
        elif isinstance(db, sqlalchemy.orm.session.Session):
            #already a db session
            return f(*args, **kwargs)
        else:
            raise ValueError("{} is not a db or db_session instance".format(args[0]))
    return wrapper

@db_to_session
def get_a_user(db_session):
    #try to use a user we already have
    q = db_session.query(User)
    user = q.first() if q.count() > 0 else None

    return user if user else new_user(db_session)

@db_to_session
def get_a_guild(db_session):
    q = db_session.query(Guild)
    guild = q.first() if q.count() > 0 else None

    return guild if guild else new_guild(db_session)

def new_req_ctx(db, user=None, guild=None, channel=None, loop=None):
    req_ctx = RequestContext()
    req_ctx.thanatos = mocks.thanatos
    req_ctx.db = db
    req_ctx.user = user if user else get_a_user(db)
    req_ctx.guild = guild if guild else get_a_guild(db)

    req_ctx.discord_msg = MagicMock()
    req_ctx.discord_msg.channel = channel if channel else defaults.CHANNEL
    return req_ctx

@static_vars(guild_id_counter=1, guild_name_counter=1)
def new_guild(db_session, guild_id=None, guild_name=None):
    guild = Guild()

    #guild id
    if guild_id:
        guild.id = guild_id
    else:
        guild.id = new_guild.guild_id_counter

    #guild name
    if guild_name:
        guild.name = guild_name
    else:
        guild.name = defaults.GUILDNAME.format(new_guild.guild_name_counter)

    return db_session.merge(guild)

@static_vars(user_id_counter=1, username_counter=1)
@db_to_session
def new_user(db_session, **kwargs):
    user = User()

    #user id
    if "user_id" in kwargs:
        user.id = kwargs["user_id"]
    else:
        user.id = new_user.user_id_counter
        new_user.user_id_counter += 1

    #username
    if "username" in kwargs:
        user.username = kwargs["username"] 
    else:
        user.username = defaults.USERNAME.format(new_user.username_counter)
        new_user.username_counter += 1

    #other fields
    user.discriminator = kwargs["discriminator"] if "discriminator" in kwargs else "0"
    user.nickname = kwargs["nickname"] if "nickname" in kwargs else "{}_".format(user.username)
    user.privilege = kwargs["privilege"] if "privilege" in kwargs else 50
    return db_session.merge(user)

@db_to_session
def new_user_favs(db_session, target_type, user=None, target_id=None, count=1):
    #let's merge user first
    user = user if user else get_a_user(db_session)
    user = db_session.merge(user)

    res = []
    for _ in range(count):
        user_fav = UserFavorite()
        user_fav.user = user
        user_fav.target_type = target_type.__name__
        if target_id != None:
            user_fav.target_id = target_id
        else:
            user_fav.target_id = db_session.query(target_type).order_by(func.random()).first().id

        #make sure we don't violate unique constraint
        q = db_session.query(UserFavorite).filter_by(user=user_fav.user,
                                                     target_type=user_fav.target_type,
                                                     target_id=user_fav.target_id)
        if q.count() > 0:
            continue
        res.append(user_fav)
    return res
