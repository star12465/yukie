# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import BrandContext

class TestBrandContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)

        with self.db.session() as db_session:
            brand = db_session.query(Brand).filter_by(id=689).one()

        res_ctx = BrandContext(req_ctx, brand)
        res_ctx.preprocess()
        self.assertTrue(hasattr(res_ctx, "discord_embed"))
