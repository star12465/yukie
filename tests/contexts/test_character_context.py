# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import *

class TestCharacterContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)

        with req_ctx.db.session() as db_session:
            character = Character.get_by_name(db_session, "螢 りんね")
        res_ctx = CharacterContext(req_ctx, character)
        res_ctx.preprocess()
