# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import CreatorContext

class TestCreatorContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)
        with self.db.session() as db_session:
            creator = db_session.query(Creator).filter_by(id=14116).one()

        res_ctx = CreatorContext(req_ctx, creator)
        res_ctx.preprocess()
        self.assertTrue(hasattr(res_ctx, "discord_embed"))
