# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import FavRankingContext

class TestFavRankingContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)

        #no favorites
        res_ctx = FavRankingContext(req_ctx, req_ctx.guild)
        res_ctx.preprocess()
        self.assertIsNotNone(res_ctx.discord_embed)

        users = []
        for _ in range(10):
            users.append(ut_utils.new_user(self.db))

        #add favorites
        ut_utils.new_user_favs(self.db, user=users[0], target_type=Creator)
        ut_utils.new_user_favs(self.db, user=users[0], target_type=Galgame)
        ut_utils.new_user_favs(self.db, user=users[0], target_type=Brand)

        #some favorites
        res_ctx = FavRankingContext(req_ctx, req_ctx.guild)
        res_ctx.preprocess()
        self.assertEqual(len(res_ctx.discord_embed.fields), 3)
