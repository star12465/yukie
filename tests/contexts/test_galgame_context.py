# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import GalgameContext

class TestGalgameContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)
        with self.db.session() as db_session:
            galgame = db_session.query(Galgame).filter_by(id=13050).one()

        res_ctx = GalgameContext(req_ctx, galgame)
        res_ctx.preprocess()
        self.assertTrue(hasattr(res_ctx, "discord_embed"))
        self.assertTrue(len(res_ctx.discord_embed.fields) > 5)
