# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from tests.common import *
from thanatos.models import *
from thanatos.contexts import *

__all__ = ["TestGalgamesMonthlyContext"]

class TestGalgamesMonthlyContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)
        now = datetime.now()

        #time in the past
        res_ctx = GalgamesMonthlyContext(req_ctx, 1999, 4)
        res_ctx.preprocess()
        self.assertTrue(hasattr(res_ctx, "discord_embed"))

        #time in the future
        if now.month == 12:
            res_ctx = GalgamesMonthlyContext(req_ctx, now.year+1, 1)
        else:
            res_ctx = GalgamesMonthlyContext(req_ctx, now.year, now.month+1)
        res_ctx.preprocess()
        self.assertTrue(hasattr(res_ctx, "discord_embed"))

        #no month past
        res_ctx = GalgamesMonthlyContext(req_ctx, 1999, None)
        res_ctx.preprocess()
        self.assertTrue(hasattr(res_ctx, "discord_embed"))

        #no month future
        res_ctx = GalgamesMonthlyContext(req_ctx, now.year+1, None)
        res_ctx.preprocess()
        self.assertTrue(hasattr(res_ctx, "discord_embed"))
