# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import *

class TestSearchResultsContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)
        req_ctx.keyword = "abc"

        #make some fake search results
        results = ((1, (Galgame, 12082)), (1, (Creator, 1409)), (1, (Brand, 84)), (1, (Character, 1)))

        #one of each type
        res_ctx = SearchResultsContext(req_ctx, results)
        res_ctx.preprocess()
        self.assertIsNotNone(res_ctx.discord_embed)

        #a lot of them
        results = (1, (Galgame, 12082))*50
        res_ctx.preprocess()
        self.assertIsNotNone(res_ctx.discord_embed)
