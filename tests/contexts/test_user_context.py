# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import UserContext

class TestUserContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)

        #no favorites
        res_ctx = UserContext(req_ctx, req_ctx.user)
        res_ctx.preprocess()
        self.assertIsNotNone(res_ctx.discord_embed)

        #add favorites
        ut_utils.new_user_favs(self.db, user=req_ctx.user, target_type=Creator)
        ut_utils.new_user_favs(self.db, user=req_ctx.user, target_type=Galgame)
        ut_utils.new_user_favs(self.db, user=req_ctx.user, target_type=Brand)

        #test with all the favorites
        res_ctx = UserContext(req_ctx, req_ctx.user)
        res_ctx.preprocess()
        self.assertEqual(len(res_ctx.discord_embed.fields), 3)
