# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.utils import *
from thanatos.models import *
from thanatos.contexts import *

class TestUserToPlayContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)

        #no favorites
        res_ctx = UserToPlayContext(req_ctx, req_ctx.user)
        res_ctx.preprocess()
        self.assertIsNotNone(res_ctx.discord_embed)

        #add to plays
        Metadata(self.db, req_ctx.user, "to_play")["to_play"] = [24991]

        #test with all the favorites
        res_ctx = UserToPlayContext(req_ctx, req_ctx.user)
        res_ctx.preprocess()
        self.assertIsNotNone(res_ctx.discord_embed)
