# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import VideoContext

class TestVideoContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)

        #get galgame
        with self.db.session() as db_session:
            galgame = db_session.query(Galgame).filter_by(id=4979).one()
        galgame.update(self.db, Galgame.Component.Videos)

        #video found (first)
        req_ctx.video_n = 1
        req_ctx.video_type = VideoType.OP
        video = galgame.get_video(req_ctx.db, req_ctx.video_type, req_ctx.video_n)
        res_ctx = VideoContext(req_ctx, video)
        res_ctx.preprocess()
        self.assertFalse(hasattr(res_ctx, "error_msg"))
        self.assertTrue(hasattr(res_ctx, "discord_content"))

        #video found (designated)
        req_ctx.video_n = 2
        with self.db.session() as db_session:
            video = galgame.get_video(req_ctx.db, req_ctx.video_type, req_ctx.video_n)
        res_ctx2 = VideoContext(req_ctx, video)
        res_ctx2.preprocess()
        self.assertFalse(hasattr(res_ctx2, "error_msg"))
        self.assertTrue(hasattr(res_ctx2, "discord_content"))
        self.assertNotEqual(res_ctx.discord_content, res_ctx2.discord_content)
