# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.contexts import VideosListContext

class TestVideosListContext(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_preprocess(self):
        req_ctx = ut_utils.new_req_ctx(self.db)

        #get galgame
        with self.db.session() as db_session:
            galgame = db_session.query(Galgame).filter_by(id=23978).one()
        galgame.update(self.db, Galgame.Component.Videos)

        #test all
        req_ctx.video_type = VideoType.All
        res_ctx = VideosListContext(req_ctx, galgame)
        res_ctx.preprocess()
        self.assertGreater(len(res_ctx.discord_embed.fields), 2)

        #test one
        req_ctx.video_type = VideoType.Countdown
        res_ctx = VideosListContext(req_ctx, galgame)
        res_ctx.preprocess()
        self.assertEqual(len(res_ctx.discord_embed.fields), 1)

        #test none
        req_ctx.video_type = VideoType.All
        with self.db.session() as db_session:
            galgame = db_session.merge(galgame)
            galgame.videos = []
        res_ctx = VideosListContext(req_ctx, galgame)
        res_ctx.preprocess()
        self.assertEqual(len(res_ctx.discord_embed.fields), 0)
