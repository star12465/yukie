# -*- coding: utf-8 -*-
import unittest
import yaml
import os
from tests.common import *
from thanatos.managers import LanguagesManager
from thanatos.thanatos import ThanatosConfig

class TestLanguagesManager(unittest.TestCase):
    
    def setUp(self):
        self.LANGUAGE = 'en'
        self.config = mocks.thanatos.config
        self.TEST_MO_DIR = self.config.locale_folder
        self.TEST_PO_DIR = 'tests/locales/test_language_po/'
        LanguagesManager.initialize(self.config.language, self.TEST_MO_DIR)

    def tearDown(self):
        LanguagesManager.cleanup()

    @unittest.mock.patch('thanatos.managers.languages_manager.shutil')
    def test_cleanup(self, mock_shutil):
        LanguagesManager.cleanup()
        mock_shutil.rmtree.assert_called_with(self.TEST_MO_DIR, ignore_errors=True)

    @unittest.mock.patch('thanatos.managers.languages_manager.os.path.abspath')
    def test_get_po_filepath(self, mock_abspath):
        mock_abspath.return_value = os.path.join(os.getcwd(), self.TEST_PO_DIR)
        fp = LanguagesManager.get_po_filepath(self.LANGUAGE)
        self.assertTrue(os.path.exists(fp))
        fp = LanguagesManager.get_po_filepath('invalid_language')
        self.assertFalse(os.path.exists(fp))

    @unittest.mock.patch('thanatos.managers.languages_manager.os.path.abspath')
    def test_generate_mo_file(self, mock_abspath):
        mock_abspath.return_value = os.path.join(os.getcwd(), self.TEST_PO_DIR)
        mo_file = LanguagesManager.generate_mo_file(self.LANGUAGE)
        self.assertTrue(os.path.exists(mo_file))
        mo_file = LanguagesManager.generate_mo_file('invalid_language')
        self.assertIsNone(mo_file)
    
    @unittest.mock.patch('thanatos.managers.languages_manager.os.path.abspath')
    def test_set_language(self, mock_abspath):
        mock_abspath.return_value = os.path.join(os.getcwd(), self.TEST_PO_DIR)
        LanguagesManager.set_language(self.LANGUAGE)
        self.assertTrue(_('test input') == 'test output')
        self.assertTrue(_('not translated') == 'not translated')
        LanguagesManager.set_language('invalid_language')
        self.assertFalse(_('test input') == 'test output')
        self.assertTrue(_('not translated') == 'not translated')

