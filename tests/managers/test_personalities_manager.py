# -*- coding: utf-8 -*-
import unittest
from copy import copy
from tests.common import *
from thanatos.message_handlers import QueryContentType
from thanatos.message_handlers.commands import CommandErrorCode
from thanatos.managers import *
from thanatos.contexts import *
from thanatos.models import *

class TestPersonalitiesManager(unittest.TestCase):
    def setUp(self):
        self.big_db = BigDB()
        mocks.thanatos.db = self.big_db
        PersonalitiesManager.initialize(mocks.thanatos)

    def tearDown(self):
        self.big_db.close()

    def test_all_personalities(self):
        req_ctx = ut_utils.new_req_ctx(self.big_db)

        for personality in PersonalitiesManager.personalities.values():
            #test properties
            self.assertIsInstance(personality.name, str)
            self.assertIsInstance(personality.author, str)
            self.assertIsInstance(personality.enabled, bool)

            #action not required by these
            #just make sure they don't throw an exception
            personality.a_command_start(req_ctx)
            personality.a_command_parsed(req_ctx)
            personality.a_query_start(req_ctx)
            req_ctx.sentence = "test"
            personality.a_chat_start(req_ctx)

            #if they choose to call send, they should supply a message
            fns = ["a_command_not_found", "a_command_help_done", "a_command_設馬甲_done",
                    "a_command_設馬甲_failed", "a_command_設本尊_done", "a_command_set_privilege_bad_privilege",
                    "a_command_set_privilege_done", "a_query_help_done",
                    "a_command_設人格_done", "a_command_設人格_not_found"]
            for fn in fns:
                res_ctx = unittest.mock.MagicMock()
                res_ctx.req_ctx = req_ctx
                getattr(personality, fn)(res_ctx)
                if res_ctx.send.called:
                    self.assertNotEqual(res_ctx.msg.strip(), "", fn)

            #command error
            for code in CommandErrorCode:
                if code == CommandErrorCode.TokenObjNotFound:
                    #we need to pass in a tuple for this
                    note = (Galgame, "rewrite")
                else:
                    note = "test"
                res_ctx = unittest.mock.MagicMock()
                res_ctx.req_ctx = req_ctx
                res_ctx.error_code = code
                res_ctx.note = note
                personality.a_command_error(res_ctx)

            #call these and call it good if no exception is thrown
            #we can call send on these without supplying a message msg
            fns = ["a_command_say_done", "a_command_偷窺_done", "a_command_最愛排行_done",
                   "a_command_我_done", "a_command_待玩_done"]
            for fn in fns:
                res_ctx = unittest.mock.MagicMock()
                res_ctx.req_ctx = req_ctx
                getattr(personality, fn)(res_ctx)
                
            #query
            res_ctx = unittest.mock.MagicMock()
            res_ctx.keyword = "a"
            personality.a_query_done(copy(res_ctx))
            res_ctx2 = copy(res_ctx)
            personality.a_query_loading(res_ctx2)
            personality.a_query_done(res_ctx2)

            #query not found
            res_ctx2 = copy(res_ctx)
            res_ctx2.query_type = QueryContentType.Video
            personality.a_query_not_found(copy(res_ctx2))
            res_ctx2 = copy(res_ctx)
            res_ctx2.query_type = QueryContentType.Unknown
            personality.a_query_not_found(copy(res_ctx2))

            #command.正義的大小
            for i in range(0, 100):
                res_ctx = unittest.mock.MagicMock()
                res_ctx.req_ctx = req_ctx
                res_ctx.size = "test"
                res_ctx.count = i
                personality.a_command_正義的大小_done(res_ctx)

            #command.加最愛
            res_ctx = unittest.mock.MagicMock()
            res_ctx.req_ctx = req_ctx
            res_ctx.favorites = ["a", "b"]
            res_ctx.not_found = ["c", "d"]
            res_ctx.already_favorites = ["e", "f"]
            r = copy(res_ctx)
            r.favorites = []
            personality.a_command_加最愛_done(r)
            r = copy(res_ctx)
            r.already_favorites = []
            personality.a_command_加最愛_done(r)
            r = copy(res_ctx)
            r.not_found = []
            personality.a_command_加最愛_done(r)
            r = copy(res_ctx)
            personality.a_command_加最愛_done(r)

            #command.刪最愛
            res_ctx = unittest.mock.MagicMock()
            res_ctx.req_ctx = req_ctx
            res_ctx.removed = ["a", "b"]
            res_ctx.not_found = ["c", "d"]
            res_ctx.not_favorites = ["e", "f"]
            r = copy(res_ctx)
            r.removed = []
            personality.a_command_刪最愛_done(r)
            r = copy(res_ctx)
            r.not_favorites = []
            personality.a_command_刪最愛_done(r)
            r = copy(res_ctx)
            r.not_found = []
            personality.a_command_刪最愛_done(r)
            r = copy(res_ctx)
            personality.a_command_刪最愛_done(r)

            #command.加待玩
            res_ctx = unittest.mock.MagicMock()
            res_ctx.req_ctx = req_ctx
            res_ctx.new = ["a", "b"]
            res_ctx.not_found = ["c", "d"]
            res_ctx.already = ["e", "f"]
            r = copy(res_ctx)
            r.favorites = []
            personality.a_command_加待玩_done(r)
            r = copy(res_ctx)
            r.already_favorites = []
            personality.a_command_加待玩_done(r)
            r = copy(res_ctx)
            r.not_found = []
            personality.a_command_加待玩_done(r)
            r = copy(res_ctx)
            personality.a_command_加待玩_done(r)

            #command.刪待玩
            res_ctx = unittest.mock.MagicMock()
            res_ctx.req_ctx = req_ctx
            res_ctx.removed = ["a", "b"]
            res_ctx.not_found = ["c", "d"]
            res_ctx.not_favorites = ["e", "f"]
            r = copy(res_ctx)
            r.removed = []
            personality.a_command_刪待玩_done(r)
            r = copy(res_ctx)
            r.not_favorites = []
            personality.a_command_刪待玩_done(r)
            r = copy(res_ctx)
            r.not_found = []
            personality.a_command_刪待玩_done(r)
            r = copy(res_ctx)
            personality.a_command_刪待玩_done(r)
