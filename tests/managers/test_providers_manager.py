# -*- coding: utf-8 -*-
import time
import logging
import unittest
import threading
from copy import copy
from tests.common import *
from thanatos.managers import *
from thanatos.contexts import *
from thanatos.providers.provider import Provider
from thanatos.models import *

__all__ = ["TestProvidersManager"]

logger = logging.getLogger(__name__)

class FakeProvider(Provider):
    components_called_with = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
    def update_galgame(self, galgame, components):
        self.components_called_with.append(components)
        galgame.mark_updated(self.db, self.__class__, components)

class SlowProvider(FakeProvider):
    def update_galgame(self, galgame, components):
        time.sleep(2)
        super().update_galgame(galgame, components)

class TestProvidersManager(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_update(self):
        return
        with self.db.session() as db_session:
            galgame = Galgame.get_by_name(db_session, "春季限定ポコ・ア・ポコ!")

        original_providers = ProvidersManager.PROVIDERS
        ProvidersManager.PROVIDERS = [FakeProvider]
        try:
            FakeProvider.components_called_with = []
            ProvidersManager.update(self.db, [galgame], Galgame.Component.Core)
            self.assertEqual(FakeProvider.components_called_with, [Galgame.Component.Core])
            ProvidersManager.update(self.db, [galgame], Galgame.Component.Core|Galgame.Component.Traits)
            self.assertEqual(FakeProvider.components_called_with, [Galgame.Component.Core, Galgame.Component.Traits])
            ProvidersManager.update(self.db, [galgame], Galgame.Component.Core|Galgame.Component.Traits)
            self.assertEqual(FakeProvider.components_called_with, [Galgame.Component.Core, Galgame.Component.Traits])
        finally:
            ProvidersManager.PROVIDERS = original_providers

    def test_update_multihread(self):
        with self.db.session() as db_session:
            galgame = Galgame.get_by_name(db_session, "Baldr Bringer")

        original_providers = ProvidersManager.PROVIDERS
        ProvidersManager.PROVIDERS = [SlowProvider]
        try:
            SlowProvider.components_called_with = []

            #test isolated components
            components = [Galgame.Component.Core, Galgame.Component.Traits, Galgame.Component.Prerelease]
            def update(n):
                component = components[n%3]
                ProvidersManager.update(self.db, [galgame], component)
            threads = []
            for _ in range(50):
                thread = threading.Thread(target=update, args=(_,))
                thread.start()
                threads.append(thread)
            for thread in threads:
                thread.join()
            self.assertSetEqual(set(SlowProvider.components_called_with), set(components))

            #test combination of components
            with self.db.session() as db_session:
                galgame = Galgame.get_by_name(db_session, "ノラと皇女と野良猫ハート")
            SlowProvider.components_called_with = []
            components = [
                    Galgame.Component.Core | Galgame.Component.Traits,
                    Galgame.Component.Core | Galgame.Component.Prerelease,
                    Galgame.Component.Core | Galgame.Component.Prerelease | Galgame.Component.Traits | Galgame.Component.Videos,
            ]
            def update(n):
                component = components[n%3]
                ProvidersManager.update(self.db, [galgame], component)
            threads = []
            for _ in range(50):
                thread = threading.Thread(target=update, args=(_,))
                thread.start()
                threads.append(thread)
            for thread in threads:
                thread.join()
            #one for core, and three for each of the combinations of components
            #so we should make no more than 4 calls to provider
            self.assertLess(len(SlowProvider.components_called_with), 5)
        finally:
            ProvidersManager.PROVIDERS = original_providers
