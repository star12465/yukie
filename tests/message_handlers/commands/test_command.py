#s -*- coding: utf-8 -*-
import logging
import unittest
from tests.common import *
from thanatos.message_handlers.command_handler import CommandHandler
from thanatos.message_handlers.commands.command import *
from thanatos.managers import *
from thanatos.contexts import *
from thanatos.models import *

logger = logging.getLogger(__name__)

class TestCommand(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    @unittest.mock.patch("thanatos.message_handlers.commands.command.AwarenessManager")
    def test_execute(self, aware_mgr):
        @make_command()
        def cmd_test(req_ctx, *args, **kwargs):
            return ResponseContext(req_ctx)
        cmd = cmd_test()

        #new request
        req_ctx = ut_utils.new_req_ctx(self.db)
        req_ctx.cmd_str = "test_cmd"
        req_ctx.args_str = "test_args"

        #no privilege
        cmd.privilege = 10
        req_ctx.user.privilege = 11
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.NoPrivilege)
        req_ctx.user.privilege = 10

        #unclosed square bracket in grammar
        cmd.grammar = "[[[a]] bbb"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.Grammar)

        #missing closing greater than symbol
        cmd.grammar = "<bb"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.Grammar)
        cmd.grammar = "bb"
        req_ctx.args_str = "<aa"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.MissingClosingGt)
        req_ctx.args_str = " aa <abc"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.MissingClosingGt)

        #a less than symbol within < and >
        cmd.grammar = "<b<b>>"
        req_ctx.args_str = ""
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.Grammar)
        cmd.grammar = "bb"
        req_ctx.args_str = "<a<ab>"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.LtInLtTokens)
        req_ctx.args_str = "<abc<"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.LtInLtTokens)

        #missing arguments
        cmd.grammar = "aa bb"
        req_ctx.args_str = "cc"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.MissingArguments)
        cmd.grammar = "<rainbow|Brand|,>"
        req_ctx.args_str = ",,,,"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.MissingArguments)

        #too many arguments
        cmd.grammar = "aa"
        req_ctx.args_str = "cc cc"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.TooManyArguments)

        #no matching signature
        cmd.grammar = "[aa bb] cc"
        req_ctx.args_str = "cc cc"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.NoMatchingSig)

        #not allowed in pm
        guild = req_ctx.guild
        req_ctx.guild = None
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.NotAllowedInPM)
        req_ctx.guild = guild

        #misc grammar error
        cmd.grammar = "[aa bb] cc [bb]"
        req_ctx.args_str = "cc cc"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.Grammar)
        cmd.grammar = "... cc"
        req_ctx.args_str = "aa bb cc"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.Grammar)
        cmd.grammar = "cc <cc|Galgame|=>"
        req_ctx.args_str = "aa rewrite=clannad"
        cmd.execute(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.error")
        self.assertEqual(res_ctx.cmd_error, CommandErrorCode.Grammar)

        #test single objects
        cmd.grammar = "aa [[bbb ddd] <@user> <galgame|Galgame> <brand|Brand>] <creator|Creator> <magic|*int>"
        req_ctx.args_str = "cc <@{}> Rewrite <key> 桐月 23".format(req_ctx.user.id)
        cmd.execute(req_ctx)
        (awareness, req_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.parsed")
        self.assertEqual(req_ctx.cmd_kwargs["aa"], "cc")
        self.assertIsInstance(req_ctx.cmd_kwargs["user"], User)
        self.assertEqual(req_ctx.cmd_kwargs["user"].id, req_ctx.user.id)
        self.assertIsInstance(req_ctx.cmd_kwargs["galgame"], Galgame)
        self.assertEqual(req_ctx.cmd_kwargs["galgame"].id, 10917)
        self.assertIsInstance(req_ctx.cmd_kwargs["brand"], Brand)
        self.assertEqual(req_ctx.cmd_kwargs["brand"].id, 83)
        self.assertIsInstance(req_ctx.cmd_kwargs["creator"], Creator)
        self.assertEqual(req_ctx.cmd_kwargs["creator"].id, 5282)
        self.assertEqual(req_ctx.cmd_kwargs["magic"], 23)

        cmd.grammar = "aa <@user> <rainbow|Galgame,Creator> ..."
        req_ctx.args_str = "<cc> <@!{}> 田中ロミオ ab cd".format(req_ctx.user.id)
        cmd.execute(req_ctx)
        (awareness, req_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.parsed")
        self.assertEqual(req_ctx.cmd_kwargs["aa"], "cc")
        self.assertIsInstance(req_ctx.cmd_kwargs["user"], User)
        self.assertEqual(req_ctx.cmd_kwargs["user"].id, req_ctx.user.id)
        self.assertIsInstance(req_ctx.cmd_kwargs["rainbow"], Creator)
        self.assertEqual(req_ctx.cmd_kwargs["rainbow"].id, 892)
        self.assertEqual(req_ctx.cmd_kwargs["other"], "ab cd")

        cmd.grammar = "aa <target|Galgame,Creator> [cc [dd]]"
        req_ctx.args_str = "cc <clannad full voice> ab cd"
        cmd.execute(req_ctx)
        (awareness, req_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.parsed")
        self.assertEqual(req_ctx.cmd_kwargs["aa"], "cc")
        self.assertIsInstance(req_ctx.cmd_kwargs["target"], Galgame)
        self.assertEqual(req_ctx.cmd_kwargs["target"].id, 10511)
        self.assertEqual(req_ctx.cmd_kwargs["cc"], "ab")
        self.assertEqual(req_ctx.cmd_kwargs["dd"], "cd")

        cmd.grammar = "<target|Galgame,Creator|,=>"
        req_ctx.args_str = "レミニセンス,雪都さお梨=麻枝准,abc"
        cmd.execute(req_ctx)
        (awareness, req_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.parsed")
        found, not_found = req_ctx.cmd_kwargs["target"]
        self.assertEqual(len(found), 3)
        self.assertEqual(found[0].id, 15986)
        self.assertIsInstance(found[0], Galgame)
        self.assertEqual(found[1].id, 10313)
        self.assertIsInstance(found[1], Creator)
        self.assertEqual(found[2].id, 938)
        self.assertIsInstance(found[2], Creator)
        self.assertEqual(not_found, ["abc"])

        cmd.grammar = "<target|Galgame,Creator|,=>"
        req_ctx.args_str = "abc"
        cmd.execute(req_ctx)
        (awareness, req_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.parsed")
        found, not_found = req_ctx.cmd_kwargs["target"]
        self.assertEqual(len(found), 0)
        self.assertEqual(not_found, ["abc"])
