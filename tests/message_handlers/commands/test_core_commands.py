# -*- coding: utf-8 -*-
import logging
import asyncio
import asynctest
from tests.common import *
from thanatos.message_handlers.command_handler import CommandHandler
from thanatos.utils import *
from thanatos.managers import *
from thanatos.contexts import *
from thanatos.models import *

logger = logging.getLogger(__name__)

class TestCoreCommands(asynctest.TestCase):
    def setUp(self):
        self.db = BigDB()
        mocks.thanatos.loop = self.loop
        mocks.thanatos.db = self.db

        #init query self.handler
        with self.db.session() as db_session:
            self.handler = CommandHandler(mocks.thanatos)

    def tearDown(self):
        self.db.close()

    @asynctest.mock.patch("thanatos.message_handlers.commands.core_commands.AwarenessManager")
    async def test_say(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)
        req_ctx.user.privilege = 0

        req_ctx.msg = "!say hello"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.say.done")
        self.assertEqual(type(res_ctx), ResponseContext)
        self.assertEqual(res_ctx.msg, "hello")

    @asynctest.mock.patch("thanatos.message_handlers.commands.core_commands.AwarenessManager")
    async def test_set_privilege(self, aware_mgr):
        user1 = ut_utils.new_user(self.db, privilege=0)
        user2 = ut_utils.new_user(self.db)
        req_ctx = ut_utils.new_req_ctx(self.db, user=user1)

        #test bad privilege
        req_ctx.msg = "!set_privilege <@{}> -10".format(user2.id)
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.set_privilege.bad_privilege")
        self.assertEqual(type(res_ctx), ResponseContext)

        #test intended
        req_ctx.msg = "!set_privilege <@{}> 30".format(user2.id)
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.set_privilege.done")
        self.assertEqual(type(res_ctx), ResponseContext)
        with self.db.session() as db_session:
            user2 = db_session.merge(user2)
            db_session.refresh(user2)
            self.assertEqual(user2.privilege, 30)

    @asynctest.mock.patch("thanatos.managers.personalities_manager.asyncio")
    @asynctest.mock.patch("thanatos.message_handlers.commands.core_commands.AwarenessManager")
    async def test_設人格(self, aware_mgr, asyncio):
        req_ctx = ut_utils.new_req_ctx(self.db)
        req_ctx.user.privilege = 0

        #patch _update_nickname, _update_avatar to prevent
        #sys:1: RuntimeWarning: coroutine 'PersonalitiesManager._update_avatar' was never awaited
        _update_nickname, _update_avatar = PersonalitiesManager._update_nickname, PersonalitiesManager._update_avatar
        def nothing(*args, **kwargs):
            pass
        PersonalitiesManager._update_nickname, PersonalitiesManager._update_avatar = nothing, nothing

        metadata = GlobalMetadata(req_ctx.db)
        self.assertEqual(metadata["last_personality"], None)
        req_ctx.msg = "!設人格 Thanatos"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.設人格.done")
        self.assertEqual(type(res_ctx), ResponseContext)
        self.assertEqual(metadata["last_personality"], "Thanatos")

        #restore
        PersonalitiesManager._update_nickname, PersonalitiesManager._update_avatar = _update_nickname, _update_avatar
