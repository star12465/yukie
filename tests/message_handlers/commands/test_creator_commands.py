# -*- coding: utf-8 -*-
import logging
import asynctest
from tests.common import *
from thanatos.message_handlers.command_handler import CommandHandler
from thanatos.contexts import *
from thanatos.models import *

logger = logging.getLogger(__name__)

class TestCreatorCommands(asynctest.TestCase):
    def setUp(self):
        self.db = BigDB()
        mocks.thanatos.loop = self.loop
        mocks.thanatos.db = self.db

        #init query self.handler
        with self.db.session() as db_session:
            self.handler = CommandHandler(mocks.thanatos)

    def tearDown(self):
        self.db.close()

    @asynctest.mock.patch("thanatos.message_handlers.commands.creator_commands.AwarenessManager")
    async def test_set_creator_alias(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)
        req_ctx.user.privilege = 40

        #test bad input
        req_ctx.msg = "!設馬甲 aaaaa"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.設馬甲.failed")
        self.assertEqual(type(res_ctx), ResponseContext)

        #test unexisting creator
        req_ctx.msg = "!設馬甲 あじ秋刀魚=abc"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.設馬甲.failed")
        self.assertEqual(type(res_ctx), ResponseContext)

        #test intended #1
        with self.db.session() as db_session:
            a = Creator.get_by_name(db_session, "あじ秋刀魚")
            b = Creator.get_by_name(db_session, "北都南")
            count = len(a.get_all_identities()) + len(b.get_all_identities())
            db_session.expunge_all()
        req_ctx.msg = "!設馬甲 {}={}".format(a.name, b.name)
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.設馬甲.done")
        with self.db.session() as db_session:
            a = db_session.merge(a, load=False)
            b = db_session.merge(b, load=False)
            db_session.refresh(a)
            db_session.refresh(b)
            self.assertEqual(len(a.get_all_identities()), len(b.get_all_identities()))
            self.assertEqual(len(a.get_all_identities()), count)

        #test intended
        with self.db.session() as db_session:
            a = Creator.get_by_name(db_session, "有栖川みや美")
            count += len(a.get_all_identities())
        req_ctx.msg = "!設馬甲 あじ秋刀魚=北都南=有栖川みや美"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.設馬甲.done")
        with self.db.session() as db_session:
            a = db_session.merge(a, load=False)
            db_session.refresh(a)
            b = Creator.get_by_name(db_session, "北都南")
            c = Creator.get_by_name(db_session, "有栖川みや美")
            a_count = len(a.get_all_identities())
            self.assertEqual(a_count, len(b.get_all_identities()))
            self.assertEqual(a_count, len(c.get_all_identities()))
            self.assertEqual(a_count, count)

    @asynctest.mock.patch("thanatos.message_handlers.commands.creator_commands.AwarenessManager")
    async def test_set_main_identity(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)
        req_ctx.user.privilege = 40

        #test bad input
        req_ctx.msg = "!設本尊 aaaaa"
        await self.handler.handle_message(req_ctx)
        aware_mgr.activate.assert_not_called()

        #test intended
        with self.db.session() as db_session:
            a = Creator.get_by_name(db_session, "火野琉乃")
            db_session.expunge(a)
        req_ctx.msg = "!設本尊 {}".format(a.name)
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.設本尊.done")
        with self.db.session() as db_session:
            a = db_session.merge(a, load=False)
            db_session.refresh(a)
            b = Creator.get_by_name(db_session, "清水愛")
            self.assertEqual(b.main_identity, a)
            self.assertIsNone(a.main_identity)
            self.assertTrue(a.fix_main_identity)
