# -*- coding: utf-8 -*-
import logging
import asynctest
from tests.common import *
from thanatos.message_handlers.command_handler import CommandHandler
from thanatos.utils import *
from thanatos.managers import *
from thanatos.contexts import *
from thanatos.models import *

logger = logging.getLogger(__name__)

class TestGeneralCommands(asynctest.TestCase):
    def setUp(self):
        self.db = BigDB()
        mocks.thanatos.loop = self.loop
        mocks.thanatos.db = self.db

        #init query self.handler
        with self.db.session() as db_session:
            self.handler = CommandHandler(mocks.thanatos)

    def tearDown(self):
        self.db.close()

    @asynctest.mock.patch("thanatos.message_handlers.commands.general_commands.AwarenessManager")
    async def test_cmd_加最愛刪最愛(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)
        user = req_ctx.user

        #add valid objects
        req_ctx.msg = "!加最愛 有栖川みや美,きっと、澄みわたる朝色よりも、,AUGUST"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.加最愛.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
        with self.db.session() as db_session:
            self.assertTrue(Creator.get_by_name(db_session, "有栖川みや美").is_fav_by_user(user))
            self.assertTrue(Galgame.get_by_name(db_session, "きっと、澄みわたる朝色よりも、").is_fav_by_user(user))
            self.assertTrue(Brand.get_by_name(db_session, "AUGUST").is_fav_by_user(user))
            count = len(db_session.query(UserFavorite).all())
            self.assertEqual(count, 3)

        #add valid and non-existing objects
        req_ctx.msg = "!加最愛 北見六花,aaa"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.加最愛.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
        with self.db.session() as db_session:
            self.assertTrue(Creator.get_by_name(db_session, "北見六花").is_fav_by_user(user))
            self.assertTrue(Creator.get_by_name(db_session, "五行なずな").is_fav_by_user(user))
            count = len(db_session.query(UserFavorite).all())
            self.assertEqual(count, 4)

        #delete non-existing and non-favorite object
        req_ctx.msg = "!刪最愛 aaaaa,そして初恋が妹になる"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.刪最愛.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
        with self.db.session() as db_session:
            self.assertEqual(len(db_session.query(UserFavorite).all()), count)
        
        #delete non-existing, non-favorite, and a favorite object
        req_ctx.msg = "!刪最愛 aaaaa,そして初恋が妹になる,北見六花"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.刪最愛.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
        with self.db.session() as db_session:
            self.assertFalse(Creator.get_by_name(db_session, "五行なずな").is_fav_by_user(user))
            self.assertEqual(len(db_session.query(UserFavorite).all()), count-1)

    @asynctest.mock.patch("thanatos.message_handlers.commands.general_commands.AwarenessManager")
    async def test_cmd_加待玩刪待玩(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)
        to_play = Metadata(self.db, req_ctx.user, "to_play")
        user = req_ctx.user

        req_ctx.msg = "!加待玩 景の海のアペイリア,きっと、澄みわたる朝色よりも、"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.加待玩.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
        self.assertSetEqual(set(to_play["to_play"]), set([12144, 24991]))
        self.assertSetEqual(set(res_ctx.new), set(["景の海のアペイリア", "きっと、澄みわたる朝色よりも、"]))
        self.assertCountEqual(res_ctx.already, [])
        self.assertCountEqual(res_ctx.not_found, [])

        #add valid, non-existing objects, and existing
        req_ctx.msg = "!加待玩 空のつくりかた -under the same sky over the rainbow-,aaa,景の海のアペイリア"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.加待玩.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
        self.assertSetEqual(set(to_play["to_play"]), set([12144, 24991, 23167]))
        self.assertSetEqual(res_ctx.new, set(["空のつくりかた -under the same sky, over the rainbow-"]))
        self.assertSetEqual(res_ctx.already, set(["景の海のアペイリア"]))
        self.assertListEqual(res_ctx.not_found, ["aaa"])

        #delete non-existing and non-favorite object
        req_ctx.msg = "!刪待玩 aaaaa,そして初恋が妹になる"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.刪待玩.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
        self.assertSetEqual(set(to_play["to_play"]), set([12144, 24991, 23167]))
        self.assertCountEqual(res_ctx.removed, [])
        self.assertSetEqual(res_ctx.not_in_to_play, set(["そして初恋が妹になる"]))
        self.assertListEqual(res_ctx.not_found, ["aaaaa"])
        
        #delete non-existing, non-favorite, and a favorite object
        req_ctx.msg = "!刪待玩 aaaaa,そして初恋が妹になる,空のつくりかた -under the same sky over the rainbow-"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.刪待玩.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
        self.assertSetEqual(set(to_play["to_play"]), set([12144, 24991]))
        self.assertSetEqual(res_ctx.removed, set(["空のつくりかた -under the same sky, over the rainbow-"]))
        self.assertSetEqual(res_ctx.not_in_to_play, set(["そして初恋が妹になる"]))
        self.assertListEqual(res_ctx.not_found, ["aaaaa"])

    @asynctest.mock.patch("thanatos.message_handlers.commands.general_commands.AwarenessManager")
    async def test_cmd_待玩(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)

        #with no argument
        req_ctx.msg = "!待玩"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.待玩.done")
        self.assertIsInstance(res_ctx, UserToPlayContext)       

        #with argument
        req_ctx.msg = "!待玩 <@{}>".format(req_ctx.user.id)
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.待玩.done")
        self.assertIsInstance(res_ctx, UserToPlayContext)       

    @asynctest.mock.patch("thanatos.message_handlers.commands.general_commands.AwarenessManager")
    async def test_cmd_我(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)

        req_ctx.msg = "!我"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.我.done")
        self.assertIsInstance(res_ctx, UserContext)       

    @asynctest.mock.patch("thanatos.message_handlers.commands.general_commands.AwarenessManager")
    async def test_cmd_偷窺(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)

        req_ctx.msg = "!偷窺 <@{}>".format(req_ctx.user.id)
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.偷窺.done")
        self.assertIsInstance(res_ctx, UserContext)       

    @asynctest.mock.patch("thanatos.message_handlers.commands.general_commands.AwarenessManager")
    async def test_cmd_最愛排行(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)

        req_ctx.msg = "!最愛排行"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.最愛排行.done")
        self.assertIsInstance(res_ctx, FavRankingContext)       
