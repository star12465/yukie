# -*- coding: utf-8 -*-
import logging
import asynctest
from tests.common import *
from thanatos.message_handlers.command_handler import CommandHandler
from thanatos.managers import *
from thanatos.contexts import *
from thanatos.models import *

logger = logging.getLogger(__name__)

class TestMiscCommands(asynctest.TestCase):
    def setUp(self):
        self.db = BigDB()
        mocks.thanatos.loop = self.loop
        mocks.thanatos.db = self.db

        #init query self.handler
        with self.db.session() as db_session:
            self.handler = CommandHandler(mocks.thanatos)

    def tearDown(self):
        self.db.close()

    @asynctest.mock.patch("thanatos.message_handlers.commands.misc_commands.AwarenessManager")
    async def test_cmd_正義的大小(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)
        user = req_ctx.user

        #add valid objects
        req_ctx.msg = "!正義的大小"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.正義的大小.done")
        self.assertEqual(type(res_ctx), ResponseContext)       
