# -*- coding: utf-8 -*-
import asynctest
from tests.common import *
from thanatos.message_handlers import ChatHandler
from thanatos.contexts import *
from thanatos.models import *

class TestChatHandler(asynctest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    async def test_handle_message(self):
        a = 0
