# -*- coding: utf-8 -*-
import asynctest
from tests.common import *
from thanatos.message_handlers import CommandHandler
from thanatos.contexts import *
from thanatos.models import *

class TestCommandHandler(asynctest.TestCase):
    def setUp(self):
        self.db = BigDB()
        mocks.thanatos.loop = self.loop
        mocks.thanatos.db = self.db

        #init query self.handler
        with self.db.session() as db_session:
            self.handler = CommandHandler(mocks.thanatos)

    def tearDown(self):
        self.db.close()

    @asynctest.mock.patch("thanatos.message_handlers.command_handler.AwarenessManager")
    async def test_cmd_help(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.db)

        req_ctx.msg = "!help"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "command.help.done")
        self.assertIsInstance(res_ctx, ResponseContext)
