# -*- coding: utf-8 -*-
import asynctest
import time
from datetime import datetime
from tests.common import *
from thanatos.message_handlers.query_handler import QueryHandler
from thanatos.contexts import *
from thanatos.models import *

class TestQueryHandler(asynctest.TestCase):
    def setUp(self):
        self.big_db = BigDB()
        mocks.thanatos.loop = self.loop
        mocks.thanatos.db = self.big_db

        #init query self.handler
        with self.big_db.session() as db_session:
            self.handler = QueryHandler(mocks.thanatos)

    def tearDown(self):
        self.big_db.close()

    @asynctest.mock.patch("thanatos.message_handlers.query_handler.AwarenessManager")
    async def test_handle_message(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.big_db)

        #irrelevant
        req_ctx.msg = "??"
        await self.handler.handle_message(req_ctx)
        aware_mgr.activate.assert_not_called()
        req_ctx.msg = "?#a"
        await self.handler.handle_message(req_ctx)
        aware_mgr.activate.assert_not_called()
        req_ctx.msg = "? hello world"
        await self.handler.handle_message(req_ctx)
        aware_mgr.activate.assert_not_called()
        req_ctx.msg = "hello world"
        await self.handler.handle_message(req_ctx)
        aware_mgr.activate.assert_not_called()

        #game
        aware_mgr.reset_mock()
        req_ctx.msg = "?eden they  were"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, GalgameContext)
        with req_ctx.db.session() as db_session:
            galgame = db_session.merge(res_ctx.galgame, load=False)
            self.assertEqual(galgame.id, 11829)

        #allow full width question mark
        req_ctx.msg = "？死逝憎悪"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, GalgameContext)
        with req_ctx.db.session() as db_session:
            galgame = db_session.merge(res_ctx.galgame, load=False)
            self.assertEqual(galgame.id, 23562)

        #video
        req_ctx.msg = "?キサラギGoLd.StAR oP"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, VideoContext)
        with req_ctx.db.session() as db_session:
            video = db_session.merge(res_ctx.video, load=False)
            self.assertEqual(video.id, 257)
            self.assertEqual(video.galgame.id, 13423)

        #video (game name not found)
        req_ctx.msg = "?aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ed"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.not_found")
        self.assertEqual(type(res_ctx), ResponseContext)

        #video (video not found)
        req_ctx.msg = "?紙の上の魔法使い playvideo"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.not_found")
        self.assertEqual(type(res_ctx), ResponseContext)

        #video list (all)
        req_ctx.msg = "?rewrite 影片"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, VideosListContext)

        #video list (one)
        req_ctx.msg = "?loverec ops"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, VideosListContext)

        #creator
        aware_mgr.reset_mock()
        req_ctx.msg = "?秋野花"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, CreatorContext)
        with self.big_db.session() as db_session:
            creator = db_session.merge(res_ctx.creator)
            self.assertEqual(creator.id, 15203)

        #brand
        req_ctx.msg = "?key"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, BrandContext)
        with self.big_db.session() as db_session:
            brand = db_session.merge(res_ctx.brand)
            self.assertEqual(brand.id, 83)

        #character
        req_ctx.msg = "?風見一姫"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, CharacterContext)
        with self.big_db.session() as db_session:
            character = db_session.merge(res_ctx.character, load=False)
            actual_character = Character.get_by_name(db_session, "風見一姫")
            self.assertEqual(character.id, actual_character.id)

        #no timeout
        aware_mgr.reset_mock()
        def slow_query(req_ctx):
            time.sleep(1)
            return [ResponseContext(req_ctx)]
        self.handler.query = slow_query
        req_ctx.msg = "?key"
        await self.handler.handle_message(req_ctx)
        args = aware_mgr.activate.call_args_list
        (awareness, req_ctx2), _ = args[0]
        self.assertEqual(awareness, "query.start")
        self.assertEqual(req_ctx, req_ctx2)
        (awareness, res_ctx), _ = args[1]
        self.assertEqual(awareness, "query.done")
        self.assertEqual(type(res_ctx), ResponseContext)
        self.assertEqual(len(args), 2)

        #timeout
        aware_mgr.reset_mock()
        def slow_query(req_ctx):
            time.sleep(3)
            return [ResponseContext(req_ctx)]
        self.handler.query = slow_query
        req_ctx.msg = "?key"
        await self.handler.handle_message(req_ctx)
        args = aware_mgr.activate.call_args_list
        (awareness, req_ctx2), _ = args[0]
        self.assertEqual(awareness, "query.start")
        self.assertEqual(req_ctx, req_ctx2)
        (awareness, res_ctx), _ = args[1]
        self.assertEqual(awareness, "query.loading")
        self.assertEqual(type(res_ctx), ResponseContext)
        (awareness, res_ctx), _ = args[2]
        self.assertEqual(awareness, "query.done")
        self.assertEqual(type(res_ctx), ResponseContext)
        self.assertEqual(len(args), 3)

        #search
        req_ctx.msg = "??alcot"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, SearchResultsContext)
        matched = 0
        for (score, (target_type, target_id)) in res_ctx.search_results:
            if target_type == Brand and target_id in (1037, 2672, 2671):
                matched += 1
        self.assertEqual(matched, 3)

        #list games by year
        req_ctx.msg = "?2012年作"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, GalgamesMonthlyContext)
        self.assertEqual(res_ctx.year, 2012)
        self.assertEqual(res_ctx.month, None)
        req_ctx.msg = "?13年作"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, GalgamesMonthlyContext)
        self.assertEqual(res_ctx.year, 2013)
        self.assertEqual(res_ctx.month, None)

        #list games by month
        req_ctx.msg = "?2014年5月作"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, GalgamesMonthlyContext)
        self.assertEqual(res_ctx.year, 2014)
        self.assertEqual(res_ctx.month, 5)
        req_ctx.msg = "?98年6月作"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, GalgamesMonthlyContext)
        self.assertEqual(res_ctx.year, 1998)
        self.assertEqual(res_ctx.month, 6)
        now = datetime.now()
        req_ctx.msg = "?7月作"
        await self.handler.handle_message(req_ctx)
        (awareness, res_ctx), _ = aware_mgr.activate.call_args
        self.assertEqual(awareness, "query.done")
        self.assertIsInstance(res_ctx, GalgamesMonthlyContext)
        self.assertEqual(res_ctx.year, now.year)
        self.assertEqual(res_ctx.month, 7)

    @asynctest.mock.patch("thanatos.message_handlers.query_handler.AwarenessManager")
    async def test_query_help(self, aware_mgr):
        req_ctx = ut_utils.new_req_ctx(self.big_db)

        #init query self.handler
        req_ctx.msg = "?help"
        await self.handler.handle_message(req_ctx)
        self.assertEqual(aware_mgr.activate.call_args[0][0], "query.help.done")
