# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from tests.common import *
from thanatos.models import *
from thanatos.providers import *

class TestBase(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_updatable(self):
        with self.db.session() as db_session:
            galgame = Galgame.get_by_name(db_session, "ToHeart2 XRATED")
        earliest_timestamp = datetime.fromtimestamp(0)
        now = datetime.now()
        self.assertEqual(galgame.get_last_updated(self.db, ErogamescapeProvider, Galgame.Component.Core),
                                earliest_timestamp)
        galgame.mark_updated(self.db, ErogamescapeProvider, Galgame.Component.Core)
        self.assertGreater(galgame.get_last_updated(self.db, ErogamescapeProvider, Galgame.Component.Core),
                                now)
