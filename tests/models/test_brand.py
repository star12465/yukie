# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *

class TestBrand(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_get_by_name(self):
        with self.db.session() as db_session:
            a = Brand.get_by_name(db_session, "ωstar")
            self.assertEqual(a.id, 2408)
            self.assertEqual(a.name, "ωstar")
            a = Brand.get_by_name(db_session, "idon'texist")
            self.assertEqual(a, None)
