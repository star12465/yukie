# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.models import *

class TestCreator(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_set_as_main_identity(self):
        def assert_main_identity(creators, main_identity):
            for creator in creators:
                if creator == main_identity:
                    self.assertIsNone(creator.main_identity)
                else:
                    self.assertIsNotNone(creator.main_identity)

        def assert_user_fav(db_session, creators, main_identity, user):
            for creator in creators:
                q = db_session.query(UserFavorite).filter_by(user=user,
                                                         target_type=Creator.__name__,
                                                         target_id=creator.id)
                if creator == main_identity:
                    self.assertEqual(q.count(), 1)
                else:
                    self.assertEqual(q.count(), 0)


        with self.db.session() as db_session:
            a = Creator.get_by_name(db_session, "種崎敦美")
            b = Creator.get_by_name(db_session, "桐谷華")
            c = Creator.get_by_name(db_session, "卯衣")
            d = Creator.get_by_name(db_session, "門脇舞以")
            e = Creator.get_by_name(db_session, "大元静")
            f = Creator.get_by_name(db_session, "小倉結衣")
            all_identities = a.get_all_identities()

            #add a user and drop all favorites
            user = ut_utils.new_user(db_session)
            db_session.query(UserFavorite).delete()

            #changing main identity + favorite migration
            ut_utils.new_user_favs(db_session, Creator, user, a.id)
            self.assertEqual(a.main_identity, b)
            self.assertTrue(a.set_as_main_identity())
            assert_main_identity(all_identities, a)
            assert_user_fav(db_session, all_identities, a, user)

            #set main identity as main identity (no change)
            self.assertTrue(a.set_as_main_identity())
            assert_main_identity(all_identities, a)
            assert_user_fav(db_session, all_identities, a, user)

            #main identity joining another main identity + favorite merging
            all_identities += c.get_all_identities()
            self.assertIsNone(c.main_identity)
            self.assertTrue(a.set_as_main_identity([c]))
            assert_main_identity(all_identities, a)
            assert_user_fav(db_session, all_identities, a, user)

            #user should only have one favorite now
            q = db_session.query(UserFavorite).filter_by(user=user, target_type=Creator.__name__)
            self.assertEqual(q.count(), 1)

            #main identity joining another sub identity
            #+joining with creators already marked as other identities
            all_identities += d.get_all_identities()
            self.assertIsNotNone(d.main_identity)
            self.assertTrue(a.set_as_main_identity([a, b, c, d]))
            assert_main_identity(all_identities, a)

            #sub identity joining another main identity
            all_identities += e.get_all_identities()
            self.assertIsNotNone(e.main_identity)
            self.assertTrue(e.set_as_main_identity([b]))
            assert_main_identity(all_identities, e)

            #fix_main_identity flag
            e.fix_main_identity = True
            self.assertTrue(b.set_as_main_identity())
            assert_main_identity(all_identities, e)
            self.assertTrue(e.fix_main_identity)

            #fix_main_identity flag with conflict
            f.fix_main_identity = True
            self.assertFalse(e.set_as_main_identity([f]))
            assert_main_identity(e.get_all_identities(), e)
            assert_main_identity(f.get_all_identities(), f)
            self.assertTrue(e.fix_main_identity)
            self.assertTrue(f.fix_main_identity)

            #use force flag to resolve conflicts
            ut_utils.new_user_favs(db_session, Creator, user, f.id)
            all_identities += f.get_all_identities()
            self.assertTrue(c.set_as_main_identity([f], force=True))
            assert_main_identity(all_identities, c)
            assert_user_fav(db_session, all_identities, c, user)
            self.assertFalse(e.fix_main_identity)
            self.assertFalse(f.fix_main_identity)
            self.assertTrue(c.fix_main_identity)

            #user should only have one favorite now
            q = db_session.query(UserFavorite).filter_by(user=user, target_type=Creator.__name__)
            self.assertEqual(q.count(), 1)
