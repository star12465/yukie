# -*- coding: utf-8 -*-
import logging
import unittest
import sqlalchemy
from datetime import date
from tests.common import *
from thanatos.models import *
from thanatos.utils import *
from thanatos.providers import ErogamescapeProvider

class TestErogamescapeProvider(unittest.TestCase):
    def setUp(self):
        self.db = EmptyDB()
        self.provider = ErogamescapeProvider(self.db)

    def tearDown(self):
        self.db.close()

    def test_update_galgames(self):
        #update one galgame
        with self.db.session(autoflush=False) as db_session:
            galgame = Galgame(id=15964)
            self.provider.update_galgames([galgame], Galgame.Component.Core)
            #refresh galgame
            galgame = db_session.query(Galgame).filter_by(id=galgame.id).one()
            self.assertEqual(galgame.brand_id, 509)
            creator = db_session.query(Creator).filter_by(id=15341).one()
            self.assertEqual(creator.works[0].galgame, galgame)

    def test_sql_results_to_metadata(self):
        return
        galgame = Galgame(id=1337)
        sql = '''SELECT
                    {} AS target_id,
                    2 AS key,
                    3 AS namespace,
                    true AS "value:boolean"'''.format(galgame.id)
        self.provider.sql_results_to_metadata(sql, Galgame, namespace="abc")
        self.assertEqual(Metadata(self.db, galgame, "3")["2"], True)

    def test_sql_results_to_model(self):
        return
        self.provider.sql_results_to_model(Brand, "select 123 as id, 'abc' as name, 'zzz' as normalized_name")

        with self.db.session() as db_session:
            a = db_session.query(Brand).filter_by(id=123).one()
            self.assertEqual(a.name, "abc")
            self.assertEqual(a.normalized_name, "zzz")

        #basic test of error handler
        def error_handler(db_session, ex, obj):
            obj.name = "rainbow"
            db_session.merge(obj)
        self.provider.sql_results_to_model(Brand, "select 345 as id, 'abc' as name, 'yyy' as normalized_name", error_handler)

        with self.db.session() as db_session:
            a = db_session.query(Brand).filter_by(id=123).one()
            self.assertEqual(a.name, "abc")
            self.assertEqual(a.normalized_name, "zzz")
            a = db_session.query(Brand).filter_by(id=345).one()
            self.assertEqual(a.name, "rainbow")
            self.assertEqual(a.normalized_name, "yyy")

        #make sure we get raise an integrity error when two brands have the same name
        #and test updating existing record (same primary key) works
        with self.assertRaises(sqlalchemy.exc.IntegrityError):
            self.provider.sql_results_to_model(Brand, "select 789 as id, 'abc' as name, 'xxx' as normalized_name")
        self.provider.sql_results_to_model(Brand, "select 345 as id, 'reindeer' as name, 'uuu' as normalized_name")

        with self.db.session() as db_session:
            a = db_session.query(Brand).filter_by(id=345).one()
            self.assertEqual(a.name, "reindeer")
            self.assertEqual(a.normalized_name, "uuu")

        def error_handler(db_session, ex, obj):
            a = db_session.query(Brand).filter_by(name=obj.name).one()
            a.name = "horse"
            db_session.merge(obj)
        self.provider.sql_results_to_model(Brand, "select 555 as id, 'abc' as name, 'hhh' as normalized_name", error_handler)

        #rename the other object
        with self.db.session() as db_session:
            a = db_session.query(Brand).filter_by(id=123).one()
            self.assertEqual(a.name, "horse")
            self.assertEqual(a.normalized_name, "zzz")
            a = db_session.query(Brand).filter_by(id=345).one()
            self.assertEqual(a.name, "reindeer")
            self.assertEqual(a.normalized_name, "uuu")
            a = db_session.query(Brand).filter_by(id=555).one()
            self.assertEqual(a.name, "abc")
            self.assertEqual(a.normalized_name, "hhh")
