# -*- coding: utf-8 -*-
import logging
import unittest
from tests.common import *
from thanatos.models import *
from thanatos.providers import VndbProvider

class TestVndbProvider(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()
        self.provider = VndbProvider(self.db)

    def tearDown(self):
        self.db.close()

    def test_update_galgame(self):
        #a game with an ambiguous name that has been released
        with self.db.session() as db_session:
            galgame = Galgame.get_by_name(db_session, "rewrite")
            galgame.vndb_id = None
        self.provider.update_galgame(galgame, Galgame.Component.Core)
        with self.db.session() as db_session:
            galgame = db_session.merge(galgame, load=False)
            db_session.refresh(galgame)
            self.assertEqual(galgame.vndb_id, 751)

        #a game with a future release date
        with self.db.session() as db_session:
            galgame = Galgame.get_by_name(db_session, "太陽の子")
            galgame.vndb_id = None
        self.provider.update_galgame(galgame, Galgame.Component.Core)
        with self.db.session() as db_session:
            galgame = db_session.merge(galgame, load=False)
            db_session.refresh(galgame)
            self.assertEqual(galgame.vndb_id, 3097)

    def test_update_creator(self):
        names = ["山羽みんと", "清水愛", "三菱アイ", "秋野花"]
        creators = []
        for name in names:
            with self.db.session(expire_on_commit=False) as db_session:
                creator = Creator.get_by_name(db_session, name)
                creator.main_identity = None
                creator.other_identities = []
                db_session.commit()
                db_session.expunge(creator)
                creators.append(creator)

        self.provider.update_creator(creators[0], Creator.Component.Core)
        with self.db.session() as db_session:
            for i in range(4):
                creators[i] = db_session.merge(creators[i], False)
                db_session.refresh(creators[i])
            a = creators[0]
            self.assertEqual(a.vndb_id, 416)
            d = a.main_identity if a.main_identity else a
            self.assertGreater(len(d.other_identities), 45)

            all_identities = d.other_identities+[d]
            for i in range(3):
                self.assertTrue(creators[i] in all_identities)
            self.assertTrue(creators[3] not in all_identities)
