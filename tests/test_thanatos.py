import logging
import time
import asyncio
import unittest
import threading
import asynctest
import concurrent.futures
from tests.common import *
from thanatos.thanatos import *
from thanatos.contexts import *
from thanatos.message_handlers import *

__all__ = ["TestThanatos"]

logger = logging.getLogger(__name__)

class MessageHandlerTest(MessageHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lock = threading.RLock()
        self.ctx_processed = []

    async def handle_message(self, req_ctx):
        self.lock.acquire()
        self.ctx_processed.append(req_ctx)
        self.lock.release()

class ThanatosTest(Thanatos):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.msg_received = []
        self.lock = threading.RLock()

        self.is_ready = asyncio.Event(loop=self.loop)

    async def on_ready(self):
        await super().on_ready()
        self.unittest_channel = self.get_channel(str(self.config.discord_unittest_channel_id))

        #add our own handler
        self.test_msg_handler = MessageHandlerTest(self)
        self.message_handlers.append(self.test_msg_handler)

        self.is_ready.set()

    def clear_ctx_processed(self):
        self.test_msg_handler.lock.acquire()
        self.test_msg_handler.ctx_processed.clear()
        self.test_msg_handler.lock.release()

    def get_ctx_processed(self):
        self.test_msg_handler.lock.acquire()
        ctx_processed = self.test_msg_handler.ctx_processed[:]
        self.test_msg_handler.lock.release()
        return ctx_processed

    def clear_msg_received(self):
        self.lock.acquire()
        self.msg_received.clear()
        self.lock.release()

    def get_msg_received(self):
        self.lock.acquire()
        msg_received = self.msg_received[:]
        self.lock.release()
        return msg_received

    async def on_message(self, msg):
        self.lock.acquire()
        self.msg_received.append(msg)
        self.lock.release()
        await super().on_message(msg)

    async def wait_until_ready(self):
        await self.is_ready.wait()

    async def logout(self):
        self.is_ready.clear()
        await super().logout()

class TestThanatos(asynctest.TestCase):
    async def setUp(self):
        self.db = BigDB()
        self.thanatos = ThanatosTest("tests/config.yaml", loop=self.loop)
        self.thanatos.db = self.db
        asyncio.ensure_future(self.thanatos.start(self.thanatos.config.discord_token), loop=self.loop)
        await self.thanatos.wait_until_ready()

    async def tearDown(self):
        await self.thanatos.close()
        self.db.close()

    async def test_listeners(self):
        def test_listeners():
            req_ctx = ut_utils.new_req_ctx(self.db, channel=self.thanatos.unittest_channel)
            req_ctx.thanatos = self.thanatos

            #test wait for with passthru=True
            def criteria(req_ctx):
                return req_ctx.user.id == int(self.thanatos.user.id) and  \
                        req_ctx.discord_msg.channel.id == self.thanatos.unittest_channel.id
            wait_for = self.thanatos.wait_for(["message"], criteria, True)
            self.assertRaises(TimeoutError, wait_for.get_message, 0)
            game_names = ["リトルバスターズ! エクスタシー", "穢翼のユースティア", "Rewrite"]
            res_ctxs = []
            for game_name in game_names:
                res_ctx = ResponseContext(req_ctx, mention=False)
                res_ctx.msg = "?{}".format(game_name)
                res_ctx.send()
                time.sleep(1)
                res_ctxs.append(res_ctx)
            found_queries, found_embeds = 0, 0
            while found_queries < len(game_names) or found_embeds < len(game_names):
                res_ctx = wait_for.get_message(5)
                if res_ctx.msg in [x.msg for x in res_ctxs]:
                    found_queries += 1
                    continue
                embeds = res_ctx.discord_msg.embeds
                if len(embeds) > 0:
                    for game_name in game_names:
                        if game_name in embeds[0]["title"]:
                            found_embeds += 1
                            break
            wait_for.delete()
            #make sure we don't get any more messages after delete
            self.thanatos.clear_ctx_processed()
            res_ctx = ResponseContext(req_ctx)
            res_ctx.msg = "test"
            res_ctx.send().result()
            self.assertRaises(TimeoutError, wait_for.get_message, 1)
            time.sleep(0.5)
            self.assertGreater(len(self.thanatos.get_ctx_processed()), 0)

            #test wait for with passthru=False
            self.thanatos.clear_ctx_processed()
            wait_for = self.thanatos.wait_for(["message"], criteria, False)
            res_ctx = ResponseContext(req_ctx)
            res_ctx.msg = "test_1"
            res_ctx.send().result()
            res_ctx = wait_for.get_message(1)
            time.sleep(0.5)
            self.assertEqual(len(self.thanatos.get_ctx_processed()), 0)
            #make sure delete sends the message back to message queue
            self.thanatos.clear_msg_received()
            res_ctx = ResponseContext(req_ctx)
            res_ctx.msg = "test_2"
            res_ctx.send().result()
            while len(self.thanatos.get_msg_received()) == 0:
                time.sleep(0.5)
            self.assertEqual(len(self.thanatos.get_ctx_processed()), 0)
            wait_for.delete()
            while len(self.thanatos.get_ctx_processed()) == 0:
                time.sleep(0.5)

            time.sleep(2)

            #test callback
            self.thanatos.clear_ctx_processed()
            threading_event = threading.Event()
            def criteria(req_ctx):
                return req_ctx.discord_msg.id == res_ctx.discord_msg.id
            async def callback(event, req_ctx):
                self.assertIn(event, ("reaction_add", "reaction_remove"))
                threading_event.set()
            cb1 = self.thanatos.add_callback_for(["reaction_add"], criteria, callback)
            cb2 = self.thanatos.add_callback_for(["reaction_remove"], criteria, callback)
            asyncio.run_coroutine_threadsafe(self.thanatos.add_reaction(res_ctx.discord_msg, "😊"),
                                             self.thanatos.loop)
            self.assertTrue(threading_event.wait(2))
            threading_event.clear()
            asyncio.run_coroutine_threadsafe(self.thanatos.remove_reaction(res_ctx.discord_msg, "😊", self.thanatos.user),
                                             self.thanatos.loop)
            self.assertTrue(threading_event.wait(2))

            time.sleep(2)

            #test delete and autodestruct
            async def on_destruct():
                threading_event.set()
            threading_event.clear()
            self.assertEqual(len(self.thanatos.event_listeners), 2)
            cb1.delete()
            cb2.auto_destruct(3, on_destruct)
            self.assertFalse(threading_event.wait(2))       #this should timeout
            self.assertTrue(threading_event.wait(2))
            self.assertEqual(len(self.thanatos.event_listeners), 0)
        await self.loop.run_in_executor(self.thanatos.executor, test_listeners)

class TestThanatosMainLoop(unittest.TestCase):
    def setUp(self):
        self.loop = asyncio.get_event_loop()
        self.db = BigDB()
        self.thanatos = ThanatosTest("tests/config.yaml", loop=self.loop)
        self.thanatos.db = self.db

    def tearDown(self):
        self.loop.run_until_complete(self.thanatos.close())
        self.db.close()

    def test_main_loop_crash(self):
        self.thanatos.start2 = self.thanatos.start
        async def start_and_crash(discord_token):
            asyncio.ensure_future(self.thanatos.start2(self.thanatos.config.discord_token), loop=self.loop)
            await self.thanatos.wait_until_ready()
            await asyncio.sleep(1)
            raise discord.DiscordException("test")
        self.thanatos.start = start_and_crash
        self.thanatos.main_loop()
        self.assertEqual(self.thanatos.reconnect_tries, self.thanatos.MAX_RECONNECT_TRIES)
