# -*- coding: utf-8 -*-
import time
import unittest
import threading
from sqlalchemy import Table
from sqlalchemy.exc import NoSuchTableError
from sqlalchemy.sql.expression import literal_column
from tests.common import *
from thanatos.models import *

class TestUtilsDB(unittest.TestCase):
    def setUp(self):
        self.db = EmptyDB()

    def tearDown(self):
        self.db.close()
   
    def test_table_creation(self):
        metadata = Base.metadata
        try:
            Table("galgames", metadata, autoload=True)
        except NoSuchTableError:
            self.fail("galgame table not created")

    def test_session(self):
        #session in session in the same thread
        #this should not block
        with self.db.session():
            with self.db.session():
                pass

        #sessions in different threads
        #this should block
        def enter_leave_db_session():
            with self.db.session() as db_session:
                #query something to start transaction
                db_session.query(literal_column("1")).all()
        t = threading.Thread(target=enter_leave_db_session)
        with self.db.session() as db_session:
            #query something to start transaction
            db_session.query(literal_column("1")).all()
            t.start()
            t.join(1)
            self.assertTrue(t.is_alive())
        #once we leave the other session should be able to enter immeidately
        t.join(1)
        self.assertFalse(t.is_alive())
