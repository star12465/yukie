# -*- coding: utf-8 -*-
import unittest
from thanatos.utils.fast_query import *
from thanatos.models import *
from tests.common import *

class TestFastQuery(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()
        self.fq = FastQuery()
        with self.db.session() as db_session:
            self.fq.add_dataset((game.name, game.id) for game in db_session.query(Galgame).all())

    def tearDown(self):
        self.db.close()

    def test_find_most_likely(self):
        for algorithm in FastQueryAlgorithm:
            self.fq.set_search_algorithm(algorithm)
            self.assertEqual(self.fq.find_most_likely("戀櫻"), 17088)
            self.assertEqual(self.fq.find_most_likely("relief"), 23563)
            self.assertEqual(self.fq.find_most_likely("aaaaaaaaaaaaaaaaaaaaa"), None)

        #precise should be able to find this
        self.assertEqual(self.fq.find_most_likely("aaサクラノ詩b森の上cc"), 4529)
