# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.utils import *
from thanatos.models import *

class TestMetadata(unittest.TestCase):
    def setUp(self):
        self.db = BigDB()

    def tearDown(self):
        self.db.close()

    def test_basic(self):
        user = ut_utils.get_a_user(self.db)
        metadata = Metadata(self.db, user, "test")
        self.assertIsNone(metadata["1"])
        metadata["1"] = "a"
        self.assertEqual(metadata["1"], "a")
        with Metadata(self.db, user, "test") as metadata:
            self.assertEqual(metadata["1"], "a")
            metadata["2"] = 2
            self.assertEqual(metadata["2"], 2)
            metadata["2"] += 1
            self.assertEqual(metadata["2"], 3)
        self.assertEqual(metadata.get("2"), 3)
        self.assertEqual(metadata.get("3", "abc"), "abc")
