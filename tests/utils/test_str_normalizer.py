# -*- coding: utf-8 -*-
import unittest
from tests.common import *
from thanatos.utils.str_normalizer import StringNormalizer, normalize_str

class TestStringNormalizer(unittest.TestCase):
    def test_normalize_case(self):
        self.assertEqual(StringNormalizer.normalize_case("測試ABab123!@＃$ "), "測試abab123!@＃$ ")

    def test_normalize_kanjis(self):
        self.assertEqual(StringNormalizer.normalize_kanjis("恋aあ０1%我發"), StringNormalizer.normalize_kanjis("戀aあ０1%我发"))

        #these should normalize to the same kanji
        x = StringNormalizer.normalize_kanjis("發")
        self.assertEqual(StringNormalizer.normalize_kanjis("發髮发発"), x*4)

    def test_remove_symbols(self):
        self.assertEqual(StringNormalizer.remove_symbols("”%我1０xX％♪"), "我1０xX")
        self.assertEqual(StringNormalizer.remove_symbols(""), "")

    def test_normalize(self):
        self.assertEqual(StringNormalizer.normalize("<baldr sky dive1 ”lost memo>"), "baldrskydive1lostmemo")
        self.assertEqual(normalize_str("<BALDR SKY Dive1 ”Lost Memo>"), "baldrskydive1lostmemo")
