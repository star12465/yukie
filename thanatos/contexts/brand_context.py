# -*- coding: utf-8 -*-
import discord
from math import ceil
from datetime import date
from thanatos.models import *
from .context import ResponseContext

__all__ = ["BrandContext"]

class BrandContext(ResponseContext):
    def __init__(self, req_ctx, brand):
        super().__init__(req_ctx)
        self.brand = brand

    def preprocess(self):
        self.discord_embed = discord.Embed(colour=0xffff99)

        def describe_games(current_page):
            with self.req_ctx.db.session() as db_session:
                self.brand = db_session.merge(self.brand, load=False)

                #get games for this page
                total_games = len(self.brand.galgames)
                total_pages = ceil(total_games/self.MaxLinesPerPage)
                if current_page > total_pages:
                    current_page = total_pages

                res = []
                galgames = self.brand.galgames[
                                (current_page-1)*self.MaxLinesPerPage:current_page*self.MaxLinesPerPage]
                for galgame in galgames:
                    if galgame.release_date == date(2030, 1, 1):
                        release_date = _("發售日未定")
                    else:
                        release_date = galgame.release_date.strftime("%Y-%m-%d")
                    popular = True if galgame.votes_ergs_count > 50 and galgame.votes_ergs_median >= 80 else False
                    if galgame.votes_ergs_count > 0:
                        rating = "{}({})".format(galgame.votes_ergs_median, galgame.votes_ergs_count)
                    else:
                        rating = "\t-\t"

                    #is favorite?
                    galgame_name = galgame.name
                    if galgame.is_fav_by_user(self.req_ctx.user):
                        galgame_name = "♥" + galgame_name

                    res.append("{}{}\t{}\t{}{}".format("**" if popular else "",
                                                           release_date,
                                                           rating,
                                                           galgame_name,
                                                           "**" if popular else ""))
            return (current_page, total_pages, "\n".join(res))

        with self.req_ctx.db.session() as db_session:
            self.brand = db_session.merge(self.brand, load=False)

            if self.brand.kind == "CIRCLE":
                name = "{} ({})".format(self.brand.name, _("同人"))
            else:
                name = "{}".format(self.brand.name)
            if self.req_ctx.guild:
                fav_count = self.brand.get_fav_count(self.req_ctx.guild)
                if fav_count > 0:
                    name += " (❤×{})".format(fav_count)
            self.discord_embed.set_author(name=name)
            self.discord_embed.url = self.brand.official_url
            self.discord_embed.description = "http://erogamescape.dyndns.org/~ap2/ero/toukei_kaiseki/brand.php?brand={}".format(self.brand.id)

            self.add_field_pagination(_("歷代作品"), describe_games)
        super().preprocess()
