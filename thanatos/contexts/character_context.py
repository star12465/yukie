# -*- coding: utf-8 -*-
import discord
from thanatos.models import *
from .context import ResponseContext

__all__ = ["CharacterContext"]

class CharacterContext(ResponseContext):
    def __init__(self, req_ctx, character):
        super().__init__(req_ctx)
        self.character = character 

    def preprocess(self):
        self.discord_embed = discord.Embed(colour=0xFE2E64)
        #self.discord_embed.set_author(name=self.user.username)

        with self.req_ctx.db.session() as db_session:
            self.character = db_session.merge(self.character, load=False)

            b, w, h = self.character.bust_size, self.character.waist_size, self.character.hip_size
            if b or w or h:
                three_size = "{}-{}-{}".format(b if b else _("未知"),
                                               w if w else _("未知"),
                                               h if h else _("未知"))
                self.discord_embed.description = three_size
            else:
                self.discord_embed.description = _("未知")

        super().preprocess()
