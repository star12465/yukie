import discord
import asyncio
import logging
import time

__all__ = ["ResponseContext", "RequestContext",
            "ContextError", "EmptyContextError"]

logger = logging.getLogger(__name__)

class ContextError(Exception):
    pass

class EmptyContextError(ContextError):
    pass

class Context(object):
    def __init__(self):
        msg = None

    def delete(self):
        return asyncio.run_coroutine_threadsafe(self.delete_async(), self.thanatos.loop)

    async def delete_async(self):
        if self.discord_msg:
            return await self.thanatos.delete_message(self.discord_msg)

    def edit(self):
        async def edit_async():
            if self.discord_msg:
                return await self.thanatos.edit_message(self.discord_msg,
                                                        self.discord_content,
                                                        embed=self.discord_embed)
        self.preprocess()
        fut = asyncio.run_coroutine_threadsafe(edit_async(), self.thanatos.loop)
        fut.add_done_callback(self.postprocess)
        return fut

    def __repr__(self):
        if hasattr(self, "msg"):
            return "<{} msg={}>".format(self.__class__.__name__, self.msg[:30].replace("\n", " "))
        else:
            return "<{}>".format(self.__class__.__name__)

class ResponseContext(Context):
    MaxLinesPerPage = 12

    def __init__(self, req_ctx, *args, **kwargs):
        super().__init__()
        self.req_ctx = req_ctx
        self.thanatos = self.req_ctx.thanatos
        self.mention = True

        for k, v in kwargs.items():
            setattr(self, k, v)

    def should_mention(self, mention):
        self.mention = mention

    def add_field_pagination(self, name, callback, *args, delim=None):
        self._pagination_name = name
        self._pagination_callback = callback
        self._pagination_callback_args = args

        if not hasattr(self, "_pagination_current_page"):
            self._pagination_current_page = 1
        if not hasattr(self, "_pagination_last_paged"):
            self._pagination_last_paged = 0

    def preprocess(self):
        #channel
        if not hasattr(self, "discord_channel"):
            self.discord_channel = self.req_ctx.discord_msg.channel

        #mention the person we're responding to
        if self.mention:
            mention_str = "<@{}> ".format(self.req_ctx.user.id)
        else:
            mention_str = ""

        #set discord content
        if hasattr(self, "error_msg"):
            self.discord_content = "{}{}".format(mention_str, self.error_msg)
        elif hasattr(self, "msg"):
            self.discord_content = "{}{}".format(mention_str, self.msg)
        else:
            self.discord_content = None

        #set embedded
        self.discord_embed = self.discord_embed if hasattr(self, "discord_embed") else None

        #make sure we're sending something
        if (not self.discord_content or self.discord_content.strip() == "") and not self.discord_embed:
            raise EmptyContextError()

        #add pagination
        if self.discord_embed and hasattr(self, "_pagination_name"):
            if hasattr(self, "_pagination_callback"):
                args = self._pagination_callback_args
                current_page, total_pages, value = self._pagination_callback(self._pagination_current_page, *args)
                self._pagination_current_page = current_page
                self._pagination_total_pages = total_pages

                pagination_text = " ({}/{})".format(current_page, total_pages) if total_pages > 1 else ""
                self.discord_embed.add_field(
                        name="{}{}".format(self._pagination_name, pagination_text),
                        value=value
                )

    def postprocess(self, fut):
        #if an exception occurred, we want to make sure we log them
        if fut.exception():
            logger.error("postprocess raised an exception", exc_info=fut.exception())
            return

        #create pagination buttons (reactions)
        if self.discord_embed and hasattr(self, "_pagination_last_paged") and \
                                    self._pagination_last_paged == 0 and \
                                    self._pagination_total_pages > 1:
            #add pagination reactions
            async def add_pagination_reactions():
                await self.thanatos.add_reaction(self.discord_msg, "⬅")
                await self.thanatos.add_reaction(self.discord_msg, "➡")
            asyncio.run_coroutine_threadsafe(add_pagination_reactions(), self.thanatos.loop)
            self._pagination_last_paged = time.time()

            #listen for event
            async def on_reaction(event, req_ctx):
                #ignore ourselves
                if req_ctx.discord_reaction_user == self.thanatos.user:
                    return
                current_page = self._pagination_current_page

                #left arrow
                if req_ctx.discord_reaction.emoji == "⬅":
                    if current_page > 1:
                        current_page -= 1
                #right arrow
                elif req_ctx.discord_reaction.emoji == "➡":
                    if current_page < self._pagination_total_pages:
                        current_page += 1

                #current page changed?
                if current_page == self._pagination_current_page:
                    return

                #we only allow changing pages once every 0.8 seconds
                current_time = time.time()
                if current_time - self._pagination_last_paged < 0.8:
                    return
                self._pagination_last_paged = current_time
                self._pagination_current_page = current_page
                self.edit()
                return True
            async def on_destruct():
                await self.thanatos.clear_reactions(self.discord_msg)
            def criteria(req_ctx):
                return self.discord_msg.id == req_ctx.discord_msg.id
            reaction_cb = self.thanatos.add_callback_for(["reaction_add", "reaction_remove"], criteria, on_reaction)
            reaction_cb.auto_destruct(5 * 60, on_destruct)

    def send(self):
        async def send_async():
            if hasattr(self, "presend_async"):
                await self.presend_async(self)
            self.discord_msg = await self.thanatos.send_message(self.discord_channel,
                                                                self.discord_content,
                                                                embed=self.discord_embed)
            if hasattr(self, "postsend_async"):
                await self.postsend_async(self)
        self.preprocess()
        fut = asyncio.run_coroutine_threadsafe(send_async(), self.thanatos.loop)
        fut.add_done_callback(self.postprocess)
        return fut

class RequestContext(Context):
    pass
