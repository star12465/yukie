# -*- coding: utf-8 -*-
import discord
from math import ceil
from thanatos.models import *
from .context import ResponseContext

__all__ = ["CreatorContext"]

class CreatorContext(ResponseContext):
    def __init__(self, req_ctx, creator):
        super().__init__(req_ctx)
        self.creator = creator

    def preprocess(self):
        def describe_works(current_page):
            with self.req_ctx.db.session() as db_session:
                self.creator = db_session.merge(self.creator, load=False)
                all_works = self.creator.get_works_rating_sorted()

                total_works = len(all_works)
                total_pages = ceil(total_works/self.MaxLinesPerPage)
                if current_page > total_pages:
                    current_page = total_pages

                res = []
                works = all_works[(current_page-1)*self.MaxLinesPerPage:current_page*self.MaxLinesPerPage]
                for work in works:
                    if work.type in (WorkType.CV, WorkType.Singer):
                        if work.level == WorkLevel.Main:
                            note = "__**{}**__".format(work.note)
                        else:
                            note = work.note
                    else:
                        note = WorkType.to_string(work.type)
                    if not note:
                        note = _("其他")

                    #is favorite?
                    galgame_name = work.galgame.name
                    if work.galgame.is_fav_by_user(self.req_ctx.user):
                        galgame_name = "♥" + galgame_name
                    res.append("{} ({})".format(galgame_name, note))
                return (current_page, total_pages, "\n".join(res))

        self.discord_embed = discord.Embed(colour=0xDEADBF)
        with self.req_ctx.db.session() as db_session:
            self.creator = db_session.merge(self.creator, load=False)

            main_identity = self.creator.main_identity if self.creator.main_identity else self.creator
            name = "{} ({})".format(main_identity.name, ", ".join(WorkType.to_string(wt) for wt in main_identity.get_types()))
            if self.req_ctx.guild:
                fav_count = main_identity.get_fav_count(self.req_ctx.guild)
                if fav_count > 0:
                    name += " (❤×{})".format(fav_count)
            self.discord_embed.set_author(name=name)

            #if our title is too long discord throws an error
            identities_str = "".join("={}".format(c.name) for c in main_identity.other_identities)
            if len(identities_str) <= 256:
                self.discord_embed.title = identities_str
            else:
                #resort to using fields
                self.discord_embed.add_field(name=_("馬甲"), value=identities_str)

            #official url
            self.discord_embed.url = main_identity.homepage_url

            #urls
            self.discord_embed.description = ""
            if main_identity.twitter_id:
                self.discord_embed.description += "https://twitter.com/{}\n".format(main_identity.twitter_id)
            self.discord_embed.description += "http://erogamescape.dyndns.org/~ap2/ero/toukei_kaiseki/creater.php?creater={}".format(main_identity.id)

            self.add_field_pagination(_("歷代作品"), describe_works)
        super().preprocess()
