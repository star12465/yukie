# -*- coding: utf-8 -*-
import discord
import sqlalchemy
from datetime import date
from thanatos.contexts import ResponseContext
from thanatos.models import *

__all__ = ["FavRankingContext"]

class FavRankingContext(ResponseContext):
    def __init__(self, req_ctx, guild):
        super().__init__(req_ctx)
        self.guild = guild

    def preprocess(self):
        self.discord_embed = discord.Embed(colour=0xA9BCF5)

        def list_fav_ranking(db_session, guild, target_type):
            favorites = db_session.query(UserFavorite.target_id, sqlalchemy.func.count("*")) \
                                  .filter(UserFavorite.target_type==target_type.__name__) \
                                  .group_by(UserFavorite.target_id)    \
                                  .order_by(sqlalchemy.func.count("*").desc()).limit(5).all()
            favorite_strs = []
            for target_id, count in favorites:
                obj = db_session.query(target_type).filter_by(id=target_id).one()
                favorite_strs.append("{} (♥×{})".format(obj.name, count))
            return "\n".join(favorite_strs)

        with self.req_ctx.db.session() as db_session:
            fav_brands = list_fav_ranking(db_session, self.guild, Brand)
            if len(fav_brands) > 0:
                self.discord_embed.add_field(name=_("最愛的公司"), value=fav_brands)
            fav_creators = list_fav_ranking(db_session, self.guild, Creator)
            if len(fav_creators) > 0:
                self.discord_embed.add_field(name=_("最愛的創作者"), value=fav_creators)
            fav_galgames = list_fav_ranking(db_session, self.guild, Galgame)
            if len(fav_galgames) > 0:
                self.discord_embed.add_field(name=_("最愛的遊戲"), value=fav_galgames)
        super().preprocess()
