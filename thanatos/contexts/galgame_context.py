# -*- coding: utf-8 -*-
import discord
import re
from datetime import date
from thanatos.models import *
from thanatos.utils import *
from .context import ResponseContext

__all__ = ["GalgameContext"]

class GalgameContext(ResponseContext):
    def __init__(self, req_ctx, galgame):
        super().__init__(req_ctx)
        self.galgame = galgame

    def preprocess(self):
        embedded = discord.Embed(colour=0x4EADBF)

        with self.req_ctx.db.session() as db_session:
            self.galgame = db_session.merge(self.galgame, load=False)

            #brand name
            if self.galgame.brand.kind == "CIRCLE":
                brand_name = "{} ({})".format(self.galgame.brand.name, _("同人"))
            else:
                brand_name = "{}".format(self.galgame.brand.name)
            embedded.set_author(name=brand_name)

            #game name
            if self.galgame.release_date == date(2030, 1, 1):
                release_date = _("發售日未定")
            else:
                release_date = self.galgame.release_date.strftime("%Y-%m-%d")
            galgame_name = self.galgame.name
            if not self.galgame.is_adult_rated:
                title = "{} ({}) ({})".format(galgame_name, _("非18禁"), release_date)
            else:
                title = "{} ({})".format(galgame_name, release_date)
            if self.req_ctx.guild:
                fav_count = self.galgame.get_fav_count(self.req_ctx.guild)
                if fav_count > 0:
                    title += " (♥×{})".format(fav_count)
            embedded.title = title

            #official url
            embedded.url = self.galgame.official_url

            #urls
            description = ""
            if self.galgame.getchu_id:
                description += "http://www.getchu.com/soft.phtml?id={}\n".format(self.galgame.getchu_id)
            description += "http://erogamescape.dyndns.org/~ap2/ero/toukei_kaiseki/game.php?game={}".format(self.galgame.id)
            embedded.description = description

            def get_staffs(req_ctx, work_type, work_levels=None, show_note=True, show_sub=True):
                res = []
                for work in self.galgame.staffs:
                    if work.type == work_type:
                        #do we have a work level requirement
                        if work_levels != None:
                            if work.level not in work_levels:
                                continue

                        creator = work.creator.main_identity if work.creator.main_identity else work.creator
                        creator_name = re.sub(r'\(.*\)$', '', creator.name)

                        notes = []
                        if show_sub and work.level == WorkLevel.Sub:
                            notes.append(_("輔助"))

                        if show_note and work.note:
                            notes.append(work.note)

                        if creator.is_fav_by_user(req_ctx.user):
                            creator_name = "♥" + creator_name

                        if len(notes) > 0:
                            notes_str = ", ".join(notes)
                            res.append("{} ({})".format(creator_name, notes_str))
                        else:
                            res.append(creator_name)
                return ", ".join(res) if len(res) > 0 else None

            #main staffs
            story_writers = get_staffs(self.req_ctx, WorkType.Story)
            if story_writers:
                embedded.add_field(name=_("劇本"), value=story_writers)
            artists = get_staffs(self.req_ctx, WorkType.Artist)
            if artists:
                embedded.add_field(name=_("畫師"), value=artists)
            main_cvs = get_staffs(self.req_ctx, WorkType.CV, [WorkLevel.Main], True)
            if main_cvs:
                embedded.add_field(name=_("主角群CV"), value=main_cvs)

            sub_cvs = get_staffs(self.req_ctx, WorkType.CV, [WorkLevel.Sub, WorkLevel.Other], True, False)
            if sub_cvs:
                embedded.add_field(name=_("配角群CV"), value=sub_cvs)
            singers = get_staffs(self.req_ctx, WorkType.Singer, show_sub=False)
            if singers:
                embedded.add_field(name=_("歌手"), value=singers)

            #votes and other info
            if self.galgame.votes_ergs_median and self.galgame.votes_ergs_count:
                embedded.add_field(name=_("日本評價 (票數)"),
                                   value="{} ({})".format(self.galgame.votes_ergs_median, self.galgame.votes_ergs_count))
            else:
                metadata = Metadata(self.req_ctx.db, self.galgame, "prerelease")
                interest = metadata["interest"]
                if interest:
                    embedded.add_field(name=_("發售前關注度"), value=str(interest))

            if self.galgame.votes_vndb_average and self.galgame.votes_vndb_count:
                embedded.add_field(name=_("美國評價 (票數)"),
                                   value="{} ({})".format(self.galgame.votes_vndb_average, self.galgame.votes_vndb_count))
            if self.galgame.playtime_median:
                embedded.add_field(name=_("平均遊戲時間"), value=self.galgame.playtime_median)
            if self.galgame.genre:
                embedded.add_field(name=_("類型"), value=self.galgame.genre)

            if self.galgame.banner_url:
                embedded.set_image(url=self.galgame.banner_url)

            res = []
            trait_votes = sorted(self.galgame.trait_votes, key=lambda vote: vote.get_score(), reverse=True)
            for trait_vote in trait_votes:
                #let's show a maximum of 30 for now
                if len(res) > 30:
                    break

                res.append("{}({})".format(trait_vote.trait.chinese_name, trait_vote.get_score()))
            embedded.set_footer(text=", ".join(res))

        self.discord_embed = embedded
        super().preprocess()
