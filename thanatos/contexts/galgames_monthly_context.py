# -*- coding: utf-8 -*-
import logging
import discord
from sqlalchemy import Integer, and_
from sqlalchemy.sql.expression import extract, cast, desc
from sqlalchemy.sql.functions import coalesce
from math import ceil
from datetime import date, datetime, time
from thanatos.models import *
from thanatos.utils import *
from .context import ResponseContext

__all__ = ["GalgamesMonthlyContext"]

class GalgamesMonthlyContext(ResponseContext):
    def __init__(self, req_ctx, year, month):
        super().__init__(req_ctx)
        self.year = year
        self.month = month

    def preprocess(self):
        self.discord_embed = discord.Embed(colour=0x01DFA5)

        def describe_games(current_page):
            with self.req_ctx.db.session() as db_session:
                now = datetime.now()
                is_future_month = self.year > now.year or \
                                    (self.month and self.year == now.year and self.month >= now.month)

                q = db_session.query(Galgame, coalesce(cast(MetadataT.value, Integer), 0))  \
                                .filter(extract("year", Galgame.release_date) == self.year) \
                                .outerjoin(MetadataT, and_(MetadataT.target_id == Galgame.id,
                                                MetadataT.target_type == Galgame.__name__,
                                                MetadataT.namespace == "prerelease",
                                                MetadataT.key == "interest"))
                if self.month:
                    q = q.filter(extract("month", Galgame.release_date) == self.month)

                if is_future_month:
                    #rating by interest
                    q = q.order_by(desc(coalesce(cast(MetadataT.value, Integer), 0)),
                                   Galgame.popularity.desc())
                else:
                    #rate by popularity
                    q = q.order_by(Galgame.popularity.desc(),
                                   desc(coalesce(cast(MetadataT.value, Integer), 0)))

                #get games for this page
                total_games = q.count()
                total_pages = ceil(total_games/self.MaxLinesPerPage)
                if current_page > total_pages:
                    current_page = total_pages

                res = []
                contains_interest = False
                q = q.slice((current_page-1)*self.MaxLinesPerPage, current_page*self.MaxLinesPerPage)
                for galgame, interest in q.all():
                    if self.month:
                        #date only
                        release_date = galgame.release_date.strftime(_("%d號"))
                    else:
                        #month only
                        release_date = galgame.release_date.strftime(_("%m月"))

                    release_datetime = datetime.combine(galgame.release_date, time())
                    if release_datetime > now:
                        contains_interest = True

                        #game yet to be released
                        rating = "\t__{}__\t".format(interest)
                    else:
                        if galgame.votes_ergs_count > 0:
                            rating = "{}({})".format(galgame.votes_ergs_median, galgame.votes_ergs_count)
                        else:
                            rating = "\t-\t"

                    #is favorite?
                    galgame_name = galgame.name
                    if galgame.is_fav_by_user(self.req_ctx.user):
                        galgame_name = "♥" + galgame_name

                    formating = ""
                    if release_datetime > now:
                        #italicize prerelease game
                        formating += "*"

                    res.append("{}{}\t{}\t{}{}".format(formating,
                                                       release_date,
                                                       rating,
                                                       galgame_name,
                                                       formating))

                    if contains_interest:
                        self.discord_embed.set_footer(text=_("_分數_為該遊戲發售前的關注度，越高代表該遊戲越受矚目。"))
            return (current_page, total_pages, "\n".join(res))

        if self.month:
            title = _("{}年{}月作").format(self.year, self.month)
        else:
            title = _("{}年作").format(self.year)
        self.add_field_pagination(title, describe_games)
        super().preprocess()
