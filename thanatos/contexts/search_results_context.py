# -*- coding: utf-8 -*-
import discord
from math import ceil
from datetime import date
from thanatos.models import *
from .context import ResponseContext

__all__ = ["SearchResultsContext"]

class SearchResultsContext(ResponseContext):
    def __init__(self, req_ctx, search_results):
        super().__init__(req_ctx)
        self.search_results = search_results

    def preprocess(self):
        self.discord_embed = discord.Embed(colour=0x0080FF)

        def describe_search_results(current_page):
            #get games for this page
            total_results = len(self.search_results)
            total_pages = ceil(total_results/self.MaxLinesPerPage)
            if current_page > total_pages:
                current_page = total_pages

            res = []
            idx = (current_page-1)*self.MaxLinesPerPage 
            page_end = current_page * self.MaxLinesPerPage
            search_results = search_results[idx:page_end]
            with self.req_ctx.db.session() as db_session:
                for (score, (target_cls, target_id)) in search_results:
                    obj = db_session.query(target_cls).filter_by(id=target_id).one()
                    obj_name = obj.name
                    notes = None

                    if target_cls == Galgame:
                        obj_type = _("遊戲")
                    elif target_cls == Creator:
                        obj_type = _("人名")
                        main_identity = obj.main_identity if obj.main_identity else obj
                        notes = ", ".join(WorkType.to_string(wt) for wt in main_identity.get_types())
                        if main_identity != obj:
                            obj_name = "{}={}".format(obj.name, main_identity.name)
                    elif target_cls == Brand:
                        obj_type = _("公司")
                        if obj.kind == "CIRCLE":
                            notes = _("同人")
                    elif target_cls == Character:
                        obj_type = _("腳色")
                    else:
                        obj_type = _("其他")

                    #notes?
                    notes = " ({})".format(notes) if notes else ""

                    #favorite?
                    if isinstance(obj, Favoritable):
                        if obj.is_fav_by_user(self.req_ctx.user):
                            obj_name = "♥" + obj_name
                    res.append("{}　{}{}".format(obj_type, obj_name, notes))
            return (current_page, total_pages, "\n".join(res))

        self.add_field_pagination(_("「{}」的搜尋結果").format(self.req_ctx.keyword), describe_search_results)
        super().preprocess()
