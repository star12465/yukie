# -*- coding: utf-8 -*-
import re
import discord
from datetime import date
from thanatos.models import *
from .context import ResponseContext

__all__ = ["UserContext"]

class UserContext(ResponseContext):
    def __init__(self, req_ctx, user):
        super().__init__(req_ctx)
        self.user = user

    def preprocess(self):
        def list_favorites(db_session, user, target_type):
            favorites = db_session.query(UserFavorite).filter_by(user=user,
                                                     target_type=target_type.__name__).all()
            favorite_strs = []
            for favorite in favorites:
                obj = db_session.query(target_type).filter_by(id=favorite.target_id).one()
                obj_name = re.sub(r'\(.*\)$', '', obj.name)
                favorite_strs.append(obj_name)
            favorite_strs.sort()
            return "/".join(favorite_strs)

        self.discord_embed = discord.Embed(colour=0x9FF781)
        self.discord_embed.set_author(name=self.user.username)

        with self.req_ctx.db.session() as db_session:
            self.user = db_session.merge(self.user)
            fav_brands = list_favorites(db_session, self.user, Brand)
            if len(fav_brands) > 0:
                self.discord_embed.add_field(name=_("最愛的公司"), value=fav_brands)
            fav_creators = list_favorites(db_session, self.user, Creator)
            if len(fav_creators) > 0:
                self.discord_embed.add_field(name=_("最愛的創作者"), value=fav_creators)
            fav_galgames = list_favorites(db_session, self.user, Galgame)
            if len(fav_galgames) > 0:
                self.discord_embed.add_field(name=_("最愛的遊戲"), value=fav_galgames)

            #no favorites?
            if len(fav_brands) + len(fav_creators) + len(fav_galgames) == 0:
                self.discord_embed.description = "{}是個沒有愛的男人".format(self.user.username)

        super().preprocess()
