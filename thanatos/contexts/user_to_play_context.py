# -*- coding: utf-8 -*-
import discord
from math import ceil
from datetime import datetime, time
from thanatos.utils import Metadata
from thanatos.models import *
from .context import ResponseContext

__all__ = ["UserToPlayContext"]

class UserToPlayContext(ResponseContext):
    def __init__(self, req_ctx, user):
        super().__init__(req_ctx)
        self.user = user 

    def preprocess(self):
        self.discord_embed = discord.Embed(colour=0x6E6E6E)
        self.discord_embed.set_author(name="{}".format(self.user.username))

        def describe_games(current_page):
            with self.req_ctx.db.session() as db_session:
                metadata = Metadata(self.req_ctx.db, self.user, "to_play")
                galgames = metadata["to_play"]
                if galgames:
                    #get games for this page
                    total_games = len(galgames)
                    total_pages = ceil(total_games/self.MaxLinesPerPage)
                    if current_page > total_pages:
                        current_page = total_pages

                    res = []
                    galgame_ids = galgames[(current_page-1)*self.MaxLinesPerPage:current_page*self.MaxLinesPerPage]
                    for galgame_id in galgame_ids:
                        galgame = db_session.query(Galgame).filter_by(id=galgame_id).one()
                        if datetime.combine(galgame.release_date, time()) > datetime.now():
                            notes = _(" (未發售)")
                        else:
                            notes = ""

                        res.append("{}{}".format(galgame.name, notes))
                    return (current_page, total_pages, "\n".join(res))
                else:
                    return (1, 1, _("沒有待玩遊戲"))

        self.add_field_pagination(_("待玩遊戲"), describe_games)
        super().preprocess()
