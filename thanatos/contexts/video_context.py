# -*- coding: utf-8 -*-
import discord
from thanatos.models import *
from .context import ResponseContext

__all__ = ["VideoContext"]

class VideoContext(ResponseContext):
    def __init__(self, req_ctx, video):
        super().__init__(req_ctx)
        self.video = video

    def preprocess(self):
        with self.req_ctx.db.session() as db_session:
            self.video = db_session.merge(self.video)
            self.msg = "{} {}".format(self.video.galgame.name, VideoType.to_string(self.video.type))
            if self.video.song_name:
                self.msg += " {}".format(self.video.song_name)
            if self.video.song_artist:
                self.msg += "/{}".format(self.video.song_artist.name)
            if self.video.release_date:
                self.msg += " ({})".format(self.video.release_date.strftime("%Y-%m-%d"))
            self.msg += "\nhttps://www.youtube.com/watch?v={}".format(self.video.youtube_id)
        super().preprocess()
