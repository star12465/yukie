# -*- coding: utf-8 -*-
import discord
from datetime import date
from thanatos.contexts import GalgameContext, ResponseContext
from thanatos.models import *

__all__ = ["VideosListContext"]

class VideosListContext(ResponseContext):
    def __init__(self, req_ctx, galgame):
        super().__init__(req_ctx)
        self.galgame = galgame

    def preprocess(self):
        self.discord_embed = discord.Embed(colour=0x8258FA)

        #let's steal the header from GalgameContext
        galgame_res_ctx = GalgameContext(self.req_ctx, self.galgame)
        galgame_res_ctx.galgame = self.galgame
        galgame_res_ctx.preprocess()

        #steal, steal, steal
        self.discord_embed.set_author(name=galgame_res_ctx.discord_embed.author.name)
        self.discord_embed.title = galgame_res_ctx.discord_embed.title
        self.discord_embed.url = galgame_res_ctx.discord_embed.url
        self.discord_embed.description = galgame_res_ctx.discord_embed.description

        def list_videos(galgame, video_type):
            videos_strs = []
            i = 1
            for video in galgame.videos:
                if video.type == video_type:
                    videos_strs.append("{}{}\t{}".format(VideoType.to_code(video_type), i, video.name))
                    i += 1
            return "\n".join(videos_strs)

        #list videos
        with self.req_ctx.db.session() as db_session:
            self.galgame = db_session.merge(self.galgame)
            for video_type in VideoType:
                if video_type == VideoType.All:
                    continue
                if self.req_ctx.video_type not in (VideoType.All, video_type):
                    continue

                videos_str = list_videos(self.galgame, video_type)
                if len(videos_str) > 0:
                    self.discord_embed.add_field(name=VideoType.to_string(video_type),
                                                 value=videos_str, inline=False)
        super().preprocess()
