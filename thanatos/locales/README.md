The way that gettext works is by generating a file mapping of
keys looking like _('string') in *.py files with empty values as a .pot file

To do this, we use pybabel to generate this file through the command
pybabel extract <dir_to_parse> -o messages.pot

Using this .pot file, we can specify that we want a dictionary with that language
pybabel init -i messages.pot -d <directory_to_extract_to> -l <language>
e.g. to create a new mapping from initial keys to english, 
pybabel init -i messages.pot -d . -l en

We then need to edit the .po files to fill in the values. For us, thanatos will read the
current directory through its LanguageManager for any languages and convert them to .mo files
to read at runtime.

In the case that we have changes in our *.py files or want to include new ones, we can generate
the .pot file again through pybabel extract.
Then, we need to update any existing .po dictionaries we have generated for specific languages
with these new keys or remove any old keys no longer relevant through pybabel.
pybabel update -i messages.pot -d <parent_directory_of_po_files>

