import asyncio
import logging

logger = logging.getLogger(__name__)

class AwarenessManager:
    '''
    This module is not thread-safe.
    However, we don't need to be if all events are registered
    before we do call_event. That is if self.events never changes.
    '''
    @classmethod
    def initialize(cls):
        cls.awarenesses = {}
        cls.handlers = []

    @classmethod
    def activate(cls, awareness, *args, **kwargs):
        '''Triggers the behaviors attached to the awareness.
        '''
        logger.debug((awareness, args, kwargs))

        for behavior in cls.awarenesses.get(awareness, []):
            behavior(*args, **kwargs)

        for handler in cls.handlers:
            handler(awareness, *args, **kwargs)

    @classmethod
    def add_behavior(cls, awareness, behavior):
        '''Attaches a behavior function to a specific awareness
        '''
        if awareness not in cls.awarenesses:
            cls.awarenesses[awareness] = []
        cls.awarenesses[awareness].append(behavior)

    @classmethod
    def add_handler(cls, handler):
        '''Adds a handler that handles all awarenesses
        '''
        #work with this cloned handler to prevent
        #race condition
        tmp_handlers = cls.handlers[:]
        tmp_handlers.append(handler)

        #this should be atomic
        cls.handlers = tmp_handlers

    @classmethod
    def remove_handler(cls, handler):
        '''Removes an existing handler
        '''
        #work with this cloned handler to prevent
        #race condition
        tmp_handlers = cls.handlers[:]
        try:
            tmp_handlers.remove(handler)
        except ValueError:
            #do nothing
            pass
        else:
            cls.handlers = tmp_handlers
