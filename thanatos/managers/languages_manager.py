import errno
import gettext
import logging
import os
import shutil
from .manager import Manager
from babel.messages.pofile import read_po
from babel.messages.mofile import write_mo
import thanatos.locales

logger = logging.getLogger(__name__)

class LanguagesManager(Manager):
    DOMAIN = 'messages'

    @classmethod
    def initialize(cls, default_language, mo_dir):
        """
        TODO: Change from using relative path of current directory to /usr/sometempfolder
        :mo_dir: Relative path to from run directory to write and read mo files including locale folder, 
                    e.g. locale
        """
        cls.mo_dir = mo_dir
        cls.set_language(default_language)

    @classmethod
    def set_language(cls, language):
        cls.generate_mo_file(language)
        # Fallback true will make it go to original text
        # via gettext class NullTranslation if domain mo is not found
        cur = gettext.translation(cls.DOMAIN, cls.mo_dir, [language], fallback=True)
        # Change _() function globally
        cur.install()

    @classmethod
    def generate_mo_file(cls, locale):
        po_file = cls.get_po_filepath(locale)
    
        if os.path.exists(po_file):
            # mo directory and file paths
            mo_file = os.path.join(cls.mo_dir, locale, 'LC_MESSAGES', cls.DOMAIN + '.mo')

            # If po_file exists, read and convert to mo_file
            with open(po_file, 'rb') as infile:
                catalog = read_po(infile, locale)
            
            for message, errors in catalog.check():
                for error in errors:
                    logger.warning('error: %s:%d: %s', po_file, message.lineno, error)

            logger.info('compiling catalog %s to %s', po_file, mo_file)

            # Create directories for mo file if they do not exist
            locale_dir = os.path.join(cls.mo_dir, locale)
            try:
                os.mkdir(cls.mo_dir)
                os.mkdir(locale_dir)
                os.mkdir(os.path.join(locale_dir, 'LC_MESSAGES'))
            except OSError as ex:
                if ex.errno != errno.EEXIST:
                    raise

            with open(mo_file, 'wb') as outfile:
                # use_fuzzy True to ignore problems, might be unsafe
                write_mo(outfile, catalog, use_fuzzy=True)
            
            return mo_file
        return None

    @classmethod
    def get_po_filepath(cls, locale):
        """
        Gets absolute path to directory of po files in this package
        and appends it with expected format for translation files
        """
        po_dir = os.path.dirname(os.path.abspath(thanatos.locales.__file__)) 
        return os.path.join(po_dir, locale, 'LC_MESSAGES', cls.DOMAIN + '.po')

    @classmethod
    def cleanup(cls):
        shutil.rmtree(cls.mo_dir, ignore_errors=True)
