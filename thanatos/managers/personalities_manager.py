import os
import asyncio
import random
import importlib
import aiohttp
import async_timeout
import thanatos.personalities
from thanatos.utils import GlobalMetadata
from .awareness_manager import AwarenessManager

class PersonalitiesManager:
    @classmethod
    def initialize(cls, thanatos):
        cls.thanatos = thanatos
        cls.load_all_personalities()
        if len(cls.personalities) == 0:
            raise RuntimeError("No personalities available")

        #current personality
        cls.current = None

    @classmethod
    def use_personality(cls, name, set_avatar=True):
        if name not in cls.personalities:
            return False

        if cls.current:
            AwarenessManager.remove_handler(cls.current.awareness_handler)
        cls.current = cls.personalities[name]
        AwarenessManager.add_handler(cls.current.awareness_handler)

        #save to database
        GlobalMetadata(cls.thanatos.db)["last_personality"] = cls.current.name

        asyncio.run_coroutine_threadsafe(cls._update_nickname(), cls.thanatos.loop)
        if set_avatar:
            asyncio.run_coroutine_threadsafe(cls._update_avatar(), cls.thanatos.loop)
        return True

    @classmethod
    def load_all_personalities(cls):
        cls.personalities = {}

        path = os.path.dirname(os.path.abspath(thanatos.personalities.__file__))
        for filename in os.listdir(path):
            if filename[0] in ("_", "."):
                continue
            name, ext = os.path.splitext(filename)
            if ext != ".py":
                continue

            module = importlib.import_module("thanatos.personalities.{}".format(name))

            #to camel case
            cls_name = "".join(x.title() for x in name.split("_"))

            personality_cls = getattr(module, cls_name)
            personality = personality_cls(cls.thanatos)
            if not isinstance(personality, thanatos.personalities.Personality):
                continue
            if personality.enabled:
                cls.personalities[personality.name] = personality

    @classmethod
    async def _update_nickname(cls, guild=None):
        '''
        guild=None to update nickname for all servers
        '''
        guilds = [guild] if guild else cls.thanatos.servers
        for guild in guilds:
            #update nickname
            if cls.current.name:
                if cls.current.name != guild.me.display_name:
                    await cls.thanatos.change_nickname(guild.me, cls.current.name)
            else:
                logger.warning("Named not supplied for personality {}".format(cls.current.__class__.__name__))

    @classmethod
    async def _update_avatar(cls):
        if not hasattr(cls.current, "avatar") or not cls.current.avatar:
            if cls.current.avatar_url:
                try:
                    async with aiohttp.ClientSession(loop=cls.thanatos.loop) as session:
                        with async_timeout.timeout(10, loop=cls.thanatos.loop):
                            async with session.get(cls.current.avatar_url) as response:
                                cls.current.avatar = await response.read()
                except asyncio.TimeoutError:
                    logger.error("Timed out while trying to fetch personality avatar at {}".format(cls.current.avatar_url))
                    return
                except aiohttp.ClientError:
                    logger.exception("Encountered an http error when fetching avatar")
                    return
            else:
                logger.warning("Personality avatar not supplied for personality {}".format(cls.current.__class__.__name__))

        if hasattr(cls.current, "avatar") and cls.current.avatar:
            await cls.thanatos.edit_profile(avatar=cls.current.avatar)
