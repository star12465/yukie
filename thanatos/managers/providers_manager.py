import logging
import threading
from datetime import datetime
from .manager import Manager
from sqlalchemy import inspect
from thanatos.providers import *
from thanatos.models import *
from thanatos.models.base import Updatable

logger = logging.getLogger(__name__)

__all__ = ["ProvidersManager"]

class ProvidersManager(Manager):
    PROVIDERS = (
        ErogamescapeProvider,
        ErogetrailersProvider,
        GameplazaProvider,
        MeigiProvider,
        SeiyuumatomeProvider,
        VndbProvider
    )

    UPDATE_METHODS = {
        Brand: "update_brand",
        Galgame: "update_galgame",
        GalgameTrait: "update_galgame_trait",
        GalgameTraitVote: "update_galgame_trait_vote",
        Creator: "update_creator",
        Character: "update_character"
    }

    updating_lock = threading.RLock()
    updating_objs = []

    @classmethod
    def update(cls, db, objs, components=Updatable.Component.Core):
        if len(objs) == 0:
            return True

        model, method = None, None
        for model, method in cls.UPDATE_METHODS.items():
            if type(objs[0]) == model:
                break
        if not method:
            return False

        #we'll have to update the core component first on its own
        if (components & Updatable.Component.Core) > 0:
            if components > Updatable.Component.Core:
                #we're updating more than just the core component
                cls.update(db, objs, Updatable.Component.Core)
                components &= ~Updatable.Component.Core

        #event for this thread
        event = threading.Event()

        #the objects waiting to be updated
        pending_objs = objs[:]

        #we want to avoid updating the same object multitimes
        #we do so by checking cls.updating_objs to see if another thread is updating this object
        #we uniquely identity ourselves by (classname, primary key values)
        while True:
            event.clear()

            #the events we'll wait for
            events_to_wait = []

            #the events we want to wait for before updating
            events_wait_and_update = []

            #the objects we're going to update in this round
            to_update = []

            cls.updating_lock.acquire()
            for obj in pending_objs[:]:
                primary_key_columns = [key.name for key in inspect(obj.__class__).primary_key]
                primary_key_values = tuple(getattr(obj, column) for column in primary_key_columns)
                key = (obj.__class__, primary_key_values)

                waiting = False
                for (key_, _, components_, event_) in cls.updating_objs:
                    if key_ != key:
                        continue

                    #if the other thread is updating the core component, we want to update
                    #after it's been updated
                    if (components_ & Updatable.Component.Core) > 0:
                        events_wait_and_update.append((obj, event_))
                        pending_objs.remove(obj)
                        waiting = True
                        break

                    #if the other thread is updating the component we're updating, wait for it
                    if components > 0 and components_ > 0 and \
                                        (components | components_) == components_:
                        events_to_wait.append(event_)
                        pending_objs.remove(obj)
                        waiting = True
                        break

                if not waiting:
                    #we'll have to update this ourselves
                    #create an event for this obj
                    to_update.append(obj)
                    pending_objs.remove(obj)
                    cls.updating_objs.append((key, obj, components, event))
            cls.updating_lock.release()

            #update the ones we can update now
            for obj in to_update:
                for provider_cls in cls.PROVIDERS:
                    provider = provider_cls(db)
                    batch_method = method + "s"
                    if not hasattr(provider, method) and not hasattr(provider, batch_method):
                        #this provider does not support updating this obj
                        continue

                    last_updated = obj.get_last_updated(db, provider_cls, components)
                    if obj.should_update(components, last_updated):
                        #update
                        if hasattr(provider, batch_method):
                            getattr(provider, batch_method)(to_update, components)
                        else:
                            for obj in to_update:
                                getattr(provider, method)(obj, components)

                #remove object from the updating list
                cls.updating_lock.acquire()
                for i in range(len(cls.updating_objs)):
                    _, obj_, components_, event_ = cls.updating_objs[i]
                    if obj == obj_ and components == components_ and event == event_:
                        cls.updating_objs.pop(i)
                        break
                cls.updating_lock.release()
            event.set()

            #ideally we want to wait until *any* event is signaled
            #but yeah...
            if len(events_wait_and_update) > 0:
                it = iter(events_wait_and_update)
                obj_, event_ = next(it)
                event_.wait()
                pending_objs.append(obj_)
                for obj_, event_ in it:
                    if event_.wait(0) == True:
                        pending_objs.append(obj_)
            else:
                #wait for the rest of the events and we're done
                for event_ in events_to_wait:
                    event_.wait()
                break
        return True

    @classmethod
    def update_all(cls, db, model_classes, components=Updatable.Component.Core):
        for model_cls in model_classes:
            #some might not support update_all
            if model_cls not in cls.UPDATE_METHODS:
                continue

            method = cls.UPDATE_METHODS[model_cls]
            for provider_cls in cls.PROVIDERS:
                #due to the volume of data
                #we don't be marking the data as updated
                provider = provider_cls(db)
                batch_method = method + "s"
                if hasattr(provider, batch_method):
                    getattr(provider, batch_method)(None, components)
