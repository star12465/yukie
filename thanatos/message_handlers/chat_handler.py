import re
import asyncio
import logging
import random
from .message_handler import MessageHandler
from thanatos.utils import *
from thanatos.models import *
from thanatos.contexts import *
from thanatos.managers import *

__all__ = ["ChatHandler"]

logger = logging.getLogger(__name__)

class ChatHandler(MessageHandler):
    def __init__(self, thanatos):
        super().__init__(thanatos)

    async def handle_message(self, req_ctx):
        msg = req_ctx.msg
        prefix = r'^<@!?' + str(self.thanatos.user.id) + '> (.*)$'
        m = re.search(prefix, msg)
        if m:
            req_ctx.sentence = m.group(1).strip()
            AwarenessManager.activate("chat.start", req_ctx)
