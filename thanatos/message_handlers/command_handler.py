import re
import asyncio
import logging
import importlib
from gettext import gettext as _
from thanatos.models import *
from thanatos.contexts import *
from thanatos.utils import normalize_str
from thanatos.managers import AwarenessManager
from .message_handler import MessageHandler
from .commands import *

logger = logging.getLogger(__name__)

class CommandHandler(MessageHandler):
    COMMANDS_MODULES = ("core_commands", "creator_commands", "general_commands", "misc_commands")

    def __init__(self, thanatos):
        super().__init__(thanatos)

        self.commands = {}
        for commands_module in self.COMMANDS_MODULES:
            module = importlib.import_module("thanatos.message_handlers.commands.{}".format(commands_module))
            for fn_str in dir(module):
                if fn_str.startswith("cmd_"):
                    fn = getattr(module, fn_str)
                    self.commands[fn_str[len("cmd_"):]] = fn()

    async def handle_message(self, req_ctx):
        msg = req_ctx.msg
        if len(msg) > 0 and msg[0] in ("!", "！"):
            #do we have an actual command to process?
            if len(normalize_str(msg[1:])) > 0:
                try:
                    req_ctx.cmd_str, req_ctx.args_str = re.split(r'\s', msg[1:], maxsplit=1)
                except ValueError:
                    #no space
                    req_ctx.cmd_str, req_ctx.args_str = msg[1:], ""

                AwarenessManager.activate("command.start", req_ctx)
                if req_ctx.cmd_str in self.commands:
                    cmd = self.commands[req_ctx.cmd_str]
                    await self.thanatos.loop.run_in_executor(self.executor, cmd.execute, req_ctx)
                elif req_ctx.cmd_str == "help":
                    #help is special :)
                    self.cmd_help(req_ctx)
                else:
                    res_ctx = ResponseContext(req_ctx)
                    res_ctx.msg = _("找不到指令")
                    AwarenessManager.activate("command.not_found", res_ctx)

    def cmd_help(self, req_ctx):
        msg = _("Thanatos (タナトス) 機器人功能介紹\n")
        msg += _("=========================================\n")
        msg += "?關鍵字: 可用來搜尋公司、創作家、遊戲、遊戲影片。輸入?help可查看詳細說明。\n"

        cmd_help_msgs = []
        for cmd_str, cmd in self.commands.items():
            if (req_ctx.guild or cmd.allow_pm) and cmd.user_has_privilege(req_ctx.user):
                cmd_help_msgs.append("!{}: {}".format(cmd_str, cmd.get_short_help()))
        cmd_help_msgs.sort()
        msg += "\n".join(cmd_help_msgs)

        res_ctx = ResponseContext(req_ctx)
        res_ctx.msg = "```{}```".format(msg)
        AwarenessManager.activate("command.help.done", res_ctx)
