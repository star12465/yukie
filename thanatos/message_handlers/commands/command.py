import re
import logging
from enum import IntEnum
from thanatos.managers import *
from thanatos.models import *
from thanatos.contexts import *

__all__ = ["CommandErrorCode", "make_command", "Command"]

logger = logging.getLogger(__name__)

class CommandError(ValueError):
    pass

class CommandErrorCode(IntEnum):
    OK = 0
    NoPrivilege = -1
    UnclosedSqrBt = -2
    MissingClosingGt = -3
    LtInLtTokens = -4
    Grammar = -5
    MissingArguments = -6
    TooManyArguments = -7
    NoMatchingSig = -8
    Internal = -9
    NotAllowedInPM = -10

    TokenDUserInvalid = -20
    TokenDChannelInvalid = -21
    TokenDEmojiInvalid = -22
    TokenObjNotFound = -23

def make_command(*args, **kwargs):
    def decorator(command_fn):
        def wrapper():
            c = Command(*args, **kwargs)
            c.name = command_fn.__name__
            c.fn = command_fn
            return c
        return wrapper
    return decorator

class Command:
    def __init__(self, *args, **kwargs):
        self.privilege = kwargs.get("privilege", 50)
        self.grammar = kwargs.get("grammar", "")
        self.brief = kwargs.get("brief", "")
        self.allow_pm = kwargs.get("allow_pm", False)

    def get_short_help(self):
        return self.brief

    def user_has_privilege(self, user):
        return user.privilege <= self.privilege

    def execute(self, req_ctx):
        def parse_args(grammar, req_ctx):
            grammar_tokens = self.Token.tokenize(req_ctx.db, grammar, is_grammar=True)
            input_token_strs = self.Token.tokenize(req_ctx.db, req_ctx.args_str, is_grammar=False)

            #find the right grammar (parse the optional sets) for this input
            expanded_optional = False
            while True:
                #whether this grammar contains wildchar
                has_wildchar = False

                #the index of the optional grammar token
                #-1 if an optional grammar token does not exist
                optional_index = -1

                #number of arguments we have to fullfil defined in grammar
                required_count = 0
                for i in range(len(grammar_tokens)):
                    _, grammar_token = grammar_tokens[i]
                    if grammar_token.is_wildchar:
                        has_wildchar = True
                    if grammar_token.is_optional:
                        if optional_index >= 0:
                            #grammar error: we can't have multiple optional sets
                            logger.error("Command {} with grammar {} has multiple optional sets".format(
                                                                                    req_ctx.cmd_str, self.grammar))
                            raise CommandError(CommandErrorCode.Grammar, "")
                        optional_index = i
                    else:
                        required_count += 1

                if required_count == len(input_token_strs):
                    #exact
                    break
                elif required_count < len(input_token_strs):
                    if has_wildchar:
                        #wildchar matches the remaining
                        break
                    else:
                        if optional_index < 0:
                            #we don't have any optional arguments to expand
                            #user is supplying too many arguments
                            raise CommandError(CommandErrorCode.TooManyArguments, "")

                        #let's try to expand the optional ones
                        i = optional_index
                        _, grammar_token = grammar_tokens[i]
                        grammar_tokens[i:i+1] = grammar_token.expand_optional()
                        expanded_optional = True
                else:
                    if expanded_optional:
                        raise CommandError(CommandErrorCode.NoMatchingSig, "")
                    else:
                        raise CommandError(CommandErrorCode.MissingArguments, "")

            #now actually parse input with our grammar
            kwargs = {}
            i, j = 0, 0
            while j < len(grammar_tokens):
                _, grammar_token = grammar_tokens[j]
                if grammar_token.is_optional:
                    j += 1
                    continue

                if i >= len(input_token_strs):
                    #more than one wildchar detected
                    logger.error("Command {} grammar {} contains a wildchar that is not at the end" \
                                    .format(req_ctx.cmd_str, self.grammar))
                    raise CommandError(CommandErrorCode.Grammar, "")
                k, input_token_str = input_token_strs[i]

                if grammar_token.is_wildchar:
                    #skip whitespace
                    while k < len(req_ctx.args_str) and req_ctx.args_str[k] in (" ", "\t"):
                        k += 1
                    s = req_ctx.args_str[k:]
                    i = len(input_token_strs)
                else:
                    s = input_token_str
                value = grammar_token.parse(s)
                if grammar_token.name in kwargs:
                    logger.error("Command {} grammar {} contains duplicate name {}".format(req_ctx.cmd_str,
                                                                                           self.grammar,
                                                                                           grammar_token.name))
                    raise CommandError(CommandErrorCode.Grammar, "")
                else:
                    kwargs[grammar_token.name] = value
                i += 1
                j += 1
            return kwargs
            
        cmd_error = CommandErrorCode.OK
        if not req_ctx.guild and not self.allow_pm:
            cmd_error = CommandErrorCode.NotAllowedInPM
            cmd_error_note = ""
            
        if cmd_error == CommandErrorCode.OK and not self.user_has_privilege(req_ctx.user):
            cmd_error = CommandErrorCode.NoPrivilege
            cmd_error_note = ""

        if cmd_error == CommandErrorCode.OK:
            try:
                req_ctx.cmd_kwargs = parse_args(self.grammar, req_ctx)
            except CommandError as ex:
                cmd_error = ex.args[0]
                cmd_error_note = ex.args[1]

        #pass args to the actual function
        logger.info("#{} {}: {} {}".format(req_ctx.discord_msg.channel,
                                           req_ctx.user,
                                           req_ctx.cmd_str,
                                           req_ctx.args_str))

        if cmd_error != CommandErrorCode.OK:
            logger.debug("error: {} {}".format(str(cmd_error), cmd_error_note))
            res_ctx = ResponseContext(req_ctx)
            res_ctx.cmd_error = cmd_error
            res_ctx.cmd_error_note = cmd_error_note
            AwarenessManager.activate("command.error", res_ctx)
        else:
            logger.debug("parsed result: {}".format(req_ctx.cmd_kwargs))
            AwarenessManager.activate("command.parsed", req_ctx)
            res_ctx = self.fn(req_ctx, **req_ctx.cmd_kwargs)

    def __repr__(self):
        return "<Command(name={})>".format(self.name)

    class Token:
        ObjectPython = 0
        ObjectSearchable = 1

        @staticmethod
        def next_token_str(s, i=0, *, allow_optional=False):
            #skip whitespace
            while i < len(s) and s[i] in (" ", "\t"):
                i += 1
            if i >= len(s):
                #nothing to parse
                return i, ""

            j = i
            if s[j] == "<":
                #special objects
                j += 1
                while j < len(s) and s[j] != ">":
                    if s[j] == "<":
                        #we don't allow < in < tokens
                        raise CommandError(CommandErrorCode.LtInLtTokens, s[i:])
                    j += 1
                if j < len(s) and s[j] == ">":
                    j += 1
                else:
                    #missing a closing '>'
                    raise CommandError(CommandErrorCode.MissingClosingGt, s[i:])
            elif allow_optional and s[j] == "[":
                #optional tokens (for grammar)
                counter = 1
                j += 1
                while j < len(s) and counter != 0:
                    if s[j] == "[":
                        counter += 1
                    elif s[j] == "]":
                        counter -= 1
                    j += 1
                if counter != 0:
                    #unbalanced []
                    raise CommandError(CommandErrorCode.UnclosedSqrBt, s[i:])
            else:
                #normal strings
                while j < len(s) and s[j] not in (" ", "\t"):
                        j += 1
            return j, s[i:j]

        @classmethod
        def tokenize(cls, db, s, is_grammar):
            i = 0
            tokens = []
            while i < len(s):
                try:
                    j, token_str = cls.next_token_str(s, i, allow_optional=is_grammar)
                except CommandError as ex:
                    #relay the error
                    if is_grammar:
                        cmd_error, cmd_error_description = ex.args[0], ex.args[1]
                        logger.error("Encountered {} while parsing token {}".format(str(cmd_error),
                                                                                    cmd_error_description))
                        raise CommandError(CommandErrorCode.Grammar, "")
                    else:
                        raise

                if is_grammar:
                    #if it's grammar, we return as Token objects
                    tokens.append((i, cls(db, token_str)))
                else:
                    #if it's input, we return as strings
                    tokens.append((i, token_str))
                i = j
            return tokens
            
        def __init__(self, db, grammar_str):
            self.db = db
            self.is_wildchar = False
            self.is_optional = False
            self.grammar_str = grammar_str
            self.parse_grammar()

        def __repr__(self):
            return "<Token name={},grammar={}>".format(self.name, self.grammar_str)

        def parse(self, token):
            logger.error("parse called. grammar={},token={}".format(self.grammar_str, token))
            raise CommandError(CommandErrorCode.Internal, "")

        def parse_grammar(self):
            if self.grammar_str[0] == "<":
                if self.grammar_str[1] == "@":
                    #discord user
                    self.parse = self.parse_discord_user
                    self.name = self.grammar_str[2:-1]
                elif self.grammar_str[1] == "#":
                    #discord channel
                    self.parse = self.parse_discord_channel
                    self.name = self.grammar_str[2:-1]
                elif self.grammar_str[1] == ":":
                    #discord emoji
                    self.parse = self.parse_discord_emoji
                    self.name = self.grammar_str[2:-1]
                else:
                    m = re.search(r'<(.*?)\|(.*?)(?:\|(.*))?>', self.grammar_str)
                    if m:
                        self.parse = self.parse_object
                        self.delimiters = ""
                        self.name = m.group(1)

                        obj_names_str = m.group(2)
                        if obj_names_str[0] == "*":
                            #normal python object
                            obj_names_str = obj_names_str[1:]
                            self.obj_type = self.ObjectPython
                        else:
                            #Searchable python objects
                            self.obj_type = self.ObjectSearchable
                        self.objs = [eval(x) for x in obj_names_str.split(",")]

                        modifiers = m.group(3)
                        if modifiers:
                            self.is_wildchar = True
                            self.delimiters = modifiers 
                    else:
                        logger.error("Unknown grammar token {}".format(self.grammar_str))
                        raise CommandError(CommandErrorCode.Grammar, "")
            elif self.grammar_str[0] == "[":
                self.is_optional = True
                self.parse = self.parse_optional
                self.name = "optional"
            elif self.grammar_str == "...":
                self.is_wildchar = True
                self.parse = self.parse_normal
                self.name = "other"
            else:
                self.parse = self.parse_normal
                self.name = self.grammar_str

        def expand_optional(self):
            if not self.is_optional:
                return self

            #get rid of the [ and ]
            return self.tokenize(self.db, self.grammar_str[1:-1], is_grammar=True)

        def parse_optional(self, token):
            logger.error("attempted to call parse on an optional parameter: {}".format(self.token_str))
            raise CommandError(CommandErrorCode.Internal, "")

        def parse_normal(self, token):
            #if it's surrounded by < and >
            #strip those only if it's not wildchar
            if not self.is_wildchar and token[0] == "<":
                token = token[1:-1]
            return token

        def parse_discord_user(self, token):
            m = re.search(r'<@!?(\d+)>', token)
            if not m:
                raise CommandError(CommandErrorCode.TokenDUserInvalid, token)
            user_id = int(m.group(1))

            #get user
            user = None
            with self.db.session() as db_session:
                q = db_session.query(User).filter_by(id=user_id)
                if q.count() > 0:
                    user = q.one()
                    db_session.refresh(user)
                    db_session.expunge(user)
            return user

        def parse_discord_channel(self, token):
            m = re.search(r'<#(\d+)>', token)
            if not m:
                raise CommandError(CommandErrorCode.TokenDChannelInvalid, token)
            channel_id = int(m.group(1))

            return channel_id

        def parse_discord_emoji(self, token):
            m = re.search(r'<:([A-z0-9_]+):(\d+)>', token)
            if not m:
                raise CommandError(CommandErrorCode.TokenDEmojiInvalid, token)
            return (m.group(1), int(m.group(2)))

        def parse_object(self, token):
            if self.delimiters:
                #quick and dirty, may not be the best
                items = re.split("[{}]".format(self.delimiters), token)
            else:
                #if it's surrounded by < and >
                #strip those
                if token[0] == "<":
                    token = token[1:-1]
                items = [token]

            found, not_found = [], []
            for item in items:
                if len(item.strip()) == 0:
                    continue
                inst = None

                #find an object that can process this item
                for obj in self.objs:
                    if self.obj_type == self.ObjectPython:
                        try:
                            inst = obj(item)
                            if inst != None:
                                break
                        except:
                            pass
                    else:
                        with self.db.session() as db_session:
                            inst = obj.get_by_name(db_session, item)
                            if inst:
                                db_session.refresh(inst)
                        if inst != None:
                            break

                #cannot parse this item into an instance of an object
                if not inst:
                    not_found.append(item)
                else:
                    found.append(inst)

            if self.delimiters:
                if len(found)+len(not_found) > 0:
                    return (found, not_found)
                else:
                    raise CommandError(CommandErrorCode.MissingArguments, (self.objs, token))
            else:
                if len(found) > 0:
                    return found[0]
                else:
                    raise CommandError(CommandErrorCode.TokenObjNotFound, (self.objs, token))
