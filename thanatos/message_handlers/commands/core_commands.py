from thanatos.models import *
from thanatos.contexts import *
from thanatos.managers import *
from .command import make_command

@make_command(privilege=0, grammar="...", brief="讓bot說話")
def cmd_say(req_ctx, other):
    res_ctx = ResponseContext(req_ctx)
    res_ctx.msg = other
    res_ctx.should_mention(False)

    #delete the command we used to tell the bot to say
    req_ctx.delete()

    AwarenessManager.activate("command.say.done", res_ctx)

@make_command(privilege=0, grammar="<@user> <privilege|*int>", brief="設定使用者權限")
def cmd_set_privilege(req_ctx, user, privilege):
    res_ctx = ResponseContext(req_ctx)
    if privilege < 0:
        AwarenessManager.activate("command.set_privilege.bad_privilege", res_ctx)
    else:
        with req_ctx.db.session() as db_session:
            user = db_session.merge(user, load=False)
            user.privilege = privilege
        AwarenessManager.activate("command.set_privilege.done", res_ctx)

@make_command(privilege=0, grammar="personality", brief="設人格")
def cmd_設人格(req_ctx, personality):
    req_ctx.personality = personality

    if PersonalitiesManager.use_personality(personality):
        AwarenessManager.activate("command.設人格.done", ResponseContext(req_ctx))
    else:
        AwarenessManager.activate("command.設人格.not_found", ResponseContext(req_ctx))
