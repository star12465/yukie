# -*- coding: utf-8 -*-
from .command import make_command
from thanatos.managers import *
from thanatos.models import *
from thanatos.contexts import *

@make_command(privilege=40, grammar="<creators|Creator|=>", brief="設馬甲")
def cmd_設馬甲(req_ctx, *, creators):
    found, not_found = creators

    if len(found) >= 2:
        with req_ctx.db.session() as db_session:
            creators = [db_session.merge(c, load=False) for c in found]
            creators[0].set_as_main_identity(creators[1:])
            creators[0].redecide_main_identity()
        res_ctx = CreatorContext(req_ctx, creators[0])
        res_ctx.found = found
        res_ctx.not_found = not_found
        AwarenessManager.activate("command.設馬甲.done", res_ctx)
    else:
        res_ctx = ResponseContext(req_ctx)
        res_ctx.found = found
        res_ctx.not_found = not_found
        AwarenessManager.activate("command.設馬甲.failed", res_ctx)

@make_command(privilege=40, grammar="<creator|Creator>", brief="設本尊")
def cmd_設本尊(req_ctx, creator):
    with req_ctx.db.session() as db_session:
        creator = db_session.merge(creator, load=False)
        creator.set_as_main_identity(force=True)

    #display success message with creator context
    res_ctx = CreatorContext(req_ctx, creator)
    AwarenessManager.activate("command.設本尊.done", res_ctx)
