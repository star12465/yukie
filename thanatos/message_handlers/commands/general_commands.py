# -*- coding: utf-8 -*-
import sqlalchemy
from .command import make_command
from thanatos.utils import *
from thanatos.models import *
from thanatos.contexts import *
from thanatos.managers import *

@make_command(brief="顯示我的資料")
def cmd_我(req_ctx):
    res_ctx = UserContext(req_ctx, req_ctx.user)
    AwarenessManager.activate("command.我.done", res_ctx)

@make_command(grammar="<@user>", brief="偷窺其他帳號的資料")
def cmd_偷窺(req_ctx, user):
    res_ctx = UserContext(req_ctx, user)
    AwarenessManager.activate("command.偷窺.done", res_ctx)

@make_command(brief="看群內最愛排行榜")
def cmd_最愛排行(req_ctx):
    res_ctx = FavRankingContext(req_ctx, req_ctx.guild)
    AwarenessManager.activate("command.最愛排行.done", res_ctx)

@make_command(grammar="<favorites|Brand,Creator,Galgame|,>", brief="將公司、創作家、遊戲設成最愛")
def cmd_加最愛(req_ctx, favorites):
    found, not_found = favorites

    #add to favorite
    new_favorites = set()
    already_favorites = set()
    with req_ctx.db.session() as db_session:
        for obj in found:
            obj = db_session.merge(obj, load=False)
            #special case
            if isinstance(obj, Creator):
                obj = obj.main_identity if obj.main_identity else obj

            user_favorite = UserFavorite()
            user_favorite.user_id = req_ctx.user.id
            user_favorite.target_type = obj.__class__.__name__
            user_favorite.target_id = obj.id
            try:
                #this rollsbacks automatically if action failed
                with db_session.begin_nested():
                    db_session.merge(user_favorite)
                new_favorites.add(obj.name)
            except sqlalchemy.exc.IntegrityError:
                #these are already our favorites
                already_favorites.add(obj.name)

    res_ctx = ResponseContext(req_ctx)
    res_ctx.not_found = not_found
    res_ctx.new_favorites = new_favorites
    res_ctx.already_favorites = already_favorites
    AwarenessManager.activate("command.加最愛.done", res_ctx)

@make_command(grammar="<favorites|Brand,Creator,Galgame|,>", brief="刪除最愛")
def cmd_刪最愛(req_ctx, favorites):
    found, not_found = favorites

    #remove from to_remove
    removed = set()
    not_favorites = set()
    with req_ctx.db.session() as db_session:
        for obj in found:
            obj = db_session.merge(obj, load=False)
            #special case
            if isinstance(obj, Creator):
                obj = obj.main_identity if obj.main_identity else obj

            count = db_session.query(UserFavorite)  \
                            .filter_by(user_id=req_ctx.user.id,
                                       target_type=obj.__class__.__name__,
                                       target_id=obj.id).delete()
            if count > 0:
                removed.add(obj.name)
            else:
                not_favorites.add(obj.name)

    res_ctx = ResponseContext(req_ctx)
    res_ctx.not_found = not_found
    res_ctx.removed = removed 
    res_ctx.not_favorites = not_favorites 
    AwarenessManager.activate("command.刪最愛.done", res_ctx)

@make_command(grammar="[<@user>]", brief="看自己的坑有多深。在指令後@人也可以看其他人的待玩列表")
def cmd_待玩(req_ctx, user=None):
    if not user:
        user = req_ctx.user

    res_ctx = UserToPlayContext(req_ctx, user)
    AwarenessManager.activate("command.待玩.done", res_ctx)

@make_command(grammar="<to_play|Galgame|,>", brief="坑不夠多? 來多加點坑吧!")
def cmd_加待玩(req_ctx, to_play):
    found, not_found = to_play

    #add to favorite
    new = set()
    already = set()
    with Metadata(req_ctx.db, req_ctx.user, "to_play") as metadata:
        to_play = metadata.get("to_play", [])
        for galgame in found:
            if not galgame.id in to_play:
                new.add(galgame.name)
                to_play.append(galgame.id)
            else:
                already.add(galgame.name)
        metadata["to_play"] = to_play

    res_ctx = ResponseContext(req_ctx)
    res_ctx.not_found = not_found
    res_ctx.new = new
    res_ctx.already = already
    AwarenessManager.activate("command.加待玩.done", res_ctx)

@make_command(grammar="<to_play|Galgame|,>", brief="坑太多了? 刪一些吧")
def cmd_刪待玩(req_ctx, to_play):
    found, not_found = to_play 

    #remove from to_remove
    removed = set()
    not_in_to_play = set()
    with Metadata(req_ctx.db, req_ctx.user, "to_play") as metadata:
        to_play = metadata.get("to_play", [])
        for galgame in found:
            try:
                to_play.remove(galgame.id)
                removed.add(galgame.name)
            except ValueError:
                not_in_to_play.add(galgame.name)
        metadata["to_play"] = to_play

    res_ctx = ResponseContext(req_ctx)
    res_ctx.not_found = not_found
    res_ctx.removed = removed 
    res_ctx.not_in_to_play = not_in_to_play
    AwarenessManager.activate("command.刪待玩.done", res_ctx)
