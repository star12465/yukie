from .command import make_command
from thanatos.models import *
from thanatos.contexts import *
from thanatos.utils import Metadata
from thanatos.managers import *

@make_command(brief="貧乳或巨乳?")
def cmd_正義的大小(req_ctx):
    with Metadata(req_ctx.db, req_ctx.user, "busty_or_flaty") as metadata:
        count = metadata["count"]
        if count == None:
            count = 0
        count += 1
        metadata["count"] = count

    res_ctx = ResponseContext(req_ctx)
    res_ctx.count = count
    res_ctx.size = "貧乳" if req_ctx.user.id % 2 else "巨乳"
    AwarenessManager.activate("command.正義的大小.done", res_ctx)
