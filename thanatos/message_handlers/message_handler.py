__all__ = ["MessageHandler"]

class MessageHandler(object):
    def __init__(self, thanatos):
        self.thanatos = thanatos
        self.loop = thanatos.loop
        self.executor = thanatos.executor

    def handle_message(self, req_ctx):
        raise NotImplementedError()
