import re
import asyncio
import unicodedata
import logging
import concurrent.futures
from datetime import datetime
from copy import copy
from enum import IntEnum

from sqlalchemy.sql.expression import extract
from thanatos.utils import FastQuery, normalize_str
from .message_handler import MessageHandler
from thanatos.models import *
from thanatos.contexts import *
from thanatos.managers import *

__all__ = ["QueryContentType", "QueryHandler"]

logger = logging.getLogger(__name__)

class QueryType(IntEnum):
    Unknown = 0
    Query = 1
    QueryGalgamesMonthly = 2
    Search = 3

class QueryContentType(IntEnum):
    All = 0xffffffff
    Unknown = 0
    Galgame = 1
    Video = 1 << 1
    Creator = 1 << 2
    Brand = 1 << 3
    Character = 1 << 4

class QueryHandler(MessageHandler):
    def __init__(self, thanatos):
        super().__init__(thanatos)

        #init fast query for games
        self.fq_games = FastQuery()
        with thanatos.db.session() as db_session:
            self.fq_games.add_dataset((game.name, game.id) for game in db_session.query(Galgame).all())

        self.fq_all = FastQuery()
        with thanatos.db.session() as db_session:
            self.fq_all.add_dataset((creator.name, (Creator, creator.id)) for creator in db_session.query(Creator).all())
            self.fq_all.add_dataset((brand.name, (Brand, brand.id)) for brand in db_session.query(Brand).all())
            self.fq_all.add_dataset((game.name, (Galgame, game.id)) for game in db_session.query(Galgame).all())

            #we don't want to get the same character twice
            q = db_session.query(Character).filter_by(parent_id=None)
            self.fq_all.add_dataset((char.name, (Character, char.id)) for char in q.all())

    async def handle_message(self, req_ctx):
        msg = req_ctx.msg
        if len(msg) >= 2 and msg[0] in ("?", "？"):
            if unicodedata.category(msg[1])[0] in ("L", "N"):
                req_ctx.query_type = QueryType.Unknown
                req_ctx.keyword = msg[1:].strip()

                m = re.search(r'(?:(\d{2,4})年)?(?:(\d{1,2})月)?作', req_ctx.keyword)
                if m:
                    year = int(m.group(1)) if m.group(1) else None
                    month = int(m.group(2)) if m.group(2) else None
                    current_year = datetime.now().year
                    if not month or month < 1 or month > 12:
                        month = None

                    #parse year
                    if year and year < 100:
                            #we only have the lower digits
                            if year <= ((current_year+1)%100):
                                year = (current_year//100*100)+year
                            else:
                                year = ((current_year//100-1)*100)+year

                    #validate year
                    if year:
                        if year < 1990 or year > current_year+1:
                            year = None
                    elif month:
                        #year not supplied but month is supplied
                        year = current_year

                    if year:
                        req_ctx.query_type = QueryType.QueryGalgamesMonthly
                        req_ctx.year = year
                        req_ctx.month = month

                if req_ctx.query_type == QueryType.Unknown:
                    req_ctx.query_type = QueryType.Query
            elif msg[1] in ("?", "？"):
                req_ctx.query_type = QueryType.Search
                req_ctx.keyword = msg[2:].strip()
            else:
                #nothing
                req_ctx.keyword = ""

            #make sure we have an effective keyword
            if len(normalize_str(req_ctx.keyword)) > 0:
                AwarenessManager.activate("query.start", req_ctx)

                logger.info("#{} [{}] {}".format(req_ctx.discord_msg.channel,
                                                 req_ctx.discord_msg.author,
                                                 req_ctx.msg))

                if req_ctx.query_type == QueryType.Query and req_ctx.keyword == "help":
                    res_ctx = ResponseContext(req_ctx)
                    AwarenessManager.activate("query.help.done", res_ctx)
                else:
                    req_ctx.query_content_type = QueryContentType.Unknown
                    if req_ctx.query_type == QueryType.Query:
                        fn = self.query
                    elif req_ctx.query_type == QueryType.QueryGalgamesMonthly:
                        fn = self.query_galgames_monthly
                    elif req_ctx.query_type == QueryType.Search:
                        fn = self.search

                    #query
                    waiting_ctx = None
                    task = self.loop.run_in_executor(self.executor, fn, req_ctx)
                    try:
                        res_ctxs = await asyncio.wait_for(asyncio.shield(task), 2, loop=self.loop)
                    except concurrent.futures._base.TimeoutError:
                        #create waiting message
                        req_ctx.waiting_ctx = ResponseContext(req_ctx)
                        AwarenessManager.activate("query.loading", req_ctx.waiting_ctx)

                        #wait for task to be finished
                        res_ctxs = await task

                    if res_ctxs:
                        #TODO: for now we'll just return the first item
                        AwarenessManager.activate("query.done", res_ctxs[0])
                    else:
                        res_ctx = ResponseContext(req_ctx)
                        AwarenessManager.activate("query.not_found", res_ctx)

    def search(self, req_ctx):
        results = self.fq_all.find_similar(req_ctx.keyword, 50, 0.6)
        logger.info(results)
        if len(results) > 0:
            return [SearchResultsContext(req_ctx, results)]
        return results

    def query_galgames_monthly(self, req_ctx):
        #update prerelease data
        with req_ctx.db.session() as db_session:
            q = db_session.query(Galgame).filter(extract("year", Galgame.release_date) == req_ctx.year)
            if req_ctx.month:
                q = q.filter(extract("month", Galgame.release_date) == req_ctx.month)
            q = q.filter(Galgame.release_date>datetime.now())
            galgames = q.all()
        ProvidersManager.update(req_ctx.db, galgames, Galgame.Component.Prerelease)

        res_ctx = GalgamesMonthlyContext(req_ctx, req_ctx.year, req_ctx.month)
        return [res_ctx]

    def query(self, req_ctx, query_content_type=QueryContentType.All):
        res_ctxs = []
        keyword = req_ctx.keyword

        #check for brands
        if (query_content_type & int(QueryContentType.Brand)) > 0:
            brands = []
            with req_ctx.db.session() as db_session:
                brands = Brand.get_by_name(db_session, keyword, True)
            for brand in brands:
                res_ctxs.append(BrandContext(req_ctx, brand))

        #check for creators
        if (query_content_type & int(QueryContentType.Creator)) > 0:
            creators = []
            with req_ctx.db.session() as db_session:
                creators = Creator.get_by_name(db_session, keyword, True)
            for creator in creators:
                creator.update(req_ctx.db)
                res_ctxs.append(CreatorContext(req_ctx, creator))

        #check for characters
        if (query_content_type & int(QueryContentType.Character)) > 0:
            characters = []
            with req_ctx.db.session() as db_session:
                characters = Character.get_by_name(db_session, keyword, True)
            for character in characters:
                character.update(req_ctx.db)
                res_ctxs.append(CharacterContext(req_ctx, character))

        #check for video keywords
        if (query_content_type & int(QueryContentType.Video)) > 0:
            m = re.match(r'(.*?)\s+([A-z]+|影片)(\d+)?$', keyword)

            #fake while loop: easier for us to break out
            while m:
                list_mode = False
                galgame_keyword, video_type_str, video_n = m.group(1), m.group(2), m.group(3)
                if video_type_str == "影片":
                    list_mode = True
                    video_type = VideoType.All
                else:
                    #parse video type
                    video_type = VideoType.from_code(video_type_str)
                    if video_type == None:
                        #list mode?
                        if video_type_str[-1] == "s":
                            video_type = VideoType.from_code(video_type_str[:-1])
                            list_mode = True

                    if video_type == None:
                        break

                #find game
                galgame_id = self.fq_games.find_most_likely(galgame_keyword)
                if galgame_id == None:
                    break
                with req_ctx.db.session() as db_session:
                    galgame = db_session.query(Galgame).filter(Galgame.id == galgame_id).one()

                req_ctx.query_content_type = QueryContentType.Video
                req_ctx.galgame = galgame
                req_ctx.video_type = video_type
                req_ctx.galgame_keyword = galgame_keyword

                #update videos
                req_ctx.galgame.update(req_ctx.db, Galgame.Component.Videos)
                res_ctx = None
                if list_mode:
                    res_ctxs.append(VideosListContext(req_ctx, req_ctx.galgame))
                else:
                    video_n = int(video_n) if video_n else 1
                    req_ctx.video_n = video_n

                    video = req_ctx.galgame.get_video(req_ctx.db, video_type, video_n)
                    if video:
                        res_ctxs.append(VideoContext(req_ctx, video))
                    else:
                        #video not found
                        #we want to return here so we don't end up
                        #doing a fuzzy search on games
                        return res_ctxs
                break

        #search for galgame
        if (query_content_type & int(QueryContentType.Galgame)) > 0:
            galgames = []
            with req_ctx.db.session() as db_session:
                galgames = Galgame.get_by_name(db_session, keyword, True)
            for galgame in galgames:
                galgame.update(req_ctx.db, Galgame.Component.Traits)
                res_ctxs.append(GalgameContext(req_ctx, galgame))

            #do fuzzy search only if we didn't find anything
            if len(res_ctxs) == 0:
                galgame_id = self.fq_games.find_most_likely(keyword)
                if galgame_id != None:
                    galgame = None
                    with req_ctx.db.session() as db_session:
                        galgame = db_session.query(Galgame).filter(Galgame.id == galgame_id).one()
                    if galgame:
                        galgame.update(req_ctx.db, Galgame.Component.Traits)
                        res_ctxs.append(GalgameContext(req_ctx, galgame))
        return res_ctxs
