# -*- coding: utf-8 -*-
import enum
import logging
import time
import threading
from sqlalchemy import inspect, Integer, Column, DateTime, inspect, String, or_, and_, MetaData
from datetime import timedelta, datetime, date
from sqlalchemy.orm.attributes import set_committed_value
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative.api import DeclarativeMeta

__all__ = ["metadata", "Base", "Updatable", "NameSearchable", "Favoritable"]

logger = logging.getLogger(__name__)

metadata = MetaData(naming_convention={
    "ix": 'ix_%(table_name)s__%(column_0_name)s',
    "uq": "uq_%(table_name)s__%(column_0_name)s",
    "fk": "fk_%(table_name)s__%(column_0_name)s__%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
})

#add this so we can do comparison such as Base < Base
def declaritvemeta_lt(self, other):
    return True
DeclarativeMeta.__lt__ = declaritvemeta_lt.__get__(DeclarativeMeta, DeclarativeMeta)
Base = declarative_base(metadata=metadata, metaclass=DeclarativeMeta)

class Updatable:
    class Component(enum.IntEnum):
        Core = 1
        All = 0xffffffff

    def should_update(self, components, last_updated):
        '''
        Whether we should check update from remote resources for this model again
        Classes inheriting this class needs to override this method
        '''
        raise NotImplementedError()

    def get_last_updated(self, db, provider_cls, components):
        from thanatos.utils import Metadata

        earliest = float("inf")
        metadata = Metadata(db, self, "{}_last_updated".format(provider_cls.__name__))
        for component in self.__class__.Component:
            if (component & components) > 0 and component != self.__class__.Component.All:
                earliest = min(metadata.get(component, float("inf")), earliest)
        if earliest == float("inf"):
            earliest = 0

        return datetime.fromtimestamp(earliest)

    def mark_updated(self, db, provider_cls, components):
        from thanatos.utils import Metadata

        metadata = Metadata(db, self, "{}_last_updated".format(provider_cls.__name__))
        for component in self.__class__.Component:
            if (component & components) > 0 != component != self.__class__.Component.All:
                metadata[component] = datetime.now().timestamp()

    def update(self, db, components=Component.Core):
        from thanatos.managers import ProvidersManager

        #we always want to update core
        components |= self.Component.Core

        #and start updating
        logger.info("Updating {} from providers".format(self))
        ProvidersManager.update(db, [self], components)

        #update fields
        with db.session() as db_session:
            obj = db_session.merge(self, load=False)
            db_session.refresh(obj)
            for column in self.__table__.columns:
                set_committed_value(self, column.key, getattr(obj, column.key))

class NameSearchable:
    normalized_name = Column(String, nullable=False, index=True)

    @classmethod
    def get_by_name(cls, db_session, name, return_all=False):
        from thanatos.utils import normalize_str
        name = name.strip()
        if len(name) == 0:
            return [] if return_all else None

        #exact
        q = db_session.query(cls).filter(cls.name == name)
        if q.count() >= 1:
            return q.all() if return_all else q.first()

        #paranthesis
        q = db_session.query(cls).filter(or_(
                    cls.name.startswith("{}(".format(name)),
                    cls.name.endswith("({})".format(name))))
        if q.count() >= 1:
            return q.all() if return_all else q.first()

        #normalized+paranthesis
        normalized = normalize_str(name)
        if len(normalized) > 0:
            #we need to search harder
            q = db_session.query(cls).filter(or_(
                    cls.normalized_name == normalized,
                    cls.normalized_name.startswith("{}(".format(normalized)),
                    cls.normalized_name.endswith("({})".format(normalized))))
            if q.count() >= 1:
                return q.all() if return_all else q.first()
        return [] if return_all else None

class Favoritable:
    def is_fav_by_user(self, user):
        from thanatos.models import UserFavorite
        db_session = inspect(self).session
        q = db_session.query(UserFavorite).filter_by(user_id=user.id,
                                                     target_type=self.__class__.__name__,
                                                     target_id=self.id)

        return q.count() > 0

    def get_fav_count(self, guild):
        from thanatos.models import User, UserFavorite
        db_session = inspect(self).session

        q = db_session.query(UserFavorite)
        if guild:
            q = q.filter(and_(User.id == UserFavorite.user_id,
                              User.guilds.any(id=guild.id)))
        q = q.filter_by(target_type=self.__class__.__name__, target_id=self.id)
        return q.count()
