# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date, or_
from sqlalchemy.orm import relationship
from .base import Base, Updatable, NameSearchable, Favoritable

class Brand(Base, NameSearchable, Favoritable):
    __tablename__ = 'brands'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, index=True, nullable=False)
    official_url = Column(String)
    kind = Column(String)
    galgames = relationship("Galgame", back_populates="brand", order_by="desc(Galgame.release_date)")

    def __repr__(self):
       return "<Brand(id={},name={})>".format(self.id, self.name)
