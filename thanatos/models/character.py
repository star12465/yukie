# -*- coding: utf-8 -*-
import logging
from enum import IntEnum
from gettext import gettext as _
from sqlalchemy import inspect, Column, Integer, String, ForeignKey, Boolean, UniqueConstraint, or_, and_
from sqlalchemy.orm import relationship, backref
from .base import *
from thanatos.utils import StringNormalizer

__all__ = ["CharacterGender", "CharacterLevel", "Character"]

logger = logging.getLogger(__name__)

class CharacterGender(IntEnum):
    Male = 0
    Female = 1
    Other = 2

    @staticmethod
    def to_string(gender):
        return (_("男性"), _("女性"), _("其他"))[gender]

class CharacterLevel(IntEnum):
    Main = 1
    Sub = 2
    Other = 3

class Character(Base, NameSearchable, Updatable, Favoritable):
    __tablename__ = 'characters'

    #we don't use ergs's character id here because its database is way too lacking
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, index=True)
    furigana = Column(String)
    #maps to CharacterGenger
    gender = Column(Integer)
    blood_type = Column(String)
    #we don't use Date here becaus year is not available
    birthday = Column(String)

    display_order = Column(Integer)

    galgame_id = Column(Integer, ForeignKey("galgames.id"), nullable=False)
    galgame = relationship("Galgame", back_populates="characters", uselist=False)
    #maps to CharacterLevel
    level = Column(Integer, nullable=False)

    cv_id = Column(Integer, ForeignKey("creators.id"))
    cv = relationship("Creator", uselist=False)

    parent_id = Column(Integer, ForeignKey("characters.id"))
    parent = relationship("Character", back_populates="children", remote_side=id)
    children = relationship("Character", back_populates="parent")

    #this maps to ergs table appearance
    spoiler = Column(Boolean, default=False, nullable=False)
    url = Column(String)
    description = Column(String)
    age = Column(Integer)
    bust_size = Column(Integer)
    waist_size = Column(Integer)
    hip_size = Column(Integer)
    height = Column(Integer)
    weight = Column(Integer)
    cup_size = Column(String)

    image_url = Column(String)

    #ergs characterlist.id
    ergs_id = Column(Integer)

    __table_args__ = (UniqueConstraint("ergs_id", "galgame_id"),)

    def __repr__(self):
        return "<Character(id={},name={})>".format(self.id, self.name)

    def should_update(self, components, last_updated):
        return False

    @classmethod
    def get_by_game_and_name(cls, db_session, galgame, name):
        normalized = normalize_str(name)
        q = db_session.query(cls).filter(and_(cls.galgame == galgame,
                                              or_(cls.normalized_name == normalized,
                                                  cls.normalized_name.startswith("{}(".format(normalized)),
                                                  cls.normalized_name.endswith("({})".format(normalized)))))
        if q.count() > 0:
            if q.count() > 1:
                logger.warning("Found {} != 0 characters for game={} and name={}".format(q.count(), galgame.id, name))
            return q.first()
        return None
