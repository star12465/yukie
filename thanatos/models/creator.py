# -*- coding: utf-8 -*-
import heapq
import logging
import enum
from datetime import datetime
from itertools import chain
from sqlalchemy.sql import expression
from sqlalchemy import inspect, Boolean, Column, Integer, String, ForeignKey, DateTime, and_, func
from sqlalchemy.orm import relationship, backref
from .base import Base, Updatable, NameSearchable, Favoritable
from .work import WorkType

__all__ = ["Creator"]

logger = logging.getLogger(__name__)

class Creator(Base, NameSearchable, Updatable, Favoritable):
    __tablename__ = 'creators'

    #erogamescape creator id
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True, index=True)

    works = relationship("Work", back_populates="creator", uselist=True)

    main_identity_id = Column(Integer, ForeignKey("creators.id"))
    main_identity = relationship("Creator", backref="other_identities", remote_side=id)
    fix_main_identity = Column(Boolean, server_default=expression.false(), nullable=False)

    homepage_url = Column(String)
    twitter_id = Column(String)

    seiyuumatome_id = Column(Integer)
    gamaplaza_id = Column(String)
    vndb_id = Column(Integer)

    class Component(enum.IntEnum):
        Core = 1
        All = 0xffffffff

    def __repr__(self):
        return "<Creator(id=%d,name=%r)>" % (self.id, self.name)

    def __lt__(self, o):
        return self.name < o.name

    def should_update(self, components, last_updated):
        now = datetime.now()
        return (now - last_updated).days >= 5

    def is_fav_by_user(self, user):
        if not self.main_identity:
            return super().is_fav_by_user(user)
        else:
            creator = self.main_identity
            return creator.is_fav_by_user(user)

    def get_types(self):
        counts = {wt: 0 for wt in WorkType}
        total = len(self.works)
        for work in self.works:
            counts[work.type] += 1
        ratio = {wt: counts[wt]/total for wt in WorkType}

        THRESHOLD = 0.35
        res = []
        for wt in WorkType:
            if ratio[wt] > THRESHOLD:
                res.append(wt)
        return res

    def get_works_rating_sorted(self):
        all_identities = self.get_all_identities()

        best_works = []
        for creator in all_identities:
            for work in creator.works:
                rating = work.get_value()
                heapq.heappush(best_works, (rating, work))

        res = []
        while len(best_works) > 0:
            res.append(heapq.heappop(best_works)[1])
        return res[::-1]

    def get_all_identities(self):
        main_identity = self.main_identity if self.main_identity else self
        all_identities = main_identity.other_identities[:]
        all_identities.append(main_identity)
        return all_identities

    def get_rating(self):
        #FIXME: work value should decrease if the creator does 2 different works on the same game
        return sum(w.get_value() for w in self.works) if len(self.works) > 0 else 0

    def set_as_main_identity(self, creators=[], force=False):
        from thanatos.models import UserFavorite

        db_session = inspect(self).session
        all_creators = list(chain.from_iterable([x.get_all_identities() for x in [self] + creators]))

        fix_main_identity = False
        main_identity = None
        if not force:
            #if force flag is not set, we'll have to make sure
            #we'll have to obey the fix_main_identity flag
            for creator in all_creators:
                if creator.fix_main_identity:
                    fix_main_identity = True

                    if not main_identity:
                        main_identity = creator
                    else:
                        #we already found another creator
                        #that has fix_main_identity set to true
                        return False
        if not main_identity:
            #we'll be the main identity if force flag is set
            #or if no one has fix_main_identity set
            main_identity = self
            
        #start clean, or else sqlalchemy will complain about Circular dependencies
        main_identity.other_identities.clear()
        db_session.commit()
        db_session.expire_all()

        creator_ids = []
        seiyuumatome_id, gamaplaza_id, vndb_id = None, None, None
        for creator in all_creators:
            creator_ids.append(creator.id)

            #we need to make sure the main identity gets all the ids
            if creator.seiyuumatome_id != None:
                seiyuumatome_id = creator.seiyuumatome_id
            if creator.gamaplaza_id != None:
                gamaplaza_id = creator.gamaplaza_id
            if creator.vndb_id != None:
                vndb_id = creator.vndb_id

            #reset fix main identity status
            creator.fix_main_identity = False

            if creator == main_identity:
                creator.main_identity = None
            else:
                creator.main_identity = main_identity

        #update provider ids
        main_identity.seiyuumatome_id = seiyuumatome_id
        main_identity.gamaplaza_id = gamaplaza_id
        main_identity.vndb_id = vndb_id

        #update fix main identity status
        main_identity.fix_main_identity = fix_main_identity or force

        #delete rows that will end up being duplicates
        #i.e. if we're merging creator A and B
        #and a user has both A and B as his favorite
        #one of them needs to go away
        db_session.execute(expression.delete(UserFavorite).where(and_(
                    UserFavorite.target_type == self.__class__.__name__,
                    UserFavorite.target_id.in_(creator_ids),    \
                    ~UserFavorite.id.in_(
                        db_session.query(func.min(UserFavorite.id))  \
                                  .filter(UserFavorite.target_type == self.__class__.__name__)  \
                                  .filter(UserFavorite.target_id.in_(creator_ids))    \
                                  .group_by(UserFavorite.user_id)))))

        #now update all the target_ids to point to this main identity
        db_session.execute(expression.update(UserFavorite).where(and_(
                    UserFavorite.target_type == self.__class__.__name__,
                    UserFavorite.target_id.in_(creator_ids))).values(target_id=main_identity.id))
        db_session.commit()
        return True

    def redecide_main_identity(self, force=False):
        all_identities = self.get_all_identities()
            
        best = float("-inf"), None
        for creator in all_identities:
            rating = creator.get_rating()
            if rating > best[0]:
                best = rating, creator

        return best[1].set_as_main_identity(force=force)
