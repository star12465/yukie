# -*- coding: utf-8 -*-
import logging
import enum
from datetime import datetime, time, date, timedelta
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.sql.functions import coalesce
from .base import *

logger = logging.getLogger(__name__)

__all__ = ["Galgame"]

class Galgame(Base, Updatable, NameSearchable, Favoritable):
    __tablename__ = 'galgames'

    #erogamescape game id
    id = Column(Integer, primary_key=True)
    name = Column(String, index=True, nullable=False)
    official_url = Column(String)
    banner_url = Column(String)
    genre = Column(String)
    is_adult_rated = Column(Boolean)
    release_date = Column(Date)
    brand_id = Column(Integer, ForeignKey("brands.id"), nullable=False)
    brand = relationship("Brand", back_populates="galgames")
    playtime_median = Column(Integer)
    getchu_id = Column(Integer)
    trial_url = Column(String)
    masterup_date = Column(Date)

    staffs = relationship("Work", back_populates="galgame", uselist=True)
    trait_votes = relationship("GalgameTraitVote",
                               order_by="(desc(GalgameTraitVote.rank_a), desc(GalgameTraitVote.rank_b), desc(GalgameTraitVote.rank_c))",
                               back_populates="galgame", uselist=True)

    videos = relationship("Video", back_populates="galgame",
                                   order_by="asc(Video.release_date)", cascade="delete, delete-orphan")
    characters = relationship("Character", back_populates="galgame", uselist=True)

    #ergs votes
    votes_ergs_median = Column(Integer)
    votes_ergs_stdev = Column(Integer)
    votes_ergs_count = Column(Integer, default=0)

    #erogetrailers
    erogetrailers_id = Column(Integer)

    #vndb
    vndb_id = Column(Integer)
    votes_vndb_average = Column(Integer)
    votes_vndb_count = Column(Integer, default=0)

    class Component(enum.IntEnum):
        Core = 1
        Traits = 1 << 1
        Videos = 1 << 2
        Prerelease = 1 << 3
        All = 0xffffffff

    def __repr__(self):
        return "<Galgame(id=%d,name=%r)>" % (self.id, self.name)

    @hybrid_property
    def popularity(self):
        if not self.votes_ergs_median or not self.votes_ergs_count:
            return 0
        return self.votes_ergs_median * self.votes_ergs_count

    @popularity.expression
    def popularity(cls):
        return coalesce(cls.votes_ergs_median, 0) * coalesce(cls.votes_ergs_count, 0)

    def should_update(self, component, last_checked_date):
        now = datetime.now()
        release_datetime = datetime.combine(self.release_date, time())
        if release_datetime > now:
            #game yet to be release, check everyday
            return (now - last_checked_date).days >= 1
        else:
            delta = now - release_datetime
            if delta.days < 14:
                #game just release, check every 12 hours
                return (now - last_checked_date) >= timedelta(hours=12)
            elif delta.days < 90:
                return (now - last_checked_date).days >= 1
            else:
                return (now - last_checked_date).days >= 7

    def get_video(self, db, video_type, video_n):
        #find the right video
        res = None
        with db.session() as db_session:
            galgame = db_session.merge(self, load=False)
            i = 1
            for video in galgame.videos:
                if video.type != video_type:
                    continue
                if video_n == i:
                    res = video
                    break
                i += 1
        return res
