# -*- coding: utf-8 -*-
from enum import IntEnum
from sqlalchemy import Column, Integer, String, Boolean
from .base import Base
from sqlalchemy.orm import relationship

class GalgameTraitCategory(IntEnum):
    Property = 0            #属性
    Genre = 1               #ジャンル
    TheBad = 2              #ネガティブ
    Summary = 3             #ワンポイント
    Art = 4                 #グラフィック
    Gamability = 5          ##ゲーム性
    #ゲームの内容外
    Protagonist = 7         #主人公
    Writing = 8             #シナリオ
    System = 9              #システム
    Condition = 10          #条件
    Theme = 11              #傾向
    Sound = 12              #音
    GirlTrait = 13          #女の子キャラクター
    Masterpiece = 14        #名作
    TheGood = 15            #ここがいい
    Time = 16               #時間
    HSceneType = 17         #エロシーン
    Background = 18         #背景
    Situation = 19          #シチュエーション
    Character = 20          #キャラクター

class GalgameTrait(Base):
    __tablename__ = 'galgame_traits'

    #same as erogamescape povlist.id
    id = Column(Integer, primary_key=True)

    name = Column(String, unique=True, nullable=False)
    chinese_name = Column(String, unique=True, nullable=False)
    category = Column(Integer, index=True, nullable=False)
    spoiler = Column(Boolean, nullable=False)

    votes = relationship("GalgameTraitVote", back_populates="trait", uselist=True)

    def __repr__(self):
       return "<GalgameTrait(id={},name={})>".format(self.id, self.name)
