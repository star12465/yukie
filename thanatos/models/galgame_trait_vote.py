# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from .base import Base
from sqlalchemy.orm import relationship

class GalgameTraitVote(Base):
    __tablename__ = 'galgame_trait_votes'

    trait_id = Column(Integer, ForeignKey("galgame_traits.id"), nullable=False, primary_key=True)
    trait = relationship("GalgameTrait", back_populates="votes", uselist=False)
    galgame_id = Column(Integer, ForeignKey("galgames.id"), nullable=False, primary_key=True)
    galgame = relationship("Galgame", back_populates="trait_votes", uselist=False)

    rank_a = Column(Integer, index=True, default=0)
    rank_b = Column(Integer, index=True, default=0)
    rank_c = Column(Integer, index=True, default=0)

    def __repr__(self):
       return "<GalgameTraitVote(trait_name={},galgame_name={})>".format(self.trait.name, self.galgame.name)

    def get_score(self):
        return self.rank_a + self.rank_b
