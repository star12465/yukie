# -*- coding: utf-8 -*-
from sqlalchemy import Column, BigInteger, String, ForeignKey
from sqlalchemy.orm import relationship
from .base import Base

class Guild(Base):
    __tablename__ = 'guilds'

    #discord id, discord guild name
    id = Column(BigInteger, primary_key=True)
    name = Column(String, index=True, nullable=False)

    users = relationship("User", secondary="users_guilds", back_populates="guilds", lazy="joined")

    def __repr__(self):
       return "<Guild(id={},name={})>".format(self.id, self.name)

    @staticmethod
    def create_or_get(db_session, discord_guild):
        guild = Guild()
        guild.id = discord_guild.id
        guild.name = discord_guild.name
        guild = db_session.merge(guild)
        db_session.commit()
        db_session.refresh(guild)
        return guild
