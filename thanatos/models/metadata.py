# -*- coding: utf-8 -*-
from sqlalchemy import Column, BigInteger, Integer, String, Boolean, ForeignKey, Date, UniqueConstraint
from sqlalchemy.orm import relationship
from .base import Base

#T for table
#to prevent naming collision with Metadata in utils
class MetadataT(Base):
    __tablename__ = 'metadata'

    target_type = Column(String, primary_key=True, nullable=False)
    target_id = Column(BigInteger, primary_key=True, nullable=False)
    namespace = Column(String, primary_key=True, nullable=False)
    key = Column(String, primary_key=True, nullable=False)
    value = Column(String)

    def __repr__(self):
       return "<MetadataT(target_type={},target_id={},namespace={},key={},value={})>".format(self.target_type,
                                                    self.target_id, self.namespace, self.key, self.value)
