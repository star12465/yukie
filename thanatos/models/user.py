# -*- coding: utf-8 -*-
import datetime
from sqlalchemy import Column, Boolean, Integer, BigInteger, String, ForeignKey, DateTime, UniqueConstraint
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from .base import Base

class User(Base):
    __tablename__ = 'users'

    #discord id, discord username, discord discriminator, discord nickname
    id = Column(BigInteger, primary_key=True)
    username = Column(String, index=True, nullable=False)
    discriminator = Column(String, nullable=False)
    nickname = Column(String, index=True)

    guilds = relationship("Guild", secondary="users_guilds", back_populates="users", lazy="joined")

    privilege = Column(Integer, default=50)

    last_online = Column(DateTime, default=func.now())
    last_spoken = Column(DateTime)
    is_online = Column(Boolean, default=False, nullable=False)
    is_bot = Column(Boolean, default=False, nullable=False)

    favorites = relationship("UserFavorite", back_populates="user", uselist=True)

    __table_args__ = (UniqueConstraint("username", "discriminator"),)

    def __repr__(self):
       return "<User(id={},username={})>".format(self.id, self.username)

    @staticmethod
    def create_or_get(db_session, discord_user, spoken=False):
        now = datetime.datetime.now()

        user = User()
        user.id = discord_user.id 
        user.username = discord_user.name
        user.discriminator = discord_user.discriminator
        user.nickname = discord_user.display_name
        user.is_bot = discord_user.bot
        user.last_online = now
        if spoken:
            user.last_spoken = now

        user = db_session.merge(user)
        db_session.commit()
        db_session.refresh(user)
        return user
