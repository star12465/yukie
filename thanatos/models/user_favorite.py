# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date, UniqueConstraint
from sqlalchemy.orm import relationship
from .base import Base

class UserFavorite(Base):
    __tablename__ = 'user_favorites'

    id = Column(Integer, primary_key=True)

    user_id = Column(Integer, ForeignKey("users.id"), index=True, nullable=False)
    user = relationship("User", back_populates="favorites", uselist=False)

    target_type = Column(String, index=True, nullable=False)
    target_id = Column(Integer, index=True, nullable=False)

    __table_args__ = (UniqueConstraint("user_id", "target_type", "target_id"),)

    def __repr__(self):
       return "<UserFavorite(user_id={},target_type={},target_id={})>".format(self.user_id, self.target_type, self.target_id)
