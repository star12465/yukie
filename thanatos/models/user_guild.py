# -*- coding: utf-8 -*-
from sqlalchemy import Column, Boolean, Integer, BigInteger, String, ForeignKey, DateTime, UniqueConstraint
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from .base import Base

class UserGuild(Base):
    __tablename__ = 'users_guilds'

    user_id = Column(BigInteger, ForeignKey("users.id"), primary_key=True)
    guild_id = Column(BigInteger, ForeignKey("guilds.id"), primary_key=True)

    def __repr__(self):
       return "<UserGuild(user={}#{},guild={})>".format(self.user.username,
                                                        self.user.discriminator,
                                                        self.guild.name)
