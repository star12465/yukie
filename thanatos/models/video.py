# -*- coding: utf-8 -*-
from enum import IntEnum
from gettext import gettext as _
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship
from .base import Base

class VideoType(IntEnum):
    OP = 0
    ED = 1
    CM = 2
    Countdown = 3
    PlayVideo = 4
    PV = 5
    Other = 6
    Radio = 7
    All = 8

    @staticmethod
    def from_code(s):
        TL = {"op": VideoType.OP,
              "ed": VideoType.ED,
              "cm": VideoType.CM,
              "countdown": VideoType.Countdown,
              "playvideo": VideoType.PlayVideo,
              "pv": VideoType.PV,
              "other": VideoType.Other,
              "radio": VideoType.Radio}
        if s.lower() not in TL:
            return None
        return TL[s.lower()]

    @staticmethod
    def to_code(s):
        TL = {VideoType.OP: "op",
              VideoType.ED: "ed",
              VideoType.CM: "cm",
              VideoType.Countdown: "countdown",
              VideoType.PlayVideo: "playvideo",
              VideoType.PV: "pv",
              VideoType.Other: "other",
              VideoType.Radio: "radio"}
        return TL[s]

    @staticmethod
    def to_string(s):
        TL = {VideoType.OP: _("OP"),
              VideoType.ED: _("ED"),
              VideoType.CM: _("廣告"),
              VideoType.Countdown: _("倒數影片"),
              VideoType.PlayVideo: _("試玩影片"),
              VideoType.PV: _("宣傳影片"),
              VideoType.Other: _("其他影片"),
              VideoType.Radio: _("廣播劇")}
        return TL[s]

class Video(Base):
    __tablename__ = 'videos'

    #we'll be using erogetrailer's id as our id
    #note: this is the *video id* on erogetrailers
    #this is different from the *soft id* of erogetrailers in Galgame
    id = Column(Integer, primary_key=True)

    type = Column(Integer, nullable=False)
    name = Column(String)
    release_date = Column(Date)
    youtube_id = Column(String)

    galgame_id = Column(Integer, ForeignKey("galgames.id"))
    galgame = relationship("Galgame", back_populates="videos")

    song_name = Column(String)
    song_artist_id = Column(Integer, ForeignKey("creators.id"))
    song_artist = relationship("Creator", foreign_keys=[song_artist_id])

    #from erogamescape
    votes_median = Column(Integer)
    votes_count = Column(Integer, default=0)

    def __repr__(self):
        if self.galgame:
            return "<Video(id={},galgame={},name={},type={})>".format(self.id, self.galgame.name, self.name, str(self.type))
        else:
            return "<Video(id={},name={},type={})>".format(self.id, self.name, str(self.type))
