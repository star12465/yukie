# -*- coding: utf-8 -*-
from enum import IntEnum
from gettext import gettext as _
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship
from .base import Base

#http://erogamescape.dyndns.org/~ap2/ero/toukei_kaiseki/sql_for_erogamer_tablelist.php
class WorkType(IntEnum):
    Artist = 1
    Story = 2
    Music = 3
    CharacterDesign = 4
    CV = 5
    Singer = 6
    Other = 7

    @staticmethod
    def to_string(wt):
        return (_("畫家"), _("劇本家"), _("音樂"), _("人設"), _("聲優"), _("歌手"), _("其他"))[wt-1]

#http://erogamescape.dyndns.org/~ap2/ero/toukei_kaiseki/sql_for_erogamer_tablelist.php
class WorkLevel(IntEnum):
    Main = 1
    Sub = 2
    Other = 3

class Work(Base):
    __tablename__ = 'works'

    #same as erogamescape shokushu.id
    id = Column(Integer, primary_key=True)

    creator_id = Column(Integer, ForeignKey("creators.id"), nullable=False)
    creator = relationship("Creator", back_populates="works", uselist=False)
    galgame_id = Column(Integer, ForeignKey("galgames.id"), nullable=False)
    galgame = relationship("Galgame", back_populates="staffs", uselist=False)
    type = Column(Integer, nullable=False)
    level = Column(Integer)
    note = Column(String)

    def __repr__(self):
        return "<Work(creator={},galgame={},type={})>".format(self.creator.name, self.galgame.name, str(self.type))

    def __lt__(self, o):
        return self.id < o.id

    def get_value(self):
        value = self.galgame.popularity ** 0.3
        value **= 1.3 if self.level == WorkLevel.Main else 1
        return value
