from thanatos.contexts import *

class Personality:
    def __init__(self, thanatos):
        self.thanatos = thanatos

    @property
    def name(self):
        raise NotImplementedError()

    @property
    def enabled(self):
        return False

    def awareness_handler(self, awareness, *args, **kwargs):
        levels = awareness.split(".")

        i = 0
        while len(levels) > 0:
            fn = "a_{}".format("_".join(levels))
            if hasattr(self, fn):
                return getattr(self, fn)(*args, **kwargs)
            levels.pop()

        #call default handler
        return self.a_default(awareness, *args, **kwargs)

    def a_default(self, awareness, *args, **kwargs):
        '''default action
        '''
        if len(args) > 0 and isinstance(args[0], ResponseContext):
            pass
            args[0].send()

    def __repr__(self):
        return "<{}>".format(self.__class__.__name__)
