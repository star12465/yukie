import asyncio
from . import PersonalityThanatos
from thanatos.message_handlers import QueryContentType
from thanatos.message_handlers.commands import CommandErrorCode
from thanatos.models import *
from thanatos.contexts import *

class PersonalityIroha(PersonalityThanatos):
    @property
    def name(self):
        return "いろは"

    @property
    def author(self):
        return "<@{}>".format(320446362304905216)

    @property
    def avatar_url(self):
        return "https://cdn.discordapp.com/attachments/351624216535105536/360090090254499841/vnr-capture-1280x720-20170825-220434.png"
    
    @property
    def enabled(self):
        return True

    def a_command_加最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new_favorites) > 0:
            res_ctx.msg += "本小姐特地抽出時間把「{}」加到你的最愛了，你可要好好記住這份恩情呢！".format("、".join(res_ctx.new_favorites))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n"
            res_ctx.msg += "要好好的輸入正確名稱阿笨蛋！你就是老這樣脫線本小姐才放心不下......".format("、".join(res_ctx.not_found))
        if len(res_ctx.already_favorites) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有，"
            res_ctx.msg += "「{}」早就是你的最愛啦！你這傢伙的記性......真擔心哪天本小姐不在了你該怎麼辦...... ".format("、".join(res_ctx.already_favorites))
        super().a_command_加最愛_done(res_ctx)

    def a_command_刪最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "「{}」已經幫你刪掉囉，本小姐果然很能幹！".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) == 0:
                res_ctx.msg += "要好好的輸入正確名稱呀！你再這樣下去本小姐可是會很擔心".format("、".join(res_ctx.not_found))
            else:
                res_ctx.msg += "\n「{}」好好的輸入正確名稱呀！本小姐也不是不能稍微給點提示啦......".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_favorites) > 0:
            if len(res_ctx.msg) == 0:
                res_ctx.msg += "「{}」根本就不在你的最愛清單，你這傢伙真的是不長記性呢（嘆）".format("、".join(res_ctx.not_favorites))
            else:
                res_ctx.msg += "\n還有，「{}」根本就不在你的最愛清單，你這傢伙還真是不長記性......".format("、".join(res_ctx.not_favorites))
        super().a_command_刪最愛_done(res_ctx)

    def a_command_加待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new) > 0:
            res_ctx.msg += "「{}」幫你加到待玩了囉，你就對本小姐心懷感激吧！".format("、".join(res_ctx.new))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n"
            res_ctx.msg += "找不到「{}」呢，是不是輸入錯誤呢？".format("、".join(res_ctx.not_found))
        if len(res_ctx.already) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」明明就已經在你的待玩了，長點記性啦笨蛋笨蛋！ ".format("、".join(res_ctx.already))
        super().a_command_加待玩_done(res_ctx)

    def a_command_刪待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "「{}」已經幫你刪了，本小姐辦事你就放一百二十個心吧！".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_in_to_play) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」不在你的待玩好嗎？別也跟著考驗本小姐的記性啦！".format("、".join(res_ctx.not_in_to_play))
        super().a_command_刪待玩_done(res_ctx)
        
    def a_command_正義的大小_done(self, res_ctx):
        size, count = res_ctx.size, res_ctx.count
        if count < 3:
            res_ctx.msg = size
        elif count < 8:
            res_ctx.msg = "{0}就是{0}，再輸入幾次都是一樣的啦".format(size)
        elif count < 12:
            res_ctx.msg = "男生們都這麼在意胸部大小嗎？真是笨蛋".format(size)
        elif count < 16:
            res_ctx.msg = "你不煩本小姐都煩了，不要洗頻！".format(size)
        elif count < 20:
            res_ctx.msg = "你就這麼喜歡{0}嗎......".format(size)
        elif count < 21:
            res_ctx.msg = " 你已經打了超過20次這個沒意義的指令".format(size)
        elif count < 22:
            res_ctx.msg = "你不覺得把時間花在別的事情上比較好嗎？".format(size)
        elif count < 23:
            res_ctx.msg = "說起來為本小姐注入靈魂的橘まお大人超棒的，比起{0}".format(size)
        elif count < 24:
            res_ctx.msg = "橘まお大人不知道會不會是{0}呢？".format(size)
        elif count < 25:
            res_ctx.msg = "你不覺得胸部不分貴賤嗎？何必在乎是不是{0}".format(size)
        elif count < 26    :
            res_ctx.msg = "有時間在這邊玩弄本小姐不如去跑galgame".format(size)
        elif count < 27:
            res_ctx.msg = "你是不是沒什麼朋友呀？".format(size)
        elif count < 28:
            res_ctx.msg = "就這麼想跟本小姐聊天嗎？".format(size)
        elif count < 29:
            res_ctx.msg = " 一個一個整天都是{0}{0}{0}，普通的大小難道就沒人權嗎！".format(size)
        elif count < 30:
            res_ctx.msg = "戀愛是什麼呢......".format(size)
        elif count < 31:
            res_ctx.msg = "欸......你有談過戀愛嗎？".format(size)
        elif count < 32:
            res_ctx.msg = "白.....白癡嗎！本小姐這麼問並不代表想跟你談什麼戀愛!".format(size)
        elif count < 33:
            res_ctx.msg = "像......像你這樣的人，本小姐才不會喜歡上你呢......".format(size)
        else:
            res_ctx.msg = "夠了本小姐要去休息了！找你的{0}妹子去吧，哼！".format(size)
        super().a_command_正義的大小_done(res_ctx)

    def a_query_start(self, req_ctx):
        super().a_query_start(req_ctx)

    def a_query_loading(self, res_ctx):
        res_ctx.msg = ("搜尋「{}」中.....等一下啦不要這麼急！").format(res_ctx.req_ctx.keyword)
        super().a_query_loading(res_ctx)

    def a_query_not_found(self, res_ctx):
        req_ctx = res_ctx.req_ctx
        if req_ctx.query_content_type == QueryContentType.Video:
            res_ctx.error_msg = "找不到「{}」的{}，再想想正確名稱啦！".format(req_ctx.galgame.name,
                                                    VideoType.to_string(req_ctx.video_type))
        else:
            res_ctx.error_msg = "本小姐已經盡力了但還是找不到......齁！別這樣消沉啦，本小姐再努力一下就是了......"
        super().a_query_not_found(res_ctx)

    def a_command_not_found(self, res_ctx):
        res_ctx.error_msg = "別亂輸入指令好嗎！這樣本小姐非常困擾"
        super().a_command_not_found(res_ctx)
        
    def a_command_error(self, res_ctx):
        error_code, note = res_ctx.cmd_error, res_ctx.cmd_error_note
        if error_code == CommandErrorCode.NoPrivilege:
            res_ctx.error_msg = "你需要更喜歡橘まお大人才能使用這指令"
        elif error_code == CommandErrorCode.MissingClosingGt:
            res_ctx.error_msg = "忘了在「{}」後面放一個>符號了啦笨蛋".format(note)
        elif error_code == CommandErrorCode.LtInLtTokens:
            res_ctx.error_msg = "笨蛋欸不可以在<>裡面放一個<符號:「{}」".format(note)
        elif error_code == CommandErrorCode.MissingArguments:
            res_ctx.error_msg = "參數不夠，好好輸入啦笨蛋笨蛋"
        elif error_code == CommandErrorCode.TooManyArguments:
            res_ctx.error_msg = "參數太多了，再好好想一想好嗎"
        elif error_code == CommandErrorCode.NoMatchingSig:
            res_ctx.error_msg = "參數數量不對，加油好嗎"
        elif error_code == CommandErrorCode.NotAllowedInPM:
            res_ctx.error_msg = "這指令不可在密頻使用，下流！"
        elif error_code == CommandErrorCode.TokenDUserInvalid:
            res_ctx.error_msg = "「{}」並非使用者，要用Discord的@語法啦！你真的沒有本小姐在身邊就不行欸......".format(note)
        elif error_code == CommandErrorCode.TokenDChannelInvalid:
            res_ctx.error_msg = "「{}」並非頻道，要用Discord的#語法懂嗎？".format(note)
        elif error_code == CommandErrorCode.TokenDEmojiInvalid:
            res_ctx.error_msg = "找不到表情符號「{}」，別蠢了".format(note)
        elif error_code == CommandErrorCode.TokenObjNotFound:
            objs, note = note
            translate = {Creator: "創作家", Galgame: "遊戲", Brand: "公司"}
            objs_str = "、".join(translate[x] for x in objs if x in translate)
            if len(objs_str) > 0:
                res_ctx.error_msg = "「{}」並不是{}".format(note, objs_str)
            else:
                res_ctx.error_msg = "無效參數「{}」，好好輸入好嗎你這大笨蛋".format(note)
        elif error_code in (CommandErrorCode.Grammar, CommandErrorCode.Internal):
            res_ctx.error_msg = "系統出錯了，都是你這笨蛋的錯"
        else:
            res_ctx.error_msg = "未知錯誤，你的腦袋能不能有點長進阿"
        super().a_command_error(res_ctx)

    def a_query_done(self, res_ctx):
        res_ctx.msg = ("才......才不是特地為了你而找的呢，哼！") 
        super().a_query_done(res_ctx)
