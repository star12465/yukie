import asyncio
from . import PersonalityThanatos
from thanatos.message_handlers import QueryContentType
from thanatos.message_handlers.commands import CommandErrorCode
from thanatos.utils import *
from thanatos.models import *
from thanatos.contexts import *

class PersonalityKanojo(PersonalityThanatos):
    @property
    def name(self):
        return "彼女 ❤"

    @property
    def enabled(self):
        return True

    @property
    def author(self):
        return "<@{}>".format(340089395145342989)
    
    @property
    def avatar_url(self):
        return "https://cdn.discordapp.com/attachments/351624216535105536/356138140634710017/Bj24JX3CYAANQP9.jpg"
    
    def a_command_start(self, req_ctx):
        super().a_command_start(req_ctx)

    def a_command_not_found(self, res_ctx):
        res_ctx.error_msg = "怎麼又有新玩法啦，在你身上試驗看看吧 ❤"
        #if res_ctx.req_ctx.user.id == 340089395145342989:
        #    res_ctx.error_msg = "ごめんなさい、お兄ちゃん、その指示わたし分かりません..."
        with Metadata(res_ctx.req_ctx.db, res_ctx.req_ctx.user, "personality_nonoka") as metadata:
            #後面那個0是預設值
            #你可把metadata想成是一個dictionary, 而這裡count就是key
            count = metadata.get("count", 0)
            if count > 10:
                res_ctx.error_msg = "已經創造了{0}種玩法了呢，今晚可以好好享受囉，**親**❤**愛**❤**的**❤".format(count)
            count += 1
            #這裡把count寫回dictionary(存回資料庫)
            metadata["count"] = count
        
        super().a_command_not_found(res_ctx)
        
    def a_command_error(self, res_ctx):
        error_code, note = res_ctx.cmd_error, res_ctx.cmd_error_note
        if error_code == CommandErrorCode.NoPrivilege:
            res_ctx.error_msg = "您需要再更廚安玖深音才能用這指令"
        super().a_command_error(res_ctx)

    def a_command_加最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new_favorites) > 0:
            res_ctx.msg += "已經把「{}」加到待消滅的的名單囉，親❤愛❤的❤".format("、".join(res_ctx.new_favorites))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            else:
                res_ctx.msg = "啊啦，"
            res_ctx.msg += "找不到「{}」呢，到底是`哪個狐狸精呢......呵呵......呵呵......`".format("、".join(res_ctx.not_found))
        if len(res_ctx.already_favorites) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg = "\n還有"
            res_ctx.msg += "「{}」已經在名單裡面了喔。別擔心很快就會處理掉了喔 ❤".format("、".join(res_ctx.already_favorites))
        super().a_command_加最愛_done(res_ctx)

    def a_command_刪最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "好~，已經將「{}」的存在抹滅囉，看著我以外的人是不行的喔，**親**❤**愛**❤**的**❤".format("、".join(res_ctx.removed))    
        if len(res_ctx.not_found) > 0:   
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            else:
                res_ctx.msg = "啊啦，"
            res_ctx.msg += "找不到「{}」呢，到底是`哪個狐狸精呢......呵呵......呵呵......`".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_favorites) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg = "\n還有"
            res_ctx.msg += "「{}」不在處理名單中呢，作為代替把你處理掉吧(舔唇)❤".format("、".join(res_ctx.not_favorites))
        super().a_command_刪最愛_done(res_ctx)

    def a_command_加待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new) > 0:
            res_ctx.msg += "已經把「{}」加至待玩的play囉❤。".format("、".join(res_ctx.new))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "找不到「{}」呢，不好好輸入就沒獎❤勵❤囉".format("、".join(res_ctx.not_found))
        if len(res_ctx.already) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」已經在待玩的play了，這麼迫不及待是不行的喔❤".format("、".join(res_ctx.already))
        super().a_command_加待玩_done(res_ctx)

    def a_command_刪待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "已經把「{}」從待玩play刪除了，不過你越不喜歡的就越想做呢❤".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "找不到「{}」呢，不好好輸入就沒獎❤勵❤囉".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_in_to_play) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」本來就不是待玩的play呢，我就幫你加進去囉❤".format("、".join(res_ctx.not_in_to_play))
        super().a_command_刪待玩_done(res_ctx)
        
    def a_command_正義的大小_done(self, res_ctx):
        size, count = res_ctx.size, res_ctx.count
        if count < 3:
            res_ctx.msg = size
        elif count < 8:
            res_ctx.msg = "{0}就是{0}，都在看這東西呢......哼哼".format(size)
        elif count < 14:
            res_ctx.msg = "在想哪個{0}的女人嗎......這樣不行喔".format(size)
        elif count < 20:
            res_ctx.msg = "還想著{0}，看來需要疼❤愛❤一下".format(size)
        else:
            res_ctx.msg = "想著**我**以外的東西是不行的喔，**親**❤**愛**❤**的**❤"
        super().a_command_正義的大小_done(res_ctx)

    def a_query_start(self, req_ctx):
        super().a_query_start(req_ctx)

    def a_query_loading(self, res_ctx):
        res_ctx.msg = ("尋找「{}」中，等待的時候眼睛亂飄的話晚點懲罰你❤").format(res_ctx.req_ctx.keyword)
        super().a_query_loading(res_ctx)

    def a_query_not_found(self, res_ctx):
        if res_ctx.req_ctx.query_content_type == QueryContentType.Video:
            res_ctx.error_msg = "找不到「{}」的{}呢。又想找哪個狐狸精的片呢.......呵呵呵.......".format(res_ctx.req_ctx.galgame.name,
                                                    VideoType.to_string(res_ctx.req_ctx.video_type))
        else:
            res_ctx.error_msg = ("啊啦，找不到呢，沒有那些蠅蟲你還有**我**，對吧❤")
        
        super().a_query_not_found(res_ctx)
