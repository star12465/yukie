import asyncio
from . import PersonalityThanatos
from thanatos.message_handlers import QueryContentType
from thanatos.message_handlers.commands import CommandErrorCode
from thanatos.models import *
from thanatos.contexts import *

class PersonalityNonoka(PersonalityThanatos):
    @property
    def name(self):
        return "Nonoka"

    @property
    def avatar_url(self):
        return "https://cdn.discordapp.com/attachments/351624216535105536/359200570633879563/13587967-big10.jpg"
    
    @property
    def enabled(self):
        return True

    @property
    def author(self):
        return "<@{}>".format(340089395145342989)
    
    def a_command_start(self, req_ctx):
        super().a_command_start(req_ctx)

    def a_command_not_found(self, res_ctx):
        res_ctx.error_msg = "お兄ちゃん又打錯指令了嗎......連著簡單的指令都記不得，妹妹瞬間想了好多養お兄ちゃん一輩子的方法......"
        if res_ctx.req_ctx.user.id == 340089395145342989:
            res_ctx.error_msg = "また間違ったの？今、私が一生お兄ちゃんを食べさせていく方法を色々考えちゃったよ"
        super().a_command_not_found(res_ctx)
        
    def a_command_error(self, res_ctx):
        error_code, note = res_ctx.cmd_error, res_ctx.cmd_error_note
        if error_code == CommandErrorCode.NoPrivilege:
            res_ctx.error_msg = "您需要再更廚安玖深音才能用這指令"
        super().a_command_error(res_ctx)

    def a_command_加最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new_favorites) > 0:
            if res_ctx.req_ctx.user.id != 340089395145342989:
                res_ctx.msg += "诶...お兄ちゃん喜歡這樣的啊......已經把「{}」加到お兄ちゃん的最愛囉 (愛心)".format("、".join(res_ctx.new_favorites))
            else:
                res_ctx.msg += "へぇぇ......お兄ちゃんこんなの好きなんだ.....は～い、「{}」をお兄ちゃんのリストに入りました～❤".format("、".join(res_ctx.new_favorites))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) == 0:
                if res_ctx.req_ctx.user.id != 340089395145342989:
                    res_ctx.msg += "お兄ちゃん，可以輸入正確名稱嗎？お兄ちゃん的記憶很令人擔心呢"
                else:
                    res_ctx.msg += "お兄ちゃん、ちゃんと入力してる？お兄ちゃんの記憶心配してっるよ"
                    
            else:
                if res_ctx.req_ctx.user.id != 340089395145342989:
                    res_ctx.msg += "\n但「{}」お兄ちゃん有好好輸入正確名稱嗎？お兄ちゃん的記憶很令人擔心呢".format("、".join(res_ctx.not_found))
                else:
                    res_ctx.msg += "\nてもお兄ちゃん、「{}」ちゃんと入力してる？お兄ちゃんの記憶心配してっるよ".format("、".join(res_ctx.not_found))
        if len(res_ctx.already_favorites) > 0:
            if len(res_ctx.msg) == 0:
                if res_ctx.req_ctx.user.id != 340089395145342989:
                    res_ctx.msg += "お兄ちゃん，「{}」已經是你的最愛了。雖然為お兄ちゃん再加一次也不是不行啦......".format("、".join(res_ctx.already_favorites))
                else:
                    res_ctx.msg += "お兄ちゃん、そんなに「{}」が好きなの、妹じゃだめなの".format("、".join(res_ctx.already_favorites))
            else:
                if res_ctx.req_ctx.user.id != 340089395145342989:
                    res_ctx.msg += "\n還有お兄ちゃん，「{}」早就是你的最愛了。雖然為お兄ちゃん再加一次也不是不行啦......".format("、".join(res_ctx.already_favorites))
                else:
                    res_ctx.msg += "\nあとお兄ちゃん、そんなに「{}」が好きなの、妹じゃだめなの".format("、".join(res_ctx.already_favorites))
        super().a_command_加最愛_done(res_ctx)

    def a_command_刪最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            if res_ctx.req_ctx.user.id != 340089395145342989:
                res_ctx.msg += "好~，已經將「{}」從お兄ちゃん的最愛刪除了~(大心)".format("、".join(res_ctx.removed))
            else:
                res_ctx.msg += "は～い～、もう「{}」はいないから、お兄ちゃん❤.......////".format("、".join(res_ctx.removed))
            
        if len(res_ctx.not_found) > 0:   
            if len(res_ctx.msg) == 0:
                if res_ctx.req_ctx.user.id != 340089395145342989:
                    res_ctx.msg += "お兄ちゃん，可以輸入正確名稱嗎？お兄ちゃん的記憶很令人擔心呢".format("、".join(res_ctx.not_found))
                else:
                    res_ctx.msg += "お兄ちゃん、ちゃんと入力してる？お兄ちゃんの記憶心配してっるよ".format("、".join(res_ctx.not_found))
                    
            else:
                if res_ctx.req_ctx.user.id != 340089395145342989:
                    res_ctx.msg += "\n但「{}」お兄ちゃん有好好輸入正確名稱嗎？お兄ちゃん的記憶很令人擔心呢".format("、".join(res_ctx.not_found))
                else:
                    res_ctx.msg += "\nてもお兄ちゃん、「{}」ちゃんと入力してる？お兄ちゃんの記憶心配してっるよ".format("、".join(res_ctx.not_found))
                    
        if len(res_ctx.not_favorites) > 0:
            if len(res_ctx.msg) == 0:
                if res_ctx.req_ctx.user.id != 340089395145342989:
                    res_ctx.msg += "「{}」不在お兄ちゃん的最愛清單裡喔。お兄ちゃん累了嗎？".format("、".join(res_ctx.not_favorites))
                else:
                    res_ctx.msg += "お兄ちゃんのリストに「{}」はいません。お兄ちゃん疲れた？".format("、".join(res_ctx.not_favorites))
            else:
                if res_ctx.req_ctx.user.id != 340089395145342989:
                    res_ctx.msg += "\n還有，「{}」不在お兄ちゃん的最愛清單裡喔。お兄ちゃん累了嗎？".format("、".join(res_ctx.not_favorites))
                else:
                    res_ctx.msg += "\nそれと，お兄ちゃんのリストに「{}」はいません。お兄ちゃん疲れた？".format("、".join(res_ctx.not_favorites))
        super().a_command_刪最愛_done(res_ctx)

    def a_command_加待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new) > 0:
            res_ctx.msg += "「{}」已經加到待玩囉，お兄ちゃん～".format("、".join(res_ctx.new))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」呢......お兄ちゃん的記憶很令人擔心呢".format("、".join(res_ctx.not_found))
        if len(res_ctx.already) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」已經在你的待玩囉，お兄ちゃん～".format("、".join(res_ctx.already))
        super().a_command_加待玩_done(res_ctx)
        
    def a_command_刪待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "已經把「{}」從待玩刪除囉 ❤".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」呢......お兄ちゃん的記憶很令人擔心呢".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_in_to_play) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」本來就不在你的待玩，不能刪掉不存在的東西喔 ❤".format("、".join(res_ctx.not_in_to_play))
        super().a_command_刪待玩_done(res_ctx)
        
    def a_command_正義的大小_done(self, res_ctx):
        size, count = res_ctx.size, res_ctx.count
        if count < 3:
            res_ctx.msg = size
        elif count < 8:
            res_ctx.msg = "{0}就是{0}，原來お兄ちゃん喜歡{0}啊！！".format(size)
        elif count < 14:
            res_ctx.msg = "哎！{0}有什麼不好！難道お兄ちゃん怕我有意見嗎。".format(size)
        elif count < 20:
            res_ctx.msg = "{0}！{0}！{0}！お兄ちゃん這麼喜歡{0}啊...........".format(size)
        else:
            res_ctx.msg = "妹妹的難道不行嗎(泣)"
        super().a_command_正義的大小_done(res_ctx)

    def a_query_start(self, req_ctx):
        super().a_query_start(req_ctx)

    def a_query_loading(self, res_ctx):
        res_ctx.msg = ("搜尋「{}」中請等一下喔，お兄ちゃん。嗯......！？"
                        "哇!!!お兄ちゃん你讓妹妹看了什麼啊 嗚嗚......"
                        "再等一下下就好，可以先把衣服穿上嗎....////").format(res_ctx.req_ctx.keyword)
        super().a_query_loading(res_ctx)

    def a_query_done(self, res_ctx):
        res_ctx.msg = ("幫お兄ちゃん找到囉")
        if res_ctx.req_ctx.user.id == 340089395145342989:
            res_ctx.msg = ("できました～❤  お兄ちゃん大好き～❤")
        
        super().a_query_done(res_ctx)
        
    def a_query_help_done(self, res_ctx):
        res_ctx.msg = ("""既然お兄ちゃん對我發問了，妹妹我只好來解答お兄ちゃん的疑問啦诶嘿嘿❤

第一個是搜尋功能，只要打__**?關鍵字**__就可以使用搜尋功能。關鍵字可以是遊戲名稱、遊戲公司名稱、聲優名稱、劇本家名稱、畫師名稱、歌手。
如果お兄ちゃん不記得完整個名稱的話也會顯示最符合條件的遊戲喔。遊戲名稱的搜尋會搜出劇本、畫師、主角群CV、日本評價、美國評價、平均遊戲時間、遊戲類型
還有其他更詳細的內容，比如配角群CV、遊戲相關歌曲資訊、遊戲標籤，還有遊戲角色名與CV的對照。還有啊......お兄ちゃん你到底要妹妹說什麼東西啊笨蛋(臉紅)
還有只要打上__**?遊戲名稱(空格)影片**__就可以搜尋遊戲相關影片喔，像是お兄ちゃん最喜歡的......什麼都沒有!!!
再來只要打__**?XX年OO月作**__就會找出那時發售的作品，XX是西元年的後兩位數，OO是月份，如果是今年的可以省略年份喔！
還有打上__**??關鍵字**__的話就會列出所有符合條件的搜索結果，只記得幾個字也沒問題囉。

如果輸入__**!加最愛(空格)XXX**__的話，妹妹就會把XXX加到お兄ちゃん的最愛清單囉。XXXXXX可以是遊戲名稱、遊戲公司名稱、聲優名稱、劇本家名稱、畫師名稱。
加最愛單次不限一筆，__**使用逗號(半形)區隔可一次輸入多筆**__，這樣お兄ちゃん就不會因為洗頻被討厭囉。
另外還有刪除的指令，只要輸入__**!刪最愛(空格)XXX**__就可以將那接近お兄ちゃん的狐狸精刪掉了喔 ㄟ嘿❤
還有加待玩的功能，有想玩的遊戲只要__**!加待玩(空格)名稱**__就可以把遊戲加到表中囉，お兄ちゃん就不會忘記囉。刪除的話只要輸入__**!刪待玩(空格)XXX**__就可以刪掉囉。

如果輸入__**!正義的大小**__的話，お兄ちゃん隱藏在心中對胸部大小的渴望就會被大家知道囉，不過第一名當然是**妹妹**對吧❤❤❤""")
        super().a_query_help_done(res_ctx)

    def a_query_not_found(self, res_ctx):
        if res_ctx.req_ctx.query_content_type == QueryContentType.Video:
            res_ctx.error_msg = "お兄ちゃん，找不到「{}」的{}呢。不能看著我就好嗎......////".format(res_ctx.req_ctx.galgame.name,
                                                    VideoType.to_string(res_ctx.req_ctx.video_type))
        else:
            res_ctx.error_msg = ("お兄ちゃん到底是要找什麼髒髒的東西都找不到呢。"
                           "嗚嗚 請不要這樣討厭妹妹，會補償お兄ちゃん的!!"
                           "對了，好久沒跟お兄ちゃん一起睡了，找不到髒髒的東西就讓妹妹來陪你吧～❤")
        
        super().a_query_not_found(res_ctx)
