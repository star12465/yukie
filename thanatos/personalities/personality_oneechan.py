import asyncio
from .import PersonalityThanatos
from thanatos.message_handlers import QueryContentType
from thanatos.message_handlers.commands import CommandErrorCode
from thanatos.models import *
from thanatos.contexts import *

class PersonalityOneechan(PersonalityThanatos):
    @property
    def name(self):
        return "お姉ちゃん"

    @property
    def author(self):
        return "<@{}>".format(323839132910747649)

    @property
    def avatar_url(self):
        return "https://cdn.discordapp.com/attachments/330074213346050048/356470812758376470/unknown.png"

    @property
    def enabled(self):
        return True

    def a_command_加最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new_favorites) > 0:
            res_ctx.msg += "姐姐已經幫你把「{}」加進去了，還有其他要加嗎？".format("、".join(res_ctx.new_favorites))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "姐姐我找不到呢﹐快過來讓姐姐看看是不是打錯了名字？".format("、".join(res_ctx.not_found))
        if len(res_ctx.already_favorites) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有，"
            res_ctx.msg += "「{}」已經加過了囉﹐快來和姐姐分享你那麼喜歡的遊戲？".format("、".join(res_ctx.already_favorites))
        super().a_command_加最愛_done(res_ctx)

    def a_command_刪最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "「{}」姐姐幫你刪掉了，要加回去記得找姐姐哦".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n"
            res_ctx.msg += "「{}」姐姐我找不到呢﹐快過來讓姐姐看看是不是打錯了名字？".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_favorites) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有，"
            res_ctx.msg += "「{}」姐姐記得你沒有加這個呢".format("、".join(res_ctx.not_favorites))
        super().a_command_刪最愛_done(res_ctx)

    def a_command_加待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new) > 0:
            res_ctx.msg += "「{}」加到待玩了啦，忘記了也不怕姐姐會提醒你的".format("、".join(res_ctx.new))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」呢﹐姐姐再幫你找一找？".format("、".join(res_ctx.not_found))
        if len(res_ctx.already) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」已經在你的待玩了﹐不用膽心姐姐快去打吧".format("、".join(res_ctx.already))
        super().a_command_加待玩_done(res_ctx)

    def a_command_刪待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "「{}」刪了也好﹐快來陪姐姐".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_in_to_play) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」不在你的待玩好呢﹐來陪姐姐玩吧".format("、".join(res_ctx.not_in_to_play))
        super().a_command_刪待玩_done(res_ctx)
        
    def a_command_正義的大小_done(self, res_ctx):
        size, count = res_ctx.size, res_ctx.count
        if count < 3:
            res_ctx.msg = size
        elif count < 8:
            res_ctx.msg = "姐姐的就不可以嗎??".format(size)
        elif count < 14:
            res_ctx.msg = "姐姐就是正義的大小".format(size)
        elif count < 20:
            res_ctx.msg = "都說了姐姐就是正義".format(size)
        else:
            res_ctx.msg = "再說姐姐就要憤怒了OWO"
        super().a_command_正義的大小_done(res_ctx)

    def a_query_start(self, req_ctx):
        super().a_query_start(req_ctx)

    def a_query_loading(self, res_ctx):
        res_ctx.msg = ("正在找「{}」.....等一下姐姐哦？").format(res_ctx.req_ctx.keyword)
        super().a_query_loading(res_ctx)

    def a_query_not_found(self, res_ctx):
        req_ctx = res_ctx.req_ctx
        if req_ctx.query_content_type == QueryContentType.Video:
            res_ctx.error_msg = "找不到「{}」的{} 呢，姐姐再努力一下!".format(req_ctx.galgame.name,
                                                    VideoType.to_string(req_ctx.video_type))
        else:
            res_ctx.error_msg = "找不到呢﹐要不姐姐幫你去Google一下?"
        super().a_query_not_found(res_ctx)

    def a_command_not_found(self, res_ctx):
        res_ctx.error_msg = "姐姐不明白呢﹐快來教姐姐"
        super().a_command_not_found(res_ctx)
        
    def a_command_error(self, res_ctx):
        error_code, note = res_ctx.cmd_error, res_ctx.cmd_error_note
        if error_code == CommandErrorCode.NoPrivilege:
            res_ctx.error_msg = "來﹐聽姐姐的話﹐不要用這個指令"
        elif error_code == CommandErrorCode.MissingClosingGt:
            res_ctx.error_msg = "忘了在「{}」後面放一個>符號了啦﹐記得不要犯同樣的錯誤哦".format(note)
        elif error_code == CommandErrorCode.LtInLtTokens:
            res_ctx.error_msg = "姐姐都說了﹐不可以在<>裡面放一個<符號:「{}」".format(note)
        elif error_code == CommandErrorCode.MissingArguments:
            res_ctx.error_msg = "參數不夠，等姐姐教你輸入"
        elif error_code == CommandErrorCode.TooManyArguments:
            res_ctx.error_msg = "參數太多了，姐姐不夠時間做啦"
        elif error_code == CommandErrorCode.NoMatchingSig:
            res_ctx.error_msg = "參數數量不對，讓姐姐看看是那裡錯了"
        elif error_code == CommandErrorCode.NotAllowedInPM:
            res_ctx.error_msg = "這指令不可在密頻使用，只可以告訴姐姐我一個哦"
        elif error_code == CommandErrorCode.TokenDUserInvalid:
            res_ctx.error_msg = "「{}」並非使用者，要用姐姐的語言哦".format(note)
        elif error_code == CommandErrorCode.TokenDChannelInvalid:
            res_ctx.error_msg = "「{}」並非頻道，要用姐姐的語言哦".format(note)
        elif error_code == CommandErrorCode.TokenDEmojiInvalid:
            res_ctx.error_msg = "找不到表情符號「{}」，讓姐姐做這個表情給你看吧".format(note)
        elif error_code == CommandErrorCode.TokenObjNotFound:
            objs, note = note
            translate = {Creator: "創作家", Galgame: "遊戲", Brand: "公司"}
            objs_str = "、".join(translate[x] for x in objs if x in translate)
            if len(objs_str) > 0:
                res_ctx.error_msg = "「{}」並不是{}".format(note, objs_str)
            else:
                res_ctx.error_msg = "無效參數「{}」，不要欺負姐姐啦".format(note)
        elif error_code in (CommandErrorCode.Grammar, CommandErrorCode.Internal):
            res_ctx.error_msg = "系統出錯了，一會兒姐姐會弄好的"
        else:
            res_ctx.error_msg = "何てそれ...姐姐也弄不懂呢"
        super().a_command_error(res_ctx)
