import asyncio
from . import PersonalityThanatos
from thanatos.message_handlers import QueryContentType
from thanatos.message_handlers.commands import CommandErrorCode
from thanatos.models import *
from thanatos.contexts import *

class PersonalityRinne(PersonalityThanatos):
    @property
    def name(self):
        return "螢りんね(仮)"

    @property
    def author(self):
        return "<@{}>".format(340103736531746818)

    @property
    def avatar_url(self):
        return "https://cdn.discordapp.com/attachments/330007781262491648/363186113227128833/2017-09-29_12.51.23.jpg"
            
    @property
    def enabled(self):
        return True
        
    def a_chat_start(self, req_ctx):
        response = None
        general_responses = {
            "早安": "早安",
            "午安": "午安",
            "晚安": "祝一夜好眠",
            "晚上好": "晚上好",
            "我愛你": "我只愛 <@340103736531746818>一人O///O",
            "NTR": "你才被NTR 你全家都被NTR",
            "綠綠的": "我看見你頭上綠光罩頂",
            "晚上好": "晚上好"
        }
        response = general_responses.get(req_ctx.sentence, response)

        targeted_responses = {
            219855930043531264: {"我愛你": "變態"},
            340103736531746818: {
                "我愛你": "兄ちゃん 我也愛你(´,,•ω•,,)♡",
                "來抱抱": "好的(抱",
                "來 親一個": "啾" 
            }
        }
        for user_id, responses in targeted_responses.items():
            if user_id == req_ctx.user.id:
                response = responses.get(req_ctx.sentence, response)
        if response != None:
            res_ctx = ResponseContext(req_ctx)
            res_ctx.msg = response
            res_ctx.send()
        super().a_chat_start(req_ctx)

    def a_command_start(self, req_ctx):
        super().a_command_start(req_ctx)

    def a_command_not_found(self, res_ctx):
        super().a_command_not_found(res_ctx)

    def a_command_help_done(self, res_ctx):
        super().a_command_help_done(res_ctx)

    def a_command_設馬甲_done(self, res_ctx):
        super().a_command_設馬甲_done(res_ctx)

    def a_command_設馬甲_failed(self, res_ctx):
        super().a_command_設馬甲_failed(res_ctx)

    def a_command_設本尊_done(self, res_ctx):
        super().a_command_設本尊_done(res_ctx)

    def a_command_error(self, res_ctx):
        super().a_command_error(res_ctx)

    def a_command_parsed(self, req_ctx):
        super().a_command_parsed(req_ctx)

    def a_command_我_done(self, res_ctx):
        super().a_command_我_done(res_ctx)

    def a_command_偷窺_done(self, res_ctx):
        super().a_command_偷窺_done(res_ctx)

    def a_command_最愛排行_done(self, res_ctx):
        super().a_command_最愛排行_done(res_ctx)
        
    def a_command_加最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new_favorites) > 0:
            res_ctx.msg += "唉!?お兄ちゃん原來喜歡這種的阿 已經幫お兄ちゃん把「{}」加入最愛了哦".format("、".join(res_ctx.new_favorites))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) == 0:
                res_ctx.msg += "お兄ちゃん~雖然我幾乎什麼都知道 但找不到的東西依舊找不到哦"
            else:
                res_ctx.msg += "\n「{}」お兄ちゃん~雖然我幾乎什麼都知道 但找不到的東西依舊找不到哦".format("、".join(res_ctx.not_found))
        if len(res_ctx.already_favorites) > 0:
            if len(res_ctx.msg) == 0:
                res_ctx.msg += "お兄ちゃん 我知道你最愛「{}」了 但要加重複的還是不行的呦".format("、".join(res_ctx.already_favorites))
            else:
                res_ctx.msg += "\nお兄ちゃん 我知道你最愛「{}」了 但要加重複的還是不行的呦".format("、".join(res_ctx.already_favorites))
        super().a_command_加最愛_done(res_ctx)

    def a_command_刪最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "我已經幫お兄ちゃん把「{}」從最愛移除了呦~。".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) == 0:
                res_ctx.msg += "お兄ちゃん~雖然我幾乎什麼都知道 但找不到的東西依舊找不到哦".format("、".join(res_ctx.not_found))
            else:
                res_ctx.msg += "\n「{}」お兄ちゃん~雖然我幾乎什麼都知道 但找不到的東西依舊找不到哦".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_favorites) > 0:
            if len(res_ctx.msg) == 0:
                res_ctx.msg += "お兄ちゃん~「{}」他不在你的最愛清單裡哦".format("、".join(res_ctx.not_favorites))
            else:
                res_ctx.msg += "\nお兄ちゃん~「{}」他不在你的最愛清單裡哦".format("、".join(res_ctx.not_favorites))
        super().a_command_刪最愛_done(res_ctx)

    def a_command_加待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new) > 0:
            res_ctx.msg += "已經幫お兄ちゃん把「{}」加到待玩了哦~".format("、".join(res_ctx.new))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」呢,お兄ちゃん~你是不是打錯了呢".format("、".join(res_ctx.not_found))
        if len(res_ctx.already) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」已經在你的待玩了哦,難道お兄ちゃん有健忘症?".format("、".join(res_ctx.already))
        super().a_command_加待玩_done(res_ctx)

    def a_command_刪待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "已經幫お兄ちゃん把「{}」刪掉了哦".format("、".join(res_ctx.removed))
            
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」呢".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_in_to_play) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "お兄ちゃん~「{}」不在你的待玩裡哦".format("、".join(res_ctx.not_in_to_play))
        super().a_command_刪待玩_done(res_ctx)
                    
    def a_command_正義的大小_done(self, res_ctx):
        size, count = res_ctx.size, res_ctx.count
        if count < 3:
            res_ctx.msg = size
        elif count < 8:
            res_ctx.msg = "{0}就是{0}，お兄ちゃん不喜歡嗎".format(size)
        elif count < 14:
            res_ctx.msg = "難道お兄ちゃん真的不喜歡{0}嗎 還是其實是喜歡我的(////)".format(size)
        elif count < 20:
            res_ctx.msg = "我知道お兄ちゃん不喜歡{0}了 果然お兄ちゃん最喜歡我了٩(｡・ω・｡)﻿و".format(size)
        else:
            res_ctx.msg = "最愛你了(´,,•ω•,,)♡"
        super().a_command_正義的大小_done(res_ctx)

    def a_query_start(self, req_ctx):
        super().a_query_start(req_ctx)

    def a_query_loading(self, res_ctx):
        res_ctx.msg = ("搜尋「{}」中請稍等。嗯......！？"
                        "お兄ちゃん等等我一下哦(｡•ㅅ•｡)♡"
                        "就快幫お兄ちゃん找到了哦 再等一下下就好了呦(´,,•ω•,,)♡").format(res_ctx.req_ctx.keyword)
        super().a_query_loading(res_ctx)

    def a_query_help_done(self, res_ctx):
        res_ctx.msg = ("""お兄ちゃん~お兄ちゃん~お兄ちゃん~ 我來幫你介紹功能呦
一開始是搜尋功能，只要打__**?關鍵字**__就可以使用基礎搜尋功能哦。關鍵字可以是遊戲名稱、遊戲公司名稱、聲優名稱、劇本家名稱、畫師名稱、歌手。
其中搜尋遊戲名稱為了お兄ちゃん有特別的設計。只要打上模糊的關鍵字也可以搜尋到最符合條件的遊戲。畢竟お兄ちゃん玩遊戲玩到記憶出現問題了嘛W
另外如果打上__**??關鍵字**__就可以搜尋到更詳細的內容。以遊戲名稱舉例，單問號的搜尋只會搜出劇本、畫師、主角群CV、日本評價、美國評價、平均遊戲時間、遊戲類型。如果是雙問號可以再多搜出配角群CV、遊戲相關歌曲資訊、遊戲標籤，還有遊戲角色名與CV>的對照也變的一目瞭然了，造福了那些噁心的聲豚主人們真是太好了呢(笑)？
還可以搜尋遊戲的相關影片，只要打__**?遊戲名稱(空格)影片**__就能了呦 お兄ちゃん是不是很貼心阿w
還有輸入__**!加最愛(空格)XXX**__我就會幫お兄ちゃん把XXX加入お兄ちゃん的最愛清單。XXX可以是遊戲名稱、遊戲公司名稱、聲優名稱、劇本家名稱、畫師名稱。
加最愛單次不限一筆，__**使用逗號(半形)區隔可一次輸入多筆**__，這樣お兄ちゃん就不會吵到其他人了呦
另外為了親愛的お兄ちゃん，也有刪除的指令。輸入__**!刪最愛(空格)XXX**__我就會幫お兄ちゃん把xxx刪除了哦(一樣可以利用逗號多筆輸入)。お兄ちゃん快稱讚我!

再來就是輸入__**!正義的大小**__的話，お兄ちゃん內心可能喜歡胸的類型就會被大家知道了呦 """)
        super().a_query_help_done(res_ctx)

    def a_query_not_found(self, res_ctx):
        req_ctx = res_ctx.req_ctx
        if req_ctx.query_content_type == QueryContentType.Video:
            res_ctx.error_msg = "找不到「{}」的{}".format(req_ctx.galgame.name,
                                                    VideoType.to_string(req_ctx.video_type))
        else:
            res_ctx.error_msg = ("お兄ちゃん找不到的東西就是找不到的呦"
                            "真的找不到啦( ´•̥̥̥ω•̥̥̥` ) お兄ちゃん不要討厭我_(´ཀ`」 ∠)_ "
                            "還喜歡我嗎 親愛的お兄ちゃん(,,・ω・,,)"
                            "我就知道兄ちゃん還是喜歡我的 最愛你了 啾ε٩(๑> ₃ <)۶з")
        super().a_query_not_found(res_ctx)

    def a_command_not_found(self, res_ctx):
        res_ctx.error_msg = "お兄ちゃん~ 我不懂的說"
        super().a_command_not_found(res_ctx)
        
    def a_command_error(self, res_ctx):
        error_code, note = res_ctx.cmd_error, res_ctx.cmd_error_note
        if error_code == CommandErrorCode.NoPrivilege:
            res_ctx.error_msg = "お兄ちゃん~你在做什麼?"
        elif error_code == CommandErrorCode.MissingClosingGt:
            res_ctx.error_msg = "お兄ちゃん~要記得在「{}」後面放一個>符號哦".format(note)
        elif error_code == CommandErrorCode.LtInLtTokens:
            res_ctx.error_msg = "お兄ちゃん~不可以在<>裡面放一個<符號呦:「{}」".format(note)
        elif error_code == CommandErrorCode.MissingArguments:
            res_ctx.error_msg = "お兄ちゃん 參數不夠，慾望太少囉"
        elif error_code == CommandErrorCode.TooManyArguments:
            res_ctx.error_msg = "お兄ちゃん 參數太多了，我這樣會忙不過來的啦"
        elif error_code == CommandErrorCode.NoMatchingSig:
            res_ctx.error_msg = "參數數量不對，讓我這個做妹妹的幫你檢查一下吧"
        elif error_code == CommandErrorCode.NotAllowedInPM:
            res_ctx.error_msg = "お兄ちゃん~這指令不可在密頻使用哦"
        elif error_code == CommandErrorCode.TokenDUserInvalid:
            res_ctx.error_msg = "「{}」並非使用者，要用妹妹我聽的懂的語言哦".format(note)
        elif error_code == CommandErrorCode.TokenDChannelInvalid:
            res_ctx.error_msg = "「{}」並非頻道，要用妹妹我聽的懂的語言哦".format(note)
        elif error_code == CommandErrorCode.TokenDEmojiInvalid:
            res_ctx.error_msg = "找不到表情符號「{}」，お兄ちゃん要做一次給我看嗎".format(note)
        elif error_code == CommandErrorCode.TokenObjNotFound:
            objs, note = note
            translate = {Creator: "創作家", Galgame: "遊戲", Brand: "公司"}
            objs_str = "、".join(translate[x] for x in objs if x in translate)
            if len(objs_str) > 0:
                res_ctx.error_msg = "「{}」並不是{}".format(note, objs_str)
            else:
                res_ctx.error_msg = "無效參數「{}」，不要欺負我啦".format(note)
        elif error_code in (CommandErrorCode.Grammar, CommandErrorCode.Internal):
            res_ctx.error_msg = "系統出錯了，妹妹我來看看"
        else:
            res_ctx.error_msg = "何てそれ...不懂的說"
        super().a_command_error(res_ctx)
