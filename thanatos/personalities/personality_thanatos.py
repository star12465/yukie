import asyncio
from .personality import Personality
from thanatos.models import *
from thanatos.message_handlers.commands import CommandErrorCode

class PersonalityThanatos(Personality):
    @property
    def name(self):
        return "Thanatos"

    @property
    def avatar_url(self):
        return "https://cdn.discordapp.com/attachments/334564147847233548/355895524412358666/unknown.png"

    @property
    def author(self):
        return ""

    @property
    def enabled(self):
        return True

    def a_chat_start(self, req_ctx):
        pass

    def a_command_start(self, req_ctx):
        pass

    def a_command_not_found(self, res_ctx):
        if not hasattr(res_ctx, "error_msg"):
            res_ctx.error_msg = "無此指令"
        res_ctx.send()

    def a_command_help_done(self, res_ctx):
        res_ctx.send()

    def a_command_設人格_not_found(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = "找不到人格「{}」".format(res_ctx.req_ctx.personality)
        res_ctx.send()

    def a_command_設人格_done(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = "已成功將人格更換成「{}」".format(res_ctx.req_ctx.personality)
        res_ctx.send()

    def a_command_設馬甲_done(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = "馬甲設定成功"
            if len(res_ctx.not_found) > 0:
                res_ctx.msg += "\n但找不到「{}」".format("、".join(res_ctx.not_found))
            res_ctx.send()

    def a_command_設馬甲_failed(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            if len(res_ctx.found) == 1:
                res_ctx.msg = "只有一位創作家設不了馬甲"
            if len(res_ctx.not_found) > 0:
                res_ctx.msg += "\n找不到「{}」".format("、".join(res_ctx.not_found))
            res_ctx.send()

    def a_command_設本尊_done(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = "本尊設定成功"
            res_ctx.send()

    def a_command_error(self, res_ctx):
        if not hasattr(res_ctx, "error_msg"):
            error_code, note = res_ctx.cmd_error, res_ctx.cmd_error_note
            if error_code == CommandErrorCode.NoPrivilege:
                res_ctx.error_msg = "你需要再更廚雪都一點才能用這指令"
            elif error_code == CommandErrorCode.MissingClosingGt:
                res_ctx.error_msg = "你是不是忘了在「{}」後面放一個>符號?".format(note)
            elif error_code == CommandErrorCode.LtInLtTokens:
                res_ctx.error_msg = "不可以在<>裡面放一個<符號: 「{}」".format(note)
            elif error_code == CommandErrorCode.MissingArguments:
                res_ctx.error_msg = "參數不夠"
            elif error_code == CommandErrorCode.TooManyArguments:
                res_ctx.error_msg = "參數太多了"
            elif error_code == CommandErrorCode.NoMatchingSig:
                res_ctx.error_msg = "參數數量不對"
            elif error_code == CommandErrorCode.NotAllowedInPM:
                res_ctx.error_msg = "這指令不可在密頻使用"
            elif error_code == CommandErrorCode.TokenDUserInvalid:
                res_ctx.error_msg = "「{}」並非使用者，請用Discord的@語法".format(note)
            elif error_code == CommandErrorCode.TokenDChannelInvalid:
                res_ctx.error_msg = "「{}」並非頻道，請用Discord的#語法".format(note)
            elif error_code == CommandErrorCode.TokenDEmojiInvalid:
                res_ctx.error_msg = "找不到表情符號「{}」".format(note)
            elif error_code == CommandErrorCode.TokenObjNotFound:
                objs, note = note
                translate = {Creator: "創作家", Galgame: "遊戲", Brand: "公司"}
                objs_str = "、".join(translate[x] for x in objs if x in translate)
                if len(objs_str) > 0:
                    res_ctx.error_msg = "「{}」並不是{}".format(note, objs_str)
                else:
                    res_ctx.error_msg = "無效參數「{}」".format(note)
            elif error_code in (CommandErrorCode.Grammar, CommandErrorCode.Internal):
                res_ctx.error_msg = "系統出錯了"
            else:
                res_ctx.error_msg = "未知錯誤"
        res_ctx.send()

    def a_command_parsed(self, req_ctx):
        pass

    def a_command_say_done(self, res_ctx):
        res_ctx.send()

    def a_command_set_privilege_bad_privilege(self, res_ctx):
        res_ctx.msg = "權限設定失敗"
        res_ctx.send()

    def a_command_set_privilege_done(self, res_ctx):
        res_ctx.msg = "權限設定成功"
        res_ctx.send()

    def a_command_我_done(self, res_ctx):
        res_ctx.send()

    def a_command_偷窺_done(self, res_ctx):
        res_ctx.send()

    def a_command_最愛排行_done(self, res_ctx):
        res_ctx.send()

    def a_command_加最愛_done(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = ""
            if len(res_ctx.new_favorites) > 0:
                res_ctx.msg += "已經「{}」加至最愛囉。".format("、".join(res_ctx.new_favorites))
            if len(res_ctx.not_found) > 0:
                if len(res_ctx.msg) > 0:
                    res_ctx.msg += "\n但"
                res_ctx.msg += "找不到「{}」".format("、".join(res_ctx.not_found))
            if len(res_ctx.already_favorites) > 0:
                if len(res_ctx.msg) > 0:
                    res_ctx.msg += "\n還有"
                res_ctx.msg += "「{}」已經在你的最愛了".format("、".join(res_ctx.already_favorites))
        res_ctx.send()

    def a_command_刪最愛_done(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = ""
            if len(res_ctx.removed) > 0:
                res_ctx.msg += "已經「{}」從最愛刪除了。".format("、".join(res_ctx.removed))
            if len(res_ctx.not_found) > 0:
                if len(res_ctx.msg) > 0:
                    res_ctx.msg += "\n但"
                res_ctx.msg += "找不到「{}」".format("、".join(res_ctx.not_found))
            if len(res_ctx.not_favorites) > 0:
                if len(res_ctx.msg) > 0:
                    res_ctx.msg += "\n還有"
                res_ctx.msg += "「{}」本來就不在你的最愛".format("、".join(res_ctx.not_favorites))
        res_ctx.send()

    def a_command_待玩_done(self, res_ctx):
        res_ctx.send()

    def a_command_加待玩_done(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = ""
            if len(res_ctx.new) > 0:
                res_ctx.msg += "已經「{}」加至待玩囉。".format("、".join(res_ctx.new))
            if len(res_ctx.not_found) > 0:
                if len(res_ctx.msg) > 0:
                    res_ctx.msg += "\n但"
                res_ctx.msg += "找不到「{}」".format("、".join(res_ctx.not_found))
            if len(res_ctx.already) > 0:
                if len(res_ctx.msg) > 0:
                    res_ctx.msg += "\n還有"
                res_ctx.msg += "「{}」已經在你的待玩了".format("、".join(res_ctx.already))
        res_ctx.send()

    def a_command_刪待玩_done(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = ""
            if len(res_ctx.removed) > 0:
                res_ctx.msg += "已經「{}」從待玩刪除了。".format("、".join(res_ctx.removed))
            if len(res_ctx.not_found) > 0:
                if len(res_ctx.msg) > 0:
                    res_ctx.msg += "\n但"
                res_ctx.msg += "找不到「{}」".format("、".join(res_ctx.not_found))
            if len(res_ctx.not_in_to_play) > 0:
                if len(res_ctx.msg) > 0:
                    res_ctx.msg += "\n還有"
                res_ctx.msg += "「{}」本來就不在你的待玩".format("、".join(res_ctx.not_in_to_play))
        res_ctx.send()

    def a_command_正義的大小_done(self, res_ctx):
        res_ctx.send()

    def a_query_start(self, req_ctx):
        pass

    def a_query_help_done(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = """?+遊戲名(可模糊）
?+創作者名or角色名or公司名(須準確）
??+想搜尋的(可模糊任何東西）
?+遊戲名+空格+影片(將找出影片清單代碼，可模糊）
?+遊戲名(建議準確但可模糊)+空格+搜出之影片清單代碼
?+XX年YY月作 從發售日找遊戲"""
        res_ctx.send()

    def a_query_loading(self, res_ctx):
        if not hasattr(res_ctx, "msg"):
            res_ctx.msg = "尋找「{}」中".format(res_ctx.req_ctx.keyword)
        res_ctx.req_ctx.waiting_task = res_ctx.send()

    def a_query_done(self, res_ctx):
        if hasattr(res_ctx.req_ctx, "waiting_ctx"):
            def waiting_done(fut):
                res_ctx.req_ctx.waiting_ctx.delete()
                res_ctx.send()
            res_ctx.req_ctx.waiting_task.add_done_callback(waiting_done)
        else:
            res_ctx.send()

    def a_query_not_found(self, res_ctx):
        if not hasattr(res_ctx, "error_msg"):
            res_ctx.error_msg = "找不到「{}」".format(res_ctx.req_ctx.keyword)

        if hasattr(res_ctx.req_ctx, "waiting_ctx"):
            def waiting_done(fut):
                res_ctx.req_ctx.waiting_ctx.delete()
                res_ctx.send()
            res_ctx.req_ctx.waiting_task.add_done_callback(waiting_done)
        else:
            res_ctx.send()
