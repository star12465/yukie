import asyncio
from . import PersonalityThanatos
from thanatos.message_handlers import QueryContentType
from thanatos.message_handlers.commands import CommandErrorCode
from thanatos.models import *
from thanatos.contexts import *

class PersonalityYukioto(PersonalityThanatos):
    @property
    def name(self):
        return "天星院雪音"

    @property
    def author(self):
        return "<@{}>".format(320446362304905216)

    @property
    def avatar_url(self):
        return "https://cdn.discordapp.com/attachments/351624216535105536/355995222414721024/222.png"
    
    @property
    def enabled(self):
        return True

    def a_command_加最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new_favorites) > 0:
            res_ctx.msg += "為什麼本小姐非得為了個垃圾浪費時間.......已經把「{}」加到你的最愛了，別再來煩我OK？".format("、".join(res_ctx.new_favorites))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "你這沒用的垃圾，可以好好的輸入正確名稱嗎？".format("、".join(res_ctx.not_found))
        if len(res_ctx.already_favorites) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有，"
            res_ctx.msg += "你這不長記性的蠢豬，「{}」早就是你的最愛，要本小姐再加一次你是腦袋裝屎？".format("、".join(res_ctx.already_favorites))
        super().a_command_加最愛_done(res_ctx)

    def a_command_刪最愛_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "「{}」已經幫你刪掉了，快滾開別煩我".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) == 0:
                res_ctx.msg += "好好的輸入正確名稱有這麼難嗎？你這沒用的垃圾".format("、".join(res_ctx.not_found))
            else:
                res_ctx.msg += "\n「{}」好好的輸入正確名稱有這麼難嗎？你這沒用的垃圾".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_favorites) > 0:
            if len(res_ctx.msg) == 0:
                res_ctx.msg += "「{}」根本就不在你的最愛清單，長點記性好嗎低能兒？".format("、".join(res_ctx.not_favorites))
            else:
                res_ctx.msg += "\n還有，「{}」根本就不在你的最愛清單，長點記性好嗎低能兒？".format("、".join(res_ctx.not_favorites))
        super().a_command_刪最愛_done(res_ctx)

    def a_command_加待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.new) > 0:
            res_ctx.msg += "「{}」加到待玩了啦，沒事就快滾".format("、".join(res_ctx.new))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」就是找不到，有意見？".format("、".join(res_ctx.not_found))
        if len(res_ctx.already) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」明明就已經在你的待玩了？？？秀智商下限？".format("、".join(res_ctx.already))
        super().a_command_加待玩_done(res_ctx)

    def a_command_刪待玩_done(self, res_ctx):
        res_ctx.msg = ""
        if len(res_ctx.removed) > 0:
            res_ctx.msg += "「{}」刪了刪了啦，別再來煩本小姐".format("、".join(res_ctx.removed))
        if len(res_ctx.not_found) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n但"
            res_ctx.msg += "找不到「{}」".format("、".join(res_ctx.not_found))
        if len(res_ctx.not_in_to_play) > 0:
            if len(res_ctx.msg) > 0:
                res_ctx.msg += "\n還有"
            res_ctx.msg += "「{}」不在你的待玩好嗎？白癡".format("、".join(res_ctx.not_in_to_play))
        super().a_command_刪待玩_done(res_ctx)
        
    def a_command_正義的大小_done(self, res_ctx):
        size, count = res_ctx.size, res_ctx.count
        if count < 3:
            res_ctx.msg = size
        elif count < 8:
            res_ctx.msg = "{0}就是{0}，閉嘴好嗎？".format(size)
        elif count < 14:
            res_ctx.msg = "可以停止呼吸別浪費地球氧氣嗎？".format(size)
        elif count < 20:
            res_ctx.msg = "別洗頻好嗎垃圾？".format(size)
        else:
            res_ctx.msg = "滾，別出現在本小姐視線範圍內"
        super().a_command_正義的大小_done(res_ctx)

    def a_query_start(self, req_ctx):
        super().a_query_start(req_ctx)

    def a_query_loading(self, res_ctx):
        res_ctx.msg = ("搜尋「{}」中.....等一下會死嗎急性子處男？").format(res_ctx.req_ctx.keyword)
        super().a_query_loading(res_ctx)

    def a_query_not_found(self, res_ctx):
        req_ctx = res_ctx.req_ctx
        if req_ctx.query_content_type == QueryContentType.Video:
            res_ctx.error_msg = "找不到「{}」的{}，可以別再煩本小姐了嗎？？".format(req_ctx.galgame.name,
                                                    VideoType.to_string(req_ctx.video_type))
        else:
            res_ctx.error_msg = "找不到找不到啦！自己去Google別浪費本小姐時間"
        super().a_query_not_found(res_ctx)

    def a_command_not_found(self, res_ctx):
        res_ctx.error_msg = "別亂輸入指令好嗎垃圾？"
        super().a_command_not_found(res_ctx)
        
    def a_command_error(self, res_ctx):
        error_code, note = res_ctx.cmd_error, res_ctx.cmd_error_note
        if error_code == CommandErrorCode.NoPrivilege:
            res_ctx.error_msg = "像你這樣的垃圾不能用這指令"
        elif error_code == CommandErrorCode.MissingClosingGt:
            res_ctx.error_msg = "忘了在「{}」後面放一個>符號了啦白癡".format(note)
        elif error_code == CommandErrorCode.LtInLtTokens:
            res_ctx.error_msg = "低能兒欸不可以在<>裡面放一個<符號:「{}」".format(note)
        elif error_code == CommandErrorCode.MissingArguments:
            res_ctx.error_msg = "參數不夠，好好輸入好嗎低能兒"
        elif error_code == CommandErrorCode.TooManyArguments:
            res_ctx.error_msg = "參數太多了，用點腦袋阿智障"
        elif error_code == CommandErrorCode.NoMatchingSig:
            res_ctx.error_msg = "參數數量不對，你怎麼不去死一死算了"
        elif error_code == CommandErrorCode.NotAllowedInPM:
            res_ctx.error_msg = "這指令不可在密頻使用，骯髒鬼"
        elif error_code == CommandErrorCode.TokenDUserInvalid:
            res_ctx.error_msg = "「{}」並非使用者，要用Discord的@語法懂嗎你這廢物".format(note)
        elif error_code == CommandErrorCode.TokenDChannelInvalid:
            res_ctx.error_msg = "「{}」並非頻道，要用Discord的#語法懂嗎？".format(note)
        elif error_code == CommandErrorCode.TokenDEmojiInvalid:
            res_ctx.error_msg = "找不到表情符號「{}」，別蠢了".format(note)
        elif error_code == CommandErrorCode.TokenObjNotFound:
            objs, note = note
            translate = {Creator: "創作家", Galgame: "遊戲", Brand: "公司"}
            objs_str = "、".join(translate[x] for x in objs if x in translate)
            if len(objs_str) > 0:
                res_ctx.error_msg = "「{}」並不是{}".format(note, objs_str)
            else:
                res_ctx.error_msg = "無效參數「{}」，好好輸入好嗎你這隻沒用的豬".format(note)
        elif error_code in (CommandErrorCode.Grammar, CommandErrorCode.Internal):
            res_ctx.error_msg = "系統出錯了，都是你這垃圾出生在這世界上的錯"
        else:
            res_ctx.error_msg = "未知錯誤，你的低腦能不能有點長進"
        super().a_command_error(res_ctx)

