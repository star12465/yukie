import re
import logging
import requests
import string
import random
import sqlalchemy
import json
from html import unescape as html_decode
from datetime import datetime
from sqlalchemy.sql import sqltypes
from thanatos.models import *
from thanatos.utils import normalize_str
from .provider import Provider

logger = logging.getLogger(__name__)

class ErogamescapeProvider(Provider):
    SQL_URL = "http://erogamescape.dyndns.org/~ap2/ero/toukei_kaiseki/sql_for_erogamer_form.php"

    def update_brands(self, brands, components):
        if brands == []:
            return

        def unique_handler(db_session, ex, brand):
            brands_to_rename = [1021, 3577, 4122, 350, 2621, 402]
            if brand.id in brands_to_rename:
                #this name should not be taken
                #forgive me for not checking
                brand.name += "_"
            else:
                #we rename the other brand
                other_brand = db_session.query(Brand).filter_by(name=brand.name).one()
                if other_brand.id in brands_to_rename:
                    other_brand.name += "_"
                else:
                    raise RuntimeError("new duplicate={}".format(brand.name))
            db_session.merge(brand)

        if brands == None:
            logging.info("Fetching and populating brands data...")
            sql = """SELECT id, brandname AS name, brandname AS normalized_name, url AS official_url, kind from brandlist
                        where id in (SELECT brandname FROM gamelist WHERE model = 'PC' AND (count2 >= 10 OR sellday >= '2010-01-01'))"""
            self.sql_results_to_model(Brand, sql, unique_handler)

            with self.db.session() as db_session:
                for brand in db_session.query(Brand).all():
                    brand.normalized_name = normalize_str(brand.name)

    def update_galgames(self, galgames, components):
        if galgames == []:
            return

        #update core component
        if (components & Galgame.Component.Core) > 0:
            sql = """SELECT id, gamename AS name, gamename AS normalized_name, sellday AS release_date, brandname AS brand_id,
                        median AS votes_ergs_median, stdev AS votes_ergs_stdev, count2 AS votes_ergs_count, comike AS getchu_id,
                        shoukai AS official_url, banner_url, erogame AS is_adult_rated, total_play_time_median AS playtime_median,
                        genre, erogetrailers AS erogetrailers_id FROM gamelist WHERE {}"""
            if galgames != None:
                condition = "id IN ({})".format(",".join(str(galgame.id) for galgame in galgames))
                self.sql_results_to_model(Galgame, sql.format(condition))

                #refresh galgames here in case in case they are
                #transient (non-persistend) objects
                with self.db.session() as db_session:
                    galgames = [db_session.query(Galgame).filter_by(id=galgame.id).one() for galgame in galgames]
            else:
                condition = "model = 'PC' AND (count2 >= 10 OR sellday >= '2010-01-01')"
                self.sql_results_to_model(Galgame, sql.format(condition))

            with self.db.session() as db_session:
                if galgames == None:
                    for galgame in db_session.query(Galgame).all():
                        galgame.normalized_name = normalize_str(galgame.name)
                else:
                    for galgame in galgames:
                        galgame = db_session.merge(galgame, load=False)
                        galgame.normalized_name = normalize_str(galgame.name)
                db_session.commit()
            if galgames:
                for galgame in galgames:
                    galgame.mark_updated(self.db, self.__class__, Galgame.Component.Core)

        #update works
        if (components & Galgame.Component.Core) > 0:
            sql = """SELECT
                        id,
                        game as galgame_id,
                        creater as creator_id,
                        shubetu as type,
                        shubetu_detail as level,
                        shubetu_detail_name as note
                     FROM shokushu
                     WHERE shubetu < 7 AND {}"""
            if galgames == None:
                condition = "game IN (select id FROM gamelist WHERE model = 'PC' AND (count2 >= 10 OR sellday >= '2010-01-01'))"
                self.sql_results_to_model(Work, sql.format(condition))
                self.update_creators(None, Creator.Component.Core)
            else:
                condition = "game IN ({})".format(",".join(str(galgame.id) for galgame in galgames))
                self.sql_results_to_model(Work, sql.format(condition))

                #update creators
                creators = []
                with self.db.session() as db_session:
                    for galgame in galgames:
                        galgame = db_session.merge(galgame, load=False)
                        for work in galgame.staffs:
                            if work.creator != None:
                                creators.append(work.creator)
                            else:
                                creators.append(Creator(id=work.creator_id))
                self.update_creators(creators, Creator.Component.Core)

        #update trait component
        if (components & Galgame.Component.Traits) > 0:
            if galgames != None:
                if len(galgames) > 1:
                    logger.warning("Updating traits for {} > 1 galgames may be slow".format(len(galgames)))

                for galgame in galgames:
                    #update pov
                    #make two requests so we don't exceed the query cost limit
                    sql = """SELECT {0} AS galgame_id, a.pov AS trait_id, rank_a FROM
                                            (SELECT pov, count(*) AS rank_a FROM povgroups WHERE game = {0} AND rank = 'A' GROUP BY pov) a""".format(galgame.id)
                    self.sql_results_to_model(GalgameTraitVote, sql)
                    sql = """SELECT {0} AS galgame_id, b.pov AS trait_id, rank_b, rank_c
                             FROM (SELECT pov, count(*) AS rank_b FROM povgroups WHERE game = {0} AND rank = 'B' GROUP BY pov) b
                                LEFT OUTER JOIN (SELECT pov, count(*) AS rank_c FROM povgroups WHERE game = {0} AND rank = 'C' GROUP BY pov) c ON b.pov = c.pov""".format(galgame.id)
                    self.sql_results_to_model(GalgameTraitVote, sql)
                    for galgame in galgames:
                        galgame.mark_updated(self.db, self.__class__, Galgame.Component.Traits)
            else:
                logger.warning("Cannot update traits for all galgames")

        #update prerelease interest
        if (components & Galgame.Component.Prerelease) > 0:
            if galgames != None:
                sql = """SELECT
                             game AS target_id,
                             SUM(
                                 CASE before_purchase_will
                                     WHEN '様子見' THEN 1
                                     WHEN '多分購入' THEN 3
                                     WHEN '0_必ず購入' THEN 6
                                     ELSE 0
                                 END
                             ) AS "value:int"
                         FROM userreview
                         WHERE game IN ({})
                         GROUP BY game""".format(",".join(str(galgame.id) for galgame in galgames))
                self.sql_results_to_metadata(sql, Galgame, namespace="prerelease", key="interest")
                for galgame in galgames:
                    galgame.mark_updated(self.db, self.__class__, Galgame.Component.Prerelease)
            else:
                logger.warning("Cannot update prerelease for all galgames")

    def update_galgame_traits(self, traits, components):
        logging.info("Fetching and populating galgame traits data...")
        sql = """SELECT
                     id,
                     system_title AS name,
                     system_title AS chinese_name,
                     CASE system_group
                         WHEN '属性' THEN 0
                         WHEN 'ジャンル' THEN 1
                         WHEN 'ネガティブ' THEN 2
                         WHEN 'ワンポイント' THEN 3
                         WHEN 'グラフィック' THEN 4
                         WHEN 'ゲーム性' THEN 5
                         WHEN 'ゲームの内容外' THEN 6
                         WHEN '主人公' THEN 7
                         WHEN 'シナリオ' THEN 8
                         WHEN 'システム' THEN 9
                         WHEN '条件' THEN 10
                         WHEN '傾向' THEN 11
                         WHEN '音' THEN 12
                         WHEN '女の子キャラクター' THEN 13
                         WHEN '名作' THEN 14
                         WHEN 'ここがいい' THEN 15
                         WHEN '時間' THEN 16
                         WHEN 'エロシーン' THEN 17
                         WHEN '背景' THEN 18
                         WHEN 'シチュエーション' THEN 19
                         WHEN 'キャラクター' THEN 20
                         ELSE -1
                    END AS category,
                    netabare AS spoiler
                FROM povlist"""
        self.sql_results_to_model(GalgameTrait, sql)

        #check to see if any new categories are added
        with self.db.session() as db_session:
            if db_session.query(GalgameTrait).filter_by(category=-1).count() > 0:
                logger.error("new categories unaccounted for")
                raise RuntimeError()

    def update_creators(self, creators, components):
        if creators == []:
            return

        def unique_handler(db_session, ex, creator):
            #rename current creator to something random
            name = creator.name
            creator.name = "".join(random.choice(string.ascii_letters) for _ in range(32))
            creator = db_session.merge(creator)
            db_session.flush()

            #get a name for the duplicate creator
            i = 1
            while True:
                second_name = "{}{}".format(name, i)
                if db_session.query(Creator).filter_by(name=second_name).count() == 0:
                    break
                i += 1

            #decide who's the main identity
            other_creator = db_session.query(Creator).filter_by(name=name).one()
            other_creator.set_as_main_identity([creator])
            other_creator.redecide_main_identity()
            if other_creator.main_identity:
                other_creator.name = second_name
                db_session.flush()
                creator.name = name
            else:
                creator.name = second_name
                db_session.flush()
                other_creator.name = name
            db_session.flush()

        if (components & Creator.Component.Core) > 0:
            sql = """SELECT
                        id, name,
                        name AS normalized_name,
                        url AS homepage_url,
                        twitter_username AS twitter_id
                     FROM createrlist
                     WHERE id IN ({})"""

            if creators == None:
                condition = "SELECT DISTINCT creater FROM shokushu WHERE shubetu < 7 AND game IN (SELECT id FROM gamelist WHERE model = 'PC' AND (count2 >= 10 OR sellday >= '2010-01-01'))"
                self.sql_results_to_model(Creator, sql.format(condition), unique_handler)
            else:
                condition = "{}".format(",".join(str(creator.id) for creator in creators))
                self.sql_results_to_model(Creator, sql.format(condition), unique_handler)

                #refresh creators here in case in case they are
                #transient (non-persistend) objects
                with self.db.session() as db_session:
                    creators = [db_session.query(Creator).filter_by(id=creator.id).one() for creator in creators]

            with self.db.session() as db_session:
                if creators == None:
                    for creator in db_session.query(Creator).all():
                        creator.normalized_name = normalize_str(creator.name)
                else:
                    for creator in creators:
                        creator = db_session.merge(creator, load=False)
                        creator.normalized_name = normalize_str(creator.name)
                db_session.commit()
            if creators:
                for creator in creators:
                    creator.mark_updated(self.db, self.__class__, Creator.Component.Core)

    def update_characters(self, characters, components):
        if characters == None:
            logging.info("Fetching and populating characters data...")
            def before_merge_hook(db_session, obj):
                #since we use a different primary key than the one on ergs
                #we have to check to see if the obj exist in db before
                #we perform a merge
                q = db_session.query(Character).filter_by(ergs_id=obj.ergs_id, galgame_id=obj.galgame_id)
                if q.count() == 1:
                    obj.id = q.one().id
                return True
            sql = """SELECT DISTINCT ON (a.game, a.character)
                         c.name AS name,
                         c.name AS normalized_name,
                         c.furigana AS furigana,
                         CASE c.sex
                             WHEN '女性' THEN 1
                             WHEN '男性' THEN 0
                             ELSE 2
                         END AS gender,
                         NULLIF(c.bloodtype, '公式情報なし') AS blood_type,
                         NULLIF(c.birthday, '公式情報なし') AS birthday,
                         order_number AS display_order,
                         a.game AS galgame_id,
                         role AS level,
                         aa.actor AS cv_id,
                         netabare AS spoiler,
                         url,
                         formal_explanation AS description,
                         age,
                         bust AS bust_size,
                         waist AS waist_size,
                         hip AS hip_size,
                         height,
                         weight,
                         cup AS cup_size,
                         'https://ap2.sakura.ne.jp/erogamescape' || COALESCE(NULLIF(aio.image_dir, ''),
                             COALESCE(NULLIF(aigt.image_dir, ''), NULLIF(aigc.image_dir, ''))
                         ) AS image_url,
                         c.id AS ergs_id
                     FROM appearance a
                         LEFT JOIN characterlist c ON a.character = c.id
                         LEFT OUTER JOIN appearance_actor aa ON aa.game = a.game AND aa.character = a.character AND aa.stage = a.stage
                         LEFT OUTER JOIN appearance_image_getchu aigc ON aigc.game = a.game AND aigc.character = a.character AND aigc.stage = a.stage
                         LEFT OUTER JOIN appearance_image_gyutto aigt ON aigt.game = a.game AND aigt.character = a.character AND aigt.stage = a.stage
                         LEFT OUTER JOIN appearance_image_official aio ON aio.game = a.game AND aio.character = a.character AND aio.stage = a.stage
                     WHERE a.game IN (select id FROM gamelist WHERE model = 'PC' AND (count2 >= 10 OR sellday >= '2010-01-01'))"""
            self.sql_results_to_model(Character, sql, before_merge_hook=before_merge_hook)

            #normalize character names
            with self.db.session() as db_session:
                for character in db_session.query(Character).all():
                    character.normalized_name = normalize_str(character.name)

            #create character links
            def before_merge_hook(db_session, obj):
                #parent_id is actually the galgame id of the parent character
                #and not actually the parent_id. So what we have to do is get the
                #real parent_id from character id (ergs_id) and parent galgame id (parent_id)
                q = db_session.query(Character).filter_by(ergs_id=obj.ergs_id, galgame_id=obj.parent_id)
                if q.count() != 1:
                    logger.error("Found {} != 1 rows for ergs_id={}, galgame_id={} (parent)".format(q.count(), obj.ergs_id, obj.parent_id))
                    return False
                obj.parent_id = q.one().id

                #we also have to find the id of the object itself
                q = db_session.query(Character).filter_by(ergs_id=obj.ergs_id, galgame_id=obj.galgame_id)
                if q.count() != 1:
                    logger.error("Found {} != 1 rows for ergs_id={}, galgame_id={} (child)".format(q.count(), obj.ergs_id, obj.galgame_id))
                    return False
                obj.id = q.one().id
                return True
            sql = """SELECT
                         a.game AS galgame_id,
                         a.character AS ergs_id,
                         b.game AS parent_id
                     FROM appearance a
                         INNER JOIN (
                             SELECT DISTINCT ON (character) character, a.game, a.stage
                             FROM appearance a, gamelist g
                             WHERE a.game = g.id
                             ORDER BY character, g.sellday ASC, a.stage ASC
                         ) b ON a.character = b.character AND a.game != b.game
                    WHERE a.game IN (select id FROM gamelist WHERE model = 'PC' AND (count2 >= 10 OR sellday >= '2010-01-01'))
                        AND b.game IN (select id FROM gamelist WHERE model = 'PC' AND (count2 >= 10 OR sellday >= '2010-01-01'))"""
            self.sql_results_to_model(Character, sql, before_merge_hook=before_merge_hook)

    def _get_sql_rows(self, sql):
        try:
            r = requests.post(ErogamescapeProvider.SQL_URL, data={"sql": sql}, timeout=10)
            if r.status_code != 200:
                logger.error("got unexpected status code {} for query {}".format(r.status_code, sql))
                return [], []
        except requests.exceptions.RequestException as ex:
            logger.exception("got exception for query {}".format(sql))
            return [], []

        if "エラー(SQL間違い)" in r.text:
            logger.error("Invalid SQL statement:\n{}".format(sql))
            return [], []

        if "エラー(実行コスト高)" in r.text:
            logger.error("SQL cost too high:\n{}".format(sql))
            return [], []

        #get columns
        m = re.search(r'<tr><th>(.*?)</th></tr>', r.text)
        if not m:
            #found nothing
            return [], []
        columns = m.group(1).split("</th><th>")

        rows = re.findall(r'<tr><td>(.*?)</td></tr>', r.text, re.DOTALL)
        for i in range(len(rows)):
            rows[i] = [html_decode(x) for x in rows[i].split("</td><td>")]
        return columns, rows
            

    def sql_results_to_metadata(self, sql, target_type, **kwargs):
        columns, rows = self._get_sql_rows(sql)
        logger.info("parsing and processing {} rows returned from erogamescape for {} metadata".format(len(rows), target_type.__name__))

        with self.db.session() as db_session:
            processed = 0
            for column_vals in rows:
                metadata = MetadataT()
                target_id, namespace, key = kwargs.get("target_id", None), kwargs.get("namespace", None), kwargs.get("key", None)
                metadata.target_type = target_type.__name__
                metadata.target_id = target_id
                metadata.namespace = namespace
                metadata.key = key

                for i in range(len(columns)):
                    val = column_vals[i]
                    if columns[i].startswith("value:"):
                        #value should contain type information
                        column_name, column_type = columns[i].split(":", 2)
                    else:
                        column_name = columns[i]
                        if columns[i] == "target_id":
                            column_type = "int"
                        else:
                            column_type = "string"

                    if column_type == "int":
                        try:
                            val = int(val)
                        except ValueError:
                            val = 0
                    elif column_type == "boolean":
                        if val.lower() in ("1", "t", "true"):
                            val = True
                        elif val.lower() in ("0", "f", "false"):
                            val = False
                        else:
                            logging.warn("cannot convert val={} to boolean for column={}".format(val, columns[i]))
                            return processed
                    elif column_type == "string":
                        #as-is
                        pass
                    else:
                        logger.error("Invalid column type supplied: {}".format(column_type))

                    if column_name == "value":
                        val = json.dumps(val)
                    setattr(metadata, column_name, val)

                #save metadata
                db_session.merge(metadata)
                processed += 1
            db_session.commit()
        return processed

    def sql_results_to_model(self, model_cls, sql, error_handler=None, before_merge_hook=None):
        columns, rows = self._get_sql_rows(sql)
        logger.info("parsing and processing {} rows returned from erogamescape for {}".format(len(rows), model_cls.__name__))

        with self.db.session() as db_session:
            processed = 0
            for column_vals in rows:
                obj = model_cls()
                for i in range(len(columns)):
                    val = column_vals[i]
                    if type(getattr(model_cls, columns[i]).type) == sqltypes.Date:
                        val = datetime.strptime(val, "%Y-%m-%d").date()
                    elif type(getattr(model_cls, columns[i]).type) == sqltypes.Integer:
                        try:
                            val = int(val)
                        except ValueError:
                            val = 0
                    elif type(getattr(model_cls, columns[i]).type) == sqltypes.Boolean:
                        if val.lower() in ("1", "t", "true"):
                            val = True
                        elif val.lower() in ("0", "f", "false"):
                            val = False
                        else:
                            logging.warn("cannot convert val={} to boolean for column={}".format(val, columns[i]))
                            return processed
                    setattr(obj, columns[i], val)

                if before_merge_hook:
                    if not before_merge_hook(db_session, obj):
                        continue

                if error_handler:
                    try:
                        #this rollsbacks automatically if action failed
                        with db_session.begin_nested():
                            db_session.merge(obj)
                    except sqlalchemy.exc.IntegrityError as ex:
                        error_handler(db_session, ex, obj)
                        db_session.flush()
                else:
                    try:
                        #if we don't have an error handler
                        #don't have to commit every time with begin_nested to increase speed
                        db_session.merge(obj)
                    except sqlalchemy.exc.SQLAlchemyError:
                        db_session.rollback()
                        raise
                processed += 1
            db_session.commit()
        return processed
