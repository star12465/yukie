import re
import logging
import requests
from html import unescape as html_decode
from datetime import date, datetime
from thanatos.models import *
from .provider import Provider 

logger = logging.getLogger(__name__)

class ErogetrailersProvider(Provider):
    SOFT_URL = "http://erogetrailers.com/soft/{}"
    VIDEO_URL = "http://erogetrailers.com/video/{}"

    VIDEO_TYPE_TRANSLATIONS = {
        "OPデモ": VideoType.OP,
        "CMムービー": VideoType.CM,
        "カウントダウン": VideoType.Countdown,
        "エンディング": VideoType.ED,
        "プレムービー": VideoType.PlayVideo,
        "プレイムービー": VideoType.PlayVideo,
        "販促ムービー": VideoType.PV,
        "企画モノ": VideoType.Other,
        "ラジオ": VideoType.Radio
    }

    def get_video_from_videoid(self, video_id, video_name, video_type):
        video = Video(id=video_id, type=video_type, name=video_name)

        video_url = self.VIDEO_URL.format(video_id)
        try:
            r = requests.get(video_url, timeout=5)
            if r.status_code != 200:
                logger.error("video_id={} got http response {}".format(video_id, r.status_code))
                return None
        except requests.exceptions.RequestException as ex:
            logger.exception("received exception while viewing video {}".format(video_id))
            return None
        text = r.text

        #screw xml parsers we'll just regex
        #release date
        m = re.search("(\d{4})/(\d{2})/(\d{2})公開", text)
        if m:
            video.release_date = date(int(m.group(1)), int(m.group(2)), int(m.group(3)))

        #youtube id
        m = re.search('my_key="([^"]+)"', text)
        if m:
            video.youtube_id = m.group(1)

        #name
        m = re.search('http://erogetrailers.com/song/\d+"\s*>(.*)</a>', text)
        if m:
            video.song_name = html_decode(m.group(1))

        #artist
        m = re.search('歌手.*?<a.*?>(.*)</a>', text)
        if m:
            with self.db.session() as db_session:
                video.song_artist = Creator.get_by_name(db_session, html_decode(m.group(1)))

        #add to videos
        return video

    def update_galgame(self, galgame, components):
        if (components & Galgame.Component.Videos) > 0:
            soft_id = galgame.erogetrailers_id
            if not soft_id:
                return False

            soft_url = self.SOFT_URL.format(soft_id)
            try:
                r = requests.get(soft_url, timeout=5)
                if r.status_code != 200:
                    logger.error("got response {} while searching for videos for game {}".format(r.status_code, soft_id))
                    return False
            except requests.exceptions.RequestException as ex:
                logger.error("received exception while searching for videos for game {}".format(soft_id))
                logger.error(ex)
                return False
            text = r.text

            galgame_videos = []

            #we want to first parse the page for the types and the videos under the types
            #i.e. OP videos, ED videos, etc
            #skip the last two because they are irrelevant
            video_groups = re.findall(r'<h3[^>]*>(.*?)</h3>.*?<div[^>]*>(.*?)</div>', text, re.DOTALL)
            for type_name, videos_html in video_groups:
                #check to see if we want the videos that fall under this type
                type_name = html_decode(type_name.strip())
                if type_name not in self.VIDEO_TYPE_TRANSLATIONS:
                    continue
                video_type = self.VIDEO_TYPE_TRANSLATIONS[type_name]

                #get ids
                videos = re.findall(r'href="http://erogetrailers.com/video/(\d+)">(.*?)</a>.*?<img data-original="([^"]+)"', videos_html, re.DOTALL)
                if len(videos) == 0:
                    #nothing here
                    continue

                #proceed to process each video
                for video_id, video_name, preview_img in videos:
                    video_name = html_decode(video_name.strip())

                    #ignore the dead videos
                    if preview_img.endswith("nofile.jpg"):
                        continue

                    video = self.get_video_from_videoid(video_id, video_name, video_type)
                    if video:
                        galgame_videos.append(video)

            #save changes to database
            with self.db.session() as db_session:
                galgame = db_session.merge(galgame, load=False)
                galgame.videos.clear()
                for galgame_video in galgame_videos:
                    galgame_video = db_session.merge(galgame_video)
                    galgame.videos.append(galgame_video)
            galgame.mark_updated(self.db, self.__class__, Galgame.Component.Videos)
