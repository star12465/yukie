# -*- coding: utf-8 -*-
import re
import logging
import requests
import time
from html import unescape as html_decode
from datetime import date, datetime
from thanatos.models.creator import Creator
from .provider import Provider 

logger = logging.getLogger(__name__)

class GameplazaProvider(Provider):
    HOME_URL = "http://gph.sakura.ne.jp/va_memo/system/"
    MAX_RETRIES = 5

    def update_creators(self, creators, components):
        if (components & Creator.Component.Core) > 0:
            if creators == None:
                #this site just fails too much
                text = None
                for i in range(self.MAX_RETRIES):
                    try:
                        r = requests.get(self.HOME_URL, timeout=10)
                        if r.status_code != 200:
                            logger.warning("recieved unexpected status code {}. Retrying...".format(r.status_code))
                            time.sleep(5)
                            continue
                        if len(r.text) < 100:
                            #some weird things happened
                            logger.warning("server responded with corrupted home page. Retrying...")
                            text = None
                            time.sleep(5)
                            continue
                    except requests.exceptions.RequestException as ex:
                        logger.warning("recieved exception while viewing homepage", exc_info=True)
                        time.sleep(5)
                        continue

                    #set encoding
                    r.encoding = "euc_jis_2004"
                    text = r.text
                    break

                if text == None:
                    logger.error("Unable to fetch homepage")
                    return

                logger.info("Processing cv data")
                cvs = re.findall(r'><a href="vadb.cgi\?action=view_ind&value=(\d+)&namecode=(\d+)">([^<]+)</a>', text)

                with self.db.session() as db_session:
                    for id_value, id_namecode, alias_str in cvs:
                        aliases = alias_str.strip().split("＝")
                        creators = list(filter(lambda x: x != None, (Creator.get_by_name(db_session, html_decode(x)) for x in aliases)))
                        if len(creators) > 1:
                            logger.info(creators)

                            #set gameplaza id
                            gameplaza_id = "{},{}".format(id_value, id_namecode)
                            for creator in creators:
                                creator.gameplaza_id = gameplaza_id

                            creators[0].set_as_main_identity(creators[1:])
                            creators[0].redecide_main_identity()
