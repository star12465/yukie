# -*- coding: utf-8 -*-
import re
import logging
import requests
from html import unescape as html_decode
from datetime import date, datetime
from thanatos.models.creator import Creator
from .provider import Provider 

logger = logging.getLogger(__name__)

class MeigiProvider(Provider):
    PAGE_URL = "http://meigi.info/voice/category/female/page/{}"

    def update_creators(self, creators, component):
        if (component & Creator.Component.Core) > 0:
            if creators == None:
                #their stupid website checks the accept-language field and if it contains
                #zh-tw it sends you away
                page = 1
                while True:
                    logger.info("scrapping page {}".format(page))
                    url = MeigiProvider.PAGE_URL.format(page)
                    try:
                        r = requests.get(url, timeout=5)
                    except requests.exceptions.RequestException as ex:
                        logger.exception("encountered exception for page {}".format(page))
                        return

                    if page > 1 and r.status_code == 404:
                        #we're done :)
                        logger.info("finished scrapping all {} available pages".format(page))
                        break
                    elif r.status_code != 200:
                        logger.error("returns status code {} for page={}".format(status, page))
                        return

                    #random images in the middle of nowhere
                    text = re.sub(r'<img[^>]+>', "", r.text)
                    groups = re.findall(r'title"><a href="http://meigi.info/voice/fe\d+/">([^<]+)</a>', text)
                    for group in groups:
                        if "≒" in group:
                            continue

                        aliases = []
                        for cv in re.split("＝", group):
                            cv = re.sub(r'【.*?】', "", cv)
                            cv = re.sub(r'\[.*?\]', "", cv)
                            cv = re.sub(r'\(.*?\)', "", cv)
                            cv = cv.replace("？", "")
                            cv = cv.strip()
                            aliases.append(cv)

                        logger.info("setting aliases: {}".format(aliases))
                        with self.db.session() as db_session:
                            creators = list(filter(lambda x: x != None, (Creator.get_by_name(db_session, html_decode(x)) for x in aliases)))
                            if len(creators) > 0:
                                creators[0].set_as_main_identity(creators[1:])
                                creators[0].redecide_main_identity()
                    page += 1
