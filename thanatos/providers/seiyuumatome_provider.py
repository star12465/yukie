# -*- coding: utf-8 -*-
import re
import logging
import requests
from html import unescape as html_decode
from datetime import date, datetime
from thanatos.models.creator import Creator
from .provider import Provider 

logger = logging.getLogger(__name__)

class SeiyuumatomeProvider(Provider):
    HOME_URL = "https://www35.atwiki.jp/seiyuumatome/"
    CV_URL = "https://www35.atwiki.jp/seiyuumatome/pages/{}.html"

    def update_creators(self, creators, components):
        if (components & Creator.Component.Core) > 0:
            if creators != None:
                for creator in creators:
                    if creator.seiyuumatome_id:
                        self.update_creator_from_id(creator.seiyuumatome_id)
            else:
                try:
                    r = requests.get(SeiyuumatomeProvider.HOME_URL, timeout=10)
                    if r.status_code != 200:
                        log.error("got unexpected status code {} when fetching homepage".format(r.status_code))
                        return
                except requests.exceptions.RequestException as ex:
                    logger.exception("encountered an exception while fetching homepage")
                    return

                m = re.search("あ行(.*)pages/139.html", r.text, re.DOTALL)
                if not m:
                    log.error("can't get cvs list")
                    return
                text = m.group(1)

                ids = re.findall(r'//www35.atwiki.jp/seiyuumatome/pages/(\d+)\.html', text)
                for seiyuumatome_id in ids:
                    seiyuumatome_id = int(seiyuumatome_id)
                    self.update_creator_from_id(seiyuumatome_id)

    def update_creator_from_id(self, seiyuumatome_id):
        try:
            r = requests.get(SeiyuumatomeProvider.CV_URL.format(seiyuumatome_id), timeout=10)
            if r.status_code != 200:
                log.error("got unexpected status code {} for page id={}".format(r.status_code, seiyuumatome_id))
                return
        except requests.exceptions.RequestException as ex:
            logger.error("encountered exception while fetchinig page {}".format(seiyuumatome_id))
            logger.error(ex)
            return

        m = re.search(r'<div id="wikibody" class="box">(.*?)<br />', r.text, re.DOTALL)
        if not m:
            log.error("cannot find cv alias text for id={}".format(seiyuumatome_id))
            return
        text = m.group(1)
        text = re.sub(r'<.*?>', "", text)
        text = text.replace("\n", "").strip()

        aliases = text.split("＝")

        with self.db.session() as db_session:
            creators = list(filter(lambda x: x != None, (Creator.get_by_name(db_session, html_decode(x)) for x in aliases)))
            if len(creators) > 1:
                logger.info(creators)

                #set seiyuumatome id
                for creator in creators:
                    creator.seiyuumatome_id = seiyuumatome_id
                
                creators[0].set_as_main_identity(creators[1:])
                creators[0].redecide_main_identity()
        for creator in creators:
            creator.mark_updated(self.db, self.__class__, Creator.Component.Core)
