import re
import logging
import requests
from datetime import datetime
from html import unescape as html_decode
from urllib.parse import quote
from thanatos.models import *
from .provider import Provider

logger = logging.getLogger(__name__)

class VndbProvider(Provider):
    GALGAME_URL = "https://vndb.org/v{}"
    SEARCH_GALGAME_URL_RELEASEDATE = "https://vndb.org/v/all?q={0}&rfil=lang-ja.date_after-{1}.date_before-{1}.plat-win"
    SEARCH_GALGAME_URL_FUTURE = "https://vndb.org/v/all?q={}&rfil=lang-ja.released-0.plat-win"
    SEARCH_GALGAME_PAGE_TITLE = "Browse visual novels"
    CREATOR_URL = "https://vndb.org/s{}"
    SEARCH_CREATOR_URL = "https://vndb.org/s/all?q={}"
    SEARCH_CREATOR_PAGE_TITLE = "Browse staff"

    def update_galgame(self, galgame, component):
        if (component & Galgame.Component.Core) > 0:
            r = None
            vndb_id = galgame.vndb_id
            if vndb_id  == None:
                #we'll have to get the id first
                release_date = galgame.release_date.strftime("%Y%m%d")
                if release_date != "20300101":
                    search_url = self.SEARCH_GALGAME_URL_RELEASEDATE.format(quote(galgame.name), release_date)
                else:
                    search_url = self.SEARCH_GALGAME_URL_FUTURE.format(quote(galgame.name))
                try:
                    r = requests.get(search_url, timeout=10)
                    if r.status_code != 200:
                        logger.error("Got unexpected status code {} when searching for {}".format(r.status_code, galgame.name))
                        return
                except requests.exceptions.RequestException as ex:
                    logger.exception("Got an exception when searching for {}".format(galgame.name))
                    return

                if self.SEARCH_GALGAME_PAGE_TITLE in r.text:
                    #lazy regex scrapping: it works! at least for now.   
                    res = re.findall(r'<a href="/v(\d+)" title="[^"]*">([^<]+)</a>', r.text)

                    #what now?
                    logger.warning("Found {} != 1 on vndb when searching for {}: {}".format(len(res), galgame.name, res))
                    return
                else:
                    #we got sent to the result page. regex ftw!
                    m = re.search(self.GALGAME_URL.format(r'(\d+)'), r.text)
                    if not m:
                        logger.warning("Unable to scrap vndb_id from result page for game={}".format(galgame.name))
                        return
                    vndb_id = int(m.group(1))
            else:
                url = self.GALGAME_URL.format(vndb_id)
                try:
                    r = requests.get(url, timeout=10)
                    if r.status_code != 200:
                        logger.error("Got unexpected status code {} when viewing game={}".format(r.status_code, vndb_id))
                        return
                except requests.exceptions.RequestException as ex:
                    logger.exception("Got an exception when viewing game={}".format(vndb_id))
                    return

            #get votes
            m = re.search(r'(\d+) votes total, average ([0-9.]+)', r.text)
            if not m:
                #we also consider this to be successful
                #we found the game, it just doesn't contain vote info
                logger.warning("vndb votes not found for vndb_id={}".format(vndb_id))
            else:
                #found vote
                with self.db.session() as db_session:
                    galgame = db_session.merge(galgame)
                    galgame.votes_vndb_count = int(m.group(1))
                    galgame.votes_vndb_average = round(float(m.group(2)) * 10)

            with self.db.session() as db_session:
                galgame = db_session.merge(galgame)
                galgame.vndb_id = vndb_id
            galgame.mark_updated(self.db, self.__class__, Creator.Component.Core)

    def update_creator(self, creator, components):
        if (components & Creator.Component.Core) > 0:
            r = None
            vndb_id = creator.vndb_id
            if vndb_id == None:
                #remove parenthesis to make search more accurate
                creator_name = re.sub(r'\(.*?\)', "", creator.name)

                #we'll have to get the id first
                search_url = self.SEARCH_CREATOR_URL.format(quote(creator_name))
                try:
                    r = requests.get(search_url, timeout=10)
                    if r.status_code != 200:
                        logger.error("Got unexpected status code {} when searching for {}".format(r.status_code, creator.name))
                        return
                except requests.exceptions.RequestException as ex:
                    logger.error("Got an exception when searching for {}: {}".format(creator.name, ex))
                    return

                if self.SEARCH_CREATOR_PAGE_TITLE in r.text:
                    #lazy regex scrapping: it works! at least for now.   
                    res = re.findall(r'<a href="/s(\d+)" title="([^"]+)">[^<]+</a>', r.text)

                    #what now?
                    logger.warning("Found {} != 1 results on vndb when searching for {}: {}".format(len(res), creator.name, res))
                    return
                else:
                    #we got sent to the result page. regex ftw!
                    m = re.search(self.CREATOR_URL.format(r'(\d+)" />'), r.text)
                    if not m:
                        logger.warning("Unable to scrap vndb_id from result page for creator={}".format(creator.name))
                        return
                    vndb_id = int(m.group(1))
            else:
                url = self.CREATOR_URL.format(vndb_id)
                try:
                    r = requests.get(url, timeout=10)
                    if r.status_code != 200:
                        logger.error("Got unexpected status code {} when viewing creator={}".format(r.status_code, vndb_id))
                        return
                except requests.exceptions.RequestException as ex:
                    logger.exception("Got an exception when viewing creator={}".format(vndb_id))
                    return

            #now grab the aliases
            s = r.text.find('<table class="aliases">')
            if s < 0:
                #some creators might not have aliases
                return
            t = r.text.find("</table>", s)
            if t < 0:
                logger.error("Unable to find the closing of alias table when viewing creator={}".format(vndb_id))
                return

            #aliases
            creator_strs = re.findall(r'<td>([^<]+)</td>', r.text[s:t])

            #main identity
            m = re.search(r'>([^<]+)</h2><table class="stripe">', r.text)
            if m:
                creator_strs.append(m.group(1))
            else:
                logger.warning("Cannot find main identity for creator={}".format(vndb_id))

            with self.db.session() as db_session:
                creators = list(filter(lambda x: x != None,
                                    (Creator.get_by_name(db_session, html_decode(x.replace(" ", "")))
                                    for x in creator_strs)))
                for creator in creators:
                    creator.vndb_id = vndb_id

                if len(creators) >= 2:
                    creators[0].set_as_main_identity(creators[1:])
                    creators[0].redecide_main_identity()
                for creator in creators:
                    creator.mark_updated(self.db, self.__class__, Creator.Component.Core)
