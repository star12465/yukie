import time
import yaml
import queue
import discord
import asyncio
import logging
import logging.config
import concurrent.futures
from thanatos.utils import *
from thanatos.models import *
from thanatos.message_handlers import *
from thanatos.managers import *
from thanatos.contexts import RequestContext

__all__ = ["ThanatosConfig", "Thanatos"]

logger = logging.getLogger(__name__)

class ThanatosConfig:
    def __init__(self):
        self.discord_token = None
        self.connection_string = None

    def validate(self):
        if self.discord_token == None:
            raise ValueError("token not provided")
        if self.connection_string == None:
            raise ValueError("Connection string not provided")

    def load(self, content):
        config = yaml.safe_load(content)
        for k, v in config.items():
            setattr(self, k, v)

    def load_from_file(self, filename):
        with open(filename) as fp:
            self.load(fp.read())

class Thanatos(discord.Client):
    MAX_RECONNECT_TRIES = 3
    MESSAGE_HANDLERS = (ChatHandler, QueryHandler, CommandHandler)

    def __init__(self, config_path, loop=None):
        #load config
        self.config = ThanatosConfig()
        self.config.load_from_file(config_path)
        self.config.validate()

        #configure logger
        logging.config.dictConfig(self.config.logging)

        #init discord client
        super().__init__(loop=loop)

        #dataabse
        self.db = DB(self.config.connection_string)

        #library initializations
        StringNormalizer.initialize(self.config.data_folder)
        AwarenessManager.initialize()
        PersonalitiesManager.initialize(self)
        LanguagesManager.initialize(self.config.language, self.config.locale_folder)

        #message handlers to use
        self.message_handlers = []

        #event handlers
        self.event_listeners = []

        #thread pools for handling messages
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=10)

        self.reconnect_tries = 0
        self.last_connected = None

    def main_loop(self):
        logger.info("Thanatos started")

        while self.reconnect_tries < self.MAX_RECONNECT_TRIES:
            #close and recreate session everytime
            #because self.logout would close a session
            #and we need to make sure it's recreated when we retry
            self.loop.run_until_complete(self.http.close())
            self.http.recreate()
            self.connection.clear()
            self._closed.clear()

            should_retry = True
            try:
                #we can't use client.run because it closes async loop at the end
                self.loop.run_until_complete(self.start(self.config.discord_token))
            except KeyboardInterrupt:
                should_retry = False
                logger.info("Keyboard interrupted. Aborting...")
            except discord.LoginFailure as ex:
                should_retry = False
                logger.exception("Failed to login to Discord")
            except Exception as ex:
                should_retry = True
                logger.exception("Exception raised in the main loop")
            finally:
                self.loop.run_until_complete(self.logout())

                #clean up
                pending = asyncio.Task.all_tasks(loop=self.loop)
                gathered = asyncio.gather(*pending, loop=self.loop, return_exceptions=True)
                gathered.cancel()
                self.loop.run_until_complete(gathered)
                gathered.exception()

                #try to reconnect?
                if not should_retry:
                    break

                #if we were able to stay connected for over 30 minutes
                #consider that a successful connection and reset reconnect tries
                if self.last_connected and (time.time()-self.last_connected) > 60*30:
                    self.reconnect_tries = 0  
                self.reconnect_tries += 1
                logger.info("Retrying {}/{}".format(self.reconnect_tries, self.MAX_RECONNECT_TRIES))

        #in case we break out from keyboard interrupt
        if self.reconnect_tries == self.MAX_RECONNECT_TRIES:
            logger.critical("Max retries reached. Aborted")
        else:
            logger.info("Aborted")

    async def on_ready(self):
        self.last_connected = time.time()

        logger.info("Successfully logged in as {}. Now initializing message handlers.".format(self.user))

        #init message handlers
        self.message_handlers.clear()
        for handler in self.MESSAGE_HANDLERS:
            self.message_handlers.append(handler(self))

        #set to use default personality
        global_metadata = GlobalMetadata(self.db)
        last_personality = global_metadata["last_personality"]
        if last_personality == None:
            logger.info("Loading default personality Thanatos.")
            PersonalitiesManager.use_personality("Thanatos", True)
            global_metadata["last_personality"] = "Thanatos"
        else:
            logger.info("Loading last personality {}.".format(last_personality))
            PersonalitiesManager.use_personality(last_personality, False)
            

        logger.info("Thanatos is now ready.")

    async def on_message(self, discord_msg):
        req_ctx = RequestContext()
        req_ctx.discord_msg = discord_msg
        req_ctx.thanatos = self
        req_ctx.db = self.db
        req_ctx.msg = discord_msg.content

        #get user and guild
        with self.db.session() as db_session:
            discord_user = discord_msg.author
            req_ctx.user = User.create_or_get(db_session, discord_user)

            if discord_msg.server:
                req_ctx.guild = Guild.create_or_get(db_session, discord_msg.server)
                if req_ctx.guild not in req_ctx.user.guilds:
                    req_ctx.user.guilds.append(req_ctx.guild)
                db_session.commit()
                db_session.refresh(req_ctx.user)
                db_session.refresh(req_ctx.guild)
            else:
                req_ctx.guild = None

        #does someone wnat to handle this message before
        #we send to the message handlers
        if await self.handle_event("message", req_ctx):
            return

        for handler in self.message_handlers:
            asyncio.ensure_future(handler.handle_message(req_ctx), loop=self.loop)

    async def on_reaction_add(self, reaction, user):
        req_ctx = RequestContext()
        req_ctx.discord_msg = reaction.message
        req_ctx.discord_reaction = reaction
        req_ctx.discord_reaction_user = user
        await self.handle_event("reaction_add", req_ctx)

    async def on_reaction_remove(self, reaction, user):
        req_ctx = RequestContext()
        req_ctx.discord_msg = reaction.message
        req_ctx.discord_reaction = reaction
        req_ctx.discord_reaction_user = user
        await self.handle_event("reaction_remove", req_ctx)

    def wait_for(self, events, criteria, passthru=False):
        '''
        Thead-safe
        '''
        wait = self.WaitFor(self, events, criteria, passthru)
        self.loop.call_soon_threadsafe(lambda: self.event_listeners.append(wait))
        return wait

    def add_callback_for(self, events, criteria, callback):
        '''
        Thread-safe
        '''
        callback = self.Callback(self, events, criteria, callback)
        self.loop.call_soon_threadsafe(lambda: self.event_listeners.append(callback))
        return callback

    async def handle_event(self, event, req_ctx):
        '''
        Returns true if the event has been processed.
        '''
        for listener in self.event_listeners:
            if event not in listener.events:
                continue
            if not listener.criteria(req_ctx):
                continue

            if isinstance(listener, self.WaitFor):
                listener.add_message(req_ctx)
                if not listener.passthru:
                    return True
            elif isinstance(listener, self.Callback):
                if await listener.callback(event, req_ctx):
                    return True
        return False

    class Listener:
        '''
        Thread safe
        '''
        def __init__(self, thanatos, events, criteria):
            self.thanatos = thanatos
            self.events = events
            self.criteria = criteria

        def delete(self):
            return asyncio.run_coroutine_threadsafe(self.delete_async(), self.thanatos.loop)

        async def delete_async(self):
            #this has to be run in the async tool in main thread
            #to prevent race condition
            try:
                self.thanatos.event_listeners.remove(self)
            except ValueError:
                #doesn't exist
                logger.warning("{} is not in listeners".format(self))

        def __repr__(self):
            return "<{} event={}>".format(self.__class__.__name__, self.event)

    class WaitFor(Listener):
        def __init__(self, thanatos, events, criteria, passthru):
            super().__init__(thanatos, events, criteria)
            self.passthru = passthru
            self.messages = queue.Queue()

        def delete(self):
            super().delete()

            #send the remaining messages for processing
            if not self.passthru:
                async def relay_messages():
                    while not self.messages.empty():
                        try:
                            await self.thanatos.on_message(self.messages.get_nowait().discord_msg)
                        except queue.Empty:
                            pass
                asyncio.run_coroutine_threadsafe(relay_messages(), self.thanatos.loop)

        def add_message(self, req_ctx):
            self.messages.put(req_ctx)

        def get_message(self, timeout):
            try:
                return self.messages.get(timeout=timeout)
            except queue.Empty:
                raise TimeoutError()

    class Callback(Listener):
        def __init__(self, thanatos, events, criteria, callback):
            super().__init__(thanatos, events, criteria)
            self.callback = callback

        def auto_destruct(self, countdown, cleanup_callback=None):
            async def cleanup():
                await asyncio.sleep(countdown)
                await self.delete_async()
                if cleanup_callback:
                    await cleanup_callback()
            asyncio.run_coroutine_threadsafe(cleanup(), self.thanatos.loop)
