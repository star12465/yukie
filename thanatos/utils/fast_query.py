import time
import heapq
import logging
from collections import defaultdict
from enum import IntEnum
from .str_normalizer import normalize_str

__all__ = ["FastQueryAlgorithm", "FastQuery"]

logger = logging.getLogger(__name__)

class FastQueryAlgorithm(IntEnum):
    Auto = 0
    Fast = 1
    Precise = 2

class FastQuery():
    CONSECUTIVE_BONUS = 0.25

    def __init__(self, algorithm=FastQueryAlgorithm.Auto):
        self.dataset = defaultdict(list)
        self.set_search_algorithm(algorithm)

    def set_search_algorithm(self, algorithm):
        if algorithm == FastQueryAlgorithm.Auto:
            self._search_corpus = self._search_auto
        elif algorithm == FastQueryAlgorithm.Fast:
            self._search_corpus = self._search_LCS_fast
        elif algorithm == FastQueryAlgorithm.Precise:
            self._search_corpus = self._search_LCS_precise
        else:
            raise ValueError("Invalid algorithm {}".format(algorithm))

    def add_dataset(self, dataset):
        '''
        Adds a given dataset for searching to this
        fastquery object's collection of datasets

        :dataset: list of (key, value) pairs where 
                    key = what to search for,
                    value = return value of query
        '''
        for (key, value) in dataset:
            normalized = normalize_str(key)
            if len(normalized) == 0:
                #we'll never be able to find you. sorry!
                continue
            # Store by length of key as key since that is how
            # we will query for the future from longest to shortest
            self.dataset[len(key)].append((normalized, value))

        #cache available lengths
        self.lengths = sorted(self.dataset.keys(), reverse=True)

    def find_exact(self, q):
        if len(q) not in self.dataset:
            return None

        normalized_q = normalize_str(q)
        for (key, val) in self.dataset[len(q)]:
            if normalized_q == key:
                return val
        return None

    def _search_auto(self, dp, keyword, corpus):
        """
        TODO: Implement function that uses search algorithm based
                on estimated runtime
        """
        return self._search_LCS_precise(dp, keyword, corpus)

    def _search_LCS_fast(self, dp, keyword, corpus):
        '''
        dp is unused
        '''
        score, i, j = 0, 0, 0
        while i < len(keyword) and j < len(corpus):
            if keyword[i] == corpus[j]:
                score += 1
                if i > 0 and j > 0 and keyword[i-1] == corpus[j-1]:
                    score += self.CONSECUTIVE_BONUS
                i += 1
            j += 1
        return score

    def _search_LCS_precise(self, dp, keyword, corpus):
        #bonus points if the characters are neighbors
        #i.e. if we search "ab", "abd" should have a higher than "adb"
        for i in range(1, len(corpus)+1):
            for j in range(1, len(keyword)+1):
                if corpus[i-1] == keyword[j-1]:
                    dp[i][j] = dp[i-1][j-1]+1
                    if i > 1 and j > 1 and corpus[i-2] == keyword[j-2]:
                        dp[i][j] += self.CONSECUTIVE_BONUS
                else:
                    dp[i][j] = max(dp[i-1][j], dp[i][j-1])
        #final score is the score of the last element
        #we can't use dp[-1][-1] because we don't actually fill up
        #the entire 2d-array.
        return dp[i][j]

    def find_similar(self, q, limit, tolerance=0.7):
        '''
        :q: query to find similar matches to
            We rate similarity by number of common subsequence
                i.e. corpus=CLANNAD, q=CLD => rating = 3;
                                     q=CLAD => rating = 4
            This also mean we only need to look at the corpus longer
            than our query
        when we have the same rating, we want to prioritize the shorter corpus

        :limit: an integer > 0 as the max size of list of 
                possibly matching entries to return
        
                '''
        if len(self.lengths) == 0:
            #no data
            logger.warning("No data loaded to FastQuery")
            return []
        start_time = time.time()
        nq = normalize_str(q)

        # Create dp space for common subsequence algorithm
        # Create biggest space possibly necessary and reuse since
        # self.length[0] represents longest possible dataset query len
        dp = [[0 for _ in range(len(nq)+1)] 
                for _ in range(self.lengths[0]+1)]

        similar_data = []  # Priority queue
        for length in self.lengths:
            if len(similar_data) == limit:
                # We're at the limit
                # Look at the lowest rating we have
                # if this is greater than or equal to the most
                # possible points the corpus can offer, we can stop
                (lowest_rating, _), _ = similar_data[0]
                max_possible_rating = length + (length-1)*self.CONSECUTIVE_BONUS
                if lowest_rating >= max_possible_rating:
                    break

            for (key, val) in self.dataset[length]:
                score = self._search_auto(dp, nq, key)

                #is this above our tolerance threshold?
                if score < len(nq)*tolerance:
                    continue

                # Priority queue sorted by (rating, -(corpus length))
                # This way we order first by rating,
                # then by shorter words to longest words
                if len(similar_data) == limit:
                    # If we are at limit, push current and then
                    # pop worst matching 
                    heapq.heappushpop(similar_data, ((score, -length), val))
                else:
                    # Push as normal
                    heapq.heappush(similar_data, ((score, -length), val))

        #reverse the order so we have the words with the highest rating
        #in the front
        res = []
        while len(similar_data) > 0:
            (rating, _), val = heapq.heappop(similar_data)
            res.append((rating, val))
        res = res[::-1]

        logger.info("Querying {} took {}ms".format(q, (time.time()-start_time)*1000))
        return res

    def find_most_likely(self, q):
        res = self.find_similar(q, 5, 0.7)
        logger.info(res)
        return res[0][1] if len(res) > 0 else None
