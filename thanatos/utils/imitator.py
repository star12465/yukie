import re
import os
import os.path
import yaml
import logging
from thanatos.models import *
from .fast_query import FastQuery
import zhconv

__all__ = ["Imitator"]

logger = logging.getLogger(__name__)

class Imitator:
    INFO_FILENAME = "info.yaml"

    def __init__(self, db, data_path, language):
        self.db = db
        self.base_path = os.path.join(data_path, "galgame_texts")
        self.language = language

        self.fq = FastQuery()
        self._load_all_texts()

    def _load_all_texts(self):
        for game_folder in os.listdir(self.base_path):
            if game_folder[0] in (".", "_"):
                continue

            game_texts_path = os.path.join(self.base_path, game_folder)
            if not os.path.isdir(game_texts_path):
                continue

            #get language
            game_texts_path = os.path.join(game_texts_path, self.language)
            if not os.path.isdir(game_texts_path):
                #language not found
                continue

            #read info file
            info_path = os.path.join(game_texts_path, self.INFO_FILENAME)
            text_info = self.GalgameTextInfo.from_file(info_path)
            if not text_info:
                continue

            logger.info("Loading galgame texts from {}".format(game_folder))
            for route_name in os.listdir(game_texts_path):
                if route_name[0] == "_":
                    continue

                #get routes
                route_folder_path = os.path.join(game_texts_path, route_name)
                if not os.path.isdir(route_folder_path):
                    continue

                for route_text_filename in sorted(os.listdir(route_folder_path)):
                    #get route texts
                    if route_text_filename[0] == "_":
                        continue
                    if os.path.splitext(route_text_filename)[1] != ".txt":
                        continue

                    text_path = os.path.join(route_folder_path, route_text_filename)
                    if not os.path.isfile(text_path):
                        continue
                    self._load_text(galgame, route_name, text_path, info)

    def _load_text(self, galgame, route_name, text_path, info):
        dataset = []
        try:
            with open(text_path, encoding="utf-8") as fp:
                lines = fp.read().split("\n")
                speaker = None
                protag_line = None
                heroine_lines = []
                for line in lines:
                    line = line.strip()
                    reset_heroines_line = False

                    is_speaker_name = False
                    if line in info.get("protag_names", []):
                        speaker = "_protag"
                        reset_heorines_line = True
                        is_speaker_name = True
                    else:
                        for heroine, heroine_names in info.get("heroines", {}).items():
                            if not heroine_names:
                                continue
                            if line in heroine_names:
                                speaker = heroine
                                is_speaker_name = True
                                break

                    if not is_speaker_name:
                        m = re.search(r'^「(.*)」$', line)
                        if m:
                            line = m.group(1)
                            if speaker == "_protag":
                                protag_line = line
                            elif speaker and protag_line:
                                heroine_lines.append(self._replace_names(line, info))
                        else:
                            #description line
                            reset_heroines_line = True

                    if reset_heroines_line:
                        if len(heroine_lines) > 0:
                            print(protag_line, heroine_lines)
                            dataset.append((protag_line, heroine_lines))
                        heroine_lines = []
                        speaker = None
                        protag_line = None
        except UnicodeDecodeError:
            logger.exception("Unable to decode {} with utf-8".format(text_path))
        self.fq.add_dataset(dataset)

    def get_response(self, q):
        response = self.fq.find_most_likely(q)
        if response:
            return zhconv.convert(response, "zh-tw")

    class GalgameTextInfo:
        @staticmethod
        def from_file(info_path):
            try:
                with open(info_path) as fp:
                    info = yaml.safe_load(fp.read())
            except IOError:
                logger.exception("Unable to read galgame text info file {}".format(info_path))
                return False

            #get game
            if "galgame_id" not in info:
                logger.error("galgame id not specified in galgame text info file {}".format(info_path))
                return False
            galgame_id = int(info["galgame_id"])
            with self.db.session() as db_session:
                galgame = db_session.query(Galgame).filter_by(id=galgame_id)
            if not galgame:
                logger.error("galgame id {} not found from galgame text info file {}".format(galgame_id, game_folder))
                return False

            self.aliases = {}
            for char_collection in ("heros", "heroines"):
                for name, char_info in info.get(char_collection, {}):
                    
                    for alias in heroine.get("aliases"):
                        self.aliases[alias] = name

        def parse_speaker(self, name):
            pass

        def _replace_names(self, speaker, line):
            names = [name for heroine_names in info.get("heroines", {}).values() for name in heroine_names]
            names.sort(key=len, reverse=True)
            for name in names:
                line = line.replace(name, "你")
            for name in info.get("protag_names", []):
                line = line.replace(name, "我")
            for remove in info.get("removes", []):
                line = re.sub(line, remove, "")
            return line
