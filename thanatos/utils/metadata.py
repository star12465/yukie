import json
from thanatos.models import *

__all__ = ["Metadata", "GlobalMetadata"]

class Metadata:
    def __init__(self, db, obj, namespace):
        self.db = db
        self.target_type = obj.__class__.__name__
        self.target_id = obj.id
        self.namespace = namespace
        self.db_session = None

    def __enter__(self):
        self.contexted_db_session = self.db.session()
        self.db_session = self.contexted_db_session.__enter__()
        return self

    def __exit__(self, *args, **kwargs):
        self.contexted_db_session.__exit__(*args, **kwargs)

    def get(self, key, default=None):
        def _get_value(db_session):
            q = db_session.query(MetadataT).filter_by(target_type=self.target_type,
                                                 target_id=self.target_id,
                                                 namespace=self.namespace,
                                                 key=key)
            return json.loads(q.one().value) if q.count() > 0 else default

        if self.db_session:
            return _get_value(self.db_session)
        else:
            with self.db.session() as db_session:
                return _get_value(db_session)

    def __getitem__(self, key):
        return self.get(key)

    def __setitem__(self, key, value):
        metadata = MetadataT()
        metadata.target_type = self.target_type
        metadata.target_id = self.target_id
        metadata.namespace = self.namespace
        metadata.key = key
        metadata.value = json.dumps(value)
        if self.db_session:
            self.db_session.merge(metadata)
            self.db_session.commit()
        else:
            with self.db.session() as db_session:
                db_session.merge(metadata)

class GlobalMetadata(Metadata):
    def __init__(self, db):
        self.db = db
        self.target_type = "Global"
        self.target_id = 0
        self.namespace = "Global"
        self.db_session = None
