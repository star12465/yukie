# -*- coding: utf-8 -*-
import os
import os.path
import unicodedata
import logging
import zhconv

__all__ = ["StringNormalizer", "normalize_str"]

logger = logging.getLogger(__name__)

class StringNormalizer(object):
    kanjis = {}

    @classmethod
    def initialize(cls, data_path):
        #preprocess kanjis
        base_path = os.path.join(data_path, "equivalent_kanjis/")
        for filename in os.listdir(base_path):
            if os.path.splitext(filename)[1] != ".txt":
                continue

            path = os.path.join(base_path, filename)
            with open(path, encoding="utf-8") as fp:
                data = fp.read().strip().split("\n")
                for row in data:
                    kanjis = row.split("=")
                    if len(kanjis) == 0:
                        continue

                    #we should check to see if we are already
                    #normalizing any of this kanji to another one
                    #if so we want to use the same target
                    target = None
                    for kanji in kanjis:
                        if kanji in cls.kanjis:
                            target = cls.kanjis[kanji]
                            break
                    if target == None:
                        target = kanjis[0]

                    for kanji in kanjis:
                        cls.kanjis[kanji] = target

    @classmethod
    def normalize_kanjis(cls, s):
        return "".join(cls.kanjis.get(x, x) for x in s)

    @staticmethod
    def normalize_case(s):
        return s.lower()

    @staticmethod
    def normalize_unicode(s):
        return unicodedata.normalize("NFKD", s)

    @staticmethod
    def remove_symbols(s):
        return "".join(ch for ch in s if unicodedata.category(ch)[0] in ("L", "N"))

    @staticmethod
    def tradc_to_simpc(s):
        return zhconv.convert(s, "zh-cn")

    @classmethod
    def normalize(cls, s, **kwargs):
        if kwargs.get("unicode", True):
            s = cls.normalize_unicode(s)
        if kwargs.get("symbols", True):
            s = cls.remove_symbols(s)
        if kwargs.get("case", True):
            s = cls.normalize_case(s)
        if kwargs.get("tradc_to_simpc", True):
            s = cls.tradc_to_simpc(s)
        if kwargs.get("kanjis", True):
            s = cls.normalize_kanjis(s)
        return s

def normalize_str(s, **kwargs):
    return StringNormalizer.normalize(s, **kwargs)
