#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
import logging
from sys import exit
from thanatos.thanatos import ThanatosConfig, Thanatos
from thanatos.utils import DB, StringNormalizer
from thanatos.models import *
from thanatos.providers import *
from thanatos.managers import ProvidersManager

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

#SELECT id || ',' || title || '=' FROM povlist order by system_group
def add_galgame_trait_tl(db_session, filename, field):
    with open(filename) as fp:
        for trait_id, trait_tl in re.findall(r'(\d+),[^=]+=(.+)', fp.read()):
            trait = db_session.query(GalgameTrait).filter_by(id=trait_id).one()
            setattr(trait, field, trait_tl)
    db_session.commit()

if __name__ == "__main__":
    #a lot of initializations
    thanatos = Thanatos("config.yaml")

    #we need to update creators after works have been populated
    #because redecide_main_identity relies on work information
    models = (
        Brand,
        Galgame,
        GalgameTrait,
        GalgameTraitVote,
        Video,
        Creator,
        Character,
    )

    try:
        ProvidersManager.update_all(thanatos.db, models)
        with thanatos.db.session() as db_session:
            add_galgame_trait_tl(db_session, "data/galgame_traits_chinesetl.txt", "chinese_name")
    finally:
        thanatos.loop.run_until_complete(thanatos.close())
